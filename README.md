     

### <a id="documentation-body"></a>

![Hackolade image](/assets/image1.png?raw=true)

MongoDB Physical Model
----------------------

#### Schema for:

Model name: WUIIY - Backend

Author:

Version:

File name: MongoDB model WUIIY - Backend.hck.json

File path: /home/erickson/MongoDB model WUIIY - Backend.hck.json

Printed On: Fri Mar 10 2023 03:02:58 GMT+0700 (Western Indonesia Time)

Created with: [Hackolade](https://hackolade.com/) - Polyglot data modeling for NoSQL databases, storage formats, REST APIs, and JSON in RDBMS

### <a id="contents"></a>

*   [Table of Contents](#contents)
*   [1\. Model](#model)
*   [2\. Databases](#containers)
    *   [2.1 brand\_management](#f4eb80a3-d474-42e2-b254-8b7516beb3f4)
        
        [2.1.2. Collections](#f4eb80a3-d474-42e2-b254-8b7516beb3f4-children)
        
        [2.1.2.1 brand](#687c8e38-bfb5-476a-b4d6-af6ef99ae244)
        
        [2.1.2.2 magic\_token\_verif](#084dbd81-74a7-446b-bbfc-8c3e96add4a2)
        
        [2.1.2.3 otp\_verif](#5f1e22f4-0879-40e9-aaf6-94dbcd99cd3f)
        
        [2.1.2.4 temp\_register\_verif](#6dbd8e45-0d33-4936-a245-7717243bafe8)
        
    *   [2.2 clinic\_central\_auth](#a980dbf3-04b3-4a12-a7b0-6a9a116d207c)
        
        [2.2.2. Collections](#a980dbf3-04b3-4a12-a7b0-6a9a116d207c-children)
        
        [2.2.2.1 brand](#b7efdaa5-e68f-4611-b6a3-d3b58fbdf646)
        
        [2.2.2.2 central\_auth\_view](#11dcebf5-9936-4ccf-a530-730dd01db5e9)
        
        [2.2.2.3 clinic](#2eac7b06-e370-4e3b-b145-3d6a77c998f0)
        
        [2.2.2.4 merchant](#262baf94-3e39-4a4b-9fdd-9989aa11b9c1)
        
    *   [2.3 doctor\_management](#15bed2f5-714c-42eb-8540-90b101910d8b)
        
        [2.3.2. Collections](#15bed2f5-714c-42eb-8540-90b101910d8b-children)
        
        [2.3.2.1 doctor](#56d72f17-dfa0-4234-9b36-66a8d2bc45dd)
        
        [2.3.2.2 magic\_token\_verif](#0b999a7b-fa7b-419d-9f6a-ef6df8bdbac7)
        
    *   [2.4 merchant\_management](#3f008a3d-bd44-4d20-9e8a-39d56386fe29)
        
        [2.4.2. Collections](#3f008a3d-bd44-4d20-9e8a-39d56386fe29-children)
        
        [2.4.2.1 magic\_token\_verif](#93d85f88-2fd3-4050-b2dc-bb43ee06bf2f)
        
        [2.4.2.2 merchant](#94f57272-e536-4bec-a992-620568b4a916)
        
    *   [2.5 payment\_processing](#df27f8e5-371f-430b-a520-e67cdd12ff67)
        
        [2.5.2. Collections](#df27f8e5-371f-430b-a520-e67cdd12ff67-children)
        
        [2.5.2.1 payment](#0aacc000-c080-402f-ae1d-90997d4daf54)
        
        [2.5.2.2 payment\_gateway](#18f9a5af-8c70-44bc-a6b8-28a673f9eb13)
        
        [2.5.2.3 payment\_method](#5bc92ff3-77a2-461f-b396-ecf1af9deb2b)
        
    *   [2.6 user\_management](#f671992c-2f83-4667-a12c-e7d809cfc82a)
        
        [2.6.2. Collections](#f671992c-2f83-4667-a12c-e7d809cfc82a-children)
        
        [2.6.2.1 magic\_token\_verif](#71559dab-09ef-4f31-85fd-3585c5537361)
        
        [2.6.2.2 otp\_verif](#3c6c484a-4a6d-4390-a7b3-117297837918)
        
        [2.6.2.3 user](#bf2c22f1-3ab1-499d-9310-1d6fe75f718a)
        
*   [3\. Relationships](#relationships)
    *   [3.1 fk Magic Token Verification(1).Doctor Id to Doctor.Id](#b29fcc15-a7c0-40ab-9ca7-f2854249bc09)
    *   [3.2 fk Magic Token Verification.Brand Id to Brand.Id](#3bdff99c-5203-4062-84ba-8ffba9af4b29)
    *   [3.3 fk Magic Token Verification.Merchant Id to Merchant.Id](#8aa0ac7f-3f40-411e-80f8-0070bd642cd9)
    *   [3.4 fk OTP Verification.Email to Brand.Merchant Email](#c77cbdd1-8e88-4e3b-b1a8-6e63d5fe7e15)
    *   [3.5 fk OTP.Email to user.Email](#5676d0e8-8dd9-4d99-8663-4fa9903c3916)
    *   [3.6 fk Payment Method.Payment Gateway to payment\_gateway.Payment Gateway Code](#6406736b-1a68-44b5-a21a-73c36e03f2cb)
    *   [3.7 fk Payment.Payment Method to Payment Method.Payment Method Code](#5f634399-6c3b-43f2-acc8-611994da352e)
    *   [3.8 fk Reset.User Id to user.Id](#bf49e0b0-c051-469e-9b54-5290c853bf1d)
    *   [3.9 fk Temp Register Verification.OTP Id to OTP Verification.Id](#955f6833-99c3-414c-887d-d47e8fd1ba1e)

### <a id="model"></a>

##### 1\. Model

##### 1.1 Model **WUIIY - Backend**

##### 1.1.1 **WUIIY - Backend** Entity Relationship Diagram

![Hackolade image](/assets/image2.png?raw=true)

##### 1.1.2 **WUIIY - Backend** Properties

##### 1.1.2.1 **Details** tab

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td><span>Model name</span></td><td>WUIIY - Backend</td></tr><tr><td><span>Technical name</span></td><td></td></tr><tr><td><span>Description</span></td><td><div class="docs-markdown"></div></td></tr><tr><td><span>Author</span></td><td></td></tr><tr><td><span>Version</span></td><td></td></tr><tr><td><span>Target</span></td><td>MongoDB</td></tr><tr><td><span>DB version</span></td><td>v6.0</td></tr><tr><td><span>Synchronization Id</span></td><td></td></tr><tr><td><span>Lineage capture</span></td><td></td></tr><tr><td><span>Polyglot models</span></td><td></td></tr><tr><td><span>Comments</span></td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 1.1.3 **WUIIY - Backend** DB Definitions

### <a id="containers"></a>

##### 2\. Databases

### <a id="f4eb80a3-d474-42e2-b254-8b7516beb3f4"></a>2.1 Database **brand\_management**

![Hackolade image](/assets/image3.png?raw=true)

##### 2.1.1 **brand\_management** Properties

<table class="collection-properties-table"><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Database name</td><td>Brand Management</td></tr><tr><td>Technical name</td><td>brand_management</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Enable sharding</td><td></td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="f4eb80a3-d474-42e2-b254-8b7516beb3f4-children"></a>2.1.2 **brand\_management** Collections

### <a id="687c8e38-bfb5-476a-b4d6-af6ef99ae244"></a>2.1.2.1 Collection **brand**

##### 2.1.2.1.1 **brand** Tree Diagram

![Hackolade image](/assets/image4.png?raw=true)

##### 2.1.2.1.2 **brand** Properties

<table class="collection-properties-table"><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Collection name</td><td>Brand</td></tr><tr><td>Technical name</td><td>brand</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>$ref</td><td></td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Database</td><td><a href=#f4eb80a3-d474-42e2-b254-8b7516beb3f4><span class="name-container">brand_management</span></a></td></tr><tr><td>Capped</td><td></td></tr><tr><td>Time series</td><td></td></tr><tr><td>Size</td><td></td></tr><tr><td>Max</td><td></td></tr><tr><td>Storage engine</td><td>WiredTiger</td></tr><tr><td>Config String</td><td></td></tr><tr><td>Validation level</td><td>Off</td></tr><tr><td>Validation action</td><td>Warn</td></tr><tr><td>Encryption metadata</td><td></td></tr><tr><td>Additional properties</td><td>false</td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.1.2.1.3 **brand** Fields

<table><thead><tr><td>Field</td><td>Type</td><td>Req</td><td>Key</td><td>Description</td><td>Comments</td></tr></thead><tbody><tr><td><a href=#714db236-d303-4735-83a5-ce5b56c59fc6 class="margin-0">_id</a></td><td class="no-break-word">objectId</td><td>false</td><td>pk, dk</td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#1c9d8190-21a2-49b8-8d6e-f581dfb32bcb class="margin-0">name</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#fdfdad1d-376a-44f0-9112-77543732a6e6 class="margin-0">email</a></td><td class="no-break-word">string</td><td>false</td><td>dk</td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#5a840726-a2b2-41e5-8863-439188e36eab class="margin-0">password</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#1e0e90c6-7136-4305-bbee-226472987d65 class="margin-0">phone_number</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#67292f7d-67f6-4e91-b9ea-9eb087514383 class="margin-0">is_whatsapp</a></td><td class="no-break-word">boolean</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#d91c0986-a66e-416d-9785-cfc477cf31bb class="margin-0">location</a></td><td class="no-break-word">document</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#1126f83d-a7c5-415b-be4e-6a0f6c254397 class="margin-5">address</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#e3e5a12d-6a05-408c-bb4d-167e21f8a85e class="margin-5">latitude</a></td><td class="no-break-word">double</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#28ddf83a-2649-4d1a-be95-11713817f83f class="margin-5">longitude</a></td><td class="no-break-word">double</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#41fb5b3d-5980-4613-a425-a820b2d379bb class="margin-0">owner_name</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#cd0eb209-7fc4-4009-99f0-1c63d142a0f0 class="margin-0">npwp_no</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#d82b993b-9f63-43b9-a0a4-786012ee3eba class="margin-0">document</a></td><td class="no-break-word">document</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#dbc85d27-0bb0-401e-a5be-b4b47b33e999 class="margin-5">npwp_doc_id</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#ad0dfa7d-7428-4ee8-9772-6d76cb8a1eb3 class="margin-5">ktp_doc_id</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#c7d1ced8-ebd9-4617-91c6-ef3347a562bb class="margin-5">nib_doc_id</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#f2a9f892-711a-49a6-b54c-9a2b8c11febc class="margin-5">cert_doc_id</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#25b999be-a442-4a0d-9707-54e546a54a9f class="margin-5">tdp_doc_id</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#e6f5dd14-659f-486c-b429-8b4d24b5d049 class="margin-5">siup_doc_id</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#1528e350-485a-4578-9ef0-9e7c36c71487 class="margin-0">pic</a></td><td class="no-break-word">document</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#d1dfc430-cb4b-404c-8050-de70aab5872b class="margin-5">name</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#3e8e39df-7076-43e9-af7d-aaa208dcec43 class="margin-5">email</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#e1e3332a-e4f4-45f8-b8b6-b9e230d166b6 class="margin-5">occupation</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#7a1b4880-605d-40c1-82c2-33a1186e63a8 class="margin-5">phone_number</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#8a606241-4e57-4a59-a5ea-d7733f659ead class="margin-5">is_whatsapp</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#53e6c9a2-949b-4296-ad61-e17a52887876 class="margin-0">created_date</a></td><td class="no-break-word">date-time</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#71ef3346-f92c-4140-a59d-ca566425170a class="margin-0">updated_date</a></td><td class="no-break-word">date-time</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#7627a5d8-b9cf-49ff-9278-97a2fdcf91e3 class="margin-0">is_active</a></td><td class="no-break-word">boolean</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="714db236-d303-4735-83a5-ce5b56c59fc6"></a>2.1.2.1.3.1 Field **\_id**

##### 2.1.2.1.3.1.1 **\_id** Tree Diagram

![Hackolade image](/assets/image5.png?raw=true)

##### 2.1.2.1.3.1.2 **\_id** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Id</td></tr><tr><td>Technical name</td><td>_id</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>objectId</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td>true</td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="1c9d8190-21a2-49b8-8d6e-f581dfb32bcb"></a>2.1.2.1.3.2 Field **name**

##### 2.1.2.1.3.2.1 **name** Tree Diagram

![Hackolade image](/assets/image6.png?raw=true)

##### 2.1.2.1.3.2.2 **name** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Name</td></tr><tr><td>Technical name</td><td>name</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="fdfdad1d-376a-44f0-9112-77543732a6e6"></a>2.1.2.1.3.3 Field **email**

##### 2.1.2.1.3.3.1 **email** Tree Diagram

![Hackolade image](/assets/image7.png?raw=true)

##### 2.1.2.1.3.3.2 **email** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Email</td></tr><tr><td>Technical name</td><td>email</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="5a840726-a2b2-41e5-8863-439188e36eab"></a>2.1.2.1.3.4 Field **password**

##### 2.1.2.1.3.4.1 **password** Tree Diagram

![Hackolade image](/assets/image8.png?raw=true)

##### 2.1.2.1.3.4.2 **password** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Password</td></tr><tr><td>Technical name</td><td>password</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="1e0e90c6-7136-4305-bbee-226472987d65"></a>2.1.2.1.3.5 Field **phone\_number**

##### 2.1.2.1.3.5.1 **phone\_number** Tree Diagram

![Hackolade image](/assets/image9.png?raw=true)

##### 2.1.2.1.3.5.2 **phone\_number** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Phone Number</td></tr><tr><td>Technical name</td><td>phone_number</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="67292f7d-67f6-4e91-b9ea-9eb087514383"></a>2.1.2.1.3.6 Field **is\_whatsapp**

##### 2.1.2.1.3.6.1 **is\_whatsapp** Tree Diagram

![Hackolade image](/assets/image10.png?raw=true)

##### 2.1.2.1.3.6.2 **is\_whatsapp** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Is Whatsapp?</td></tr><tr><td>Technical name</td><td>is_whatsapp</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>boolean</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="d91c0986-a66e-416d-9785-cfc477cf31bb"></a>2.1.2.1.3.7 Field **location**

##### 2.1.2.1.3.7.1 **location** Tree Diagram

![Hackolade image](/assets/image11.png?raw=true)

##### 2.1.2.1.3.7.2 **location** Hierarchy

Parent field: **brand**

Child field(s):

<table class="field-properties-table"><thead><tr><td>Field</td><td>Type</td><td>Req</td><td>Key</td><td>Description</td><td>Comments</td></tr></thead><tbody><tr><td><a href=#1126f83d-a7c5-415b-be4e-6a0f6c254397 class="margin-NaN">Address</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#e3e5a12d-6a05-408c-bb4d-167e21f8a85e class="margin-NaN">Latitude</a></td><td class="no-break-word">numeric</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#28ddf83a-2649-4d1a-be95-11713817f83f class="margin-NaN">Longitude</a></td><td class="no-break-word">numeric</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.1.2.1.3.7.3 **location** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Location</td></tr><tr><td>Technical name</td><td>location</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>document</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>DBRef</td><td></td></tr><tr><td>Min Properties</td><td></td></tr><tr><td>Max Properties</td><td></td></tr><tr><td>Additional properties</td><td>false</td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="1126f83d-a7c5-415b-be4e-6a0f6c254397"></a>2.1.2.1.3.8 Field **address**

##### 2.1.2.1.3.8.1 **address** Tree Diagram

![Hackolade image](/assets/image12.png?raw=true)

##### 2.1.2.1.3.8.2 **address** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Address</td></tr><tr><td>Technical name</td><td>address</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="e3e5a12d-6a05-408c-bb4d-167e21f8a85e"></a>2.1.2.1.3.9 Field **latitude**

##### 2.1.2.1.3.9.1 **latitude** Tree Diagram

![Hackolade image](/assets/image13.png?raw=true)

##### 2.1.2.1.3.9.2 **latitude** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Latitude</td></tr><tr><td>Technical name</td><td>latitude</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>numeric</td></tr><tr><td>Subtype</td><td>double</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Unit</td><td></td></tr><tr><td>Min value</td><td></td></tr><tr><td>Excl min</td><td></td></tr><tr><td>Max value</td><td></td></tr><tr><td>Excl max</td><td></td></tr><tr><td>Multiple of</td><td></td></tr><tr><td>Divisible by</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="28ddf83a-2649-4d1a-be95-11713817f83f"></a>2.1.2.1.3.10 Field **longitude**

##### 2.1.2.1.3.10.1 **longitude** Tree Diagram

![Hackolade image](/assets/image14.png?raw=true)

##### 2.1.2.1.3.10.2 **longitude** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Longitude</td></tr><tr><td>Technical name</td><td>longitude</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>numeric</td></tr><tr><td>Subtype</td><td>double</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Unit</td><td></td></tr><tr><td>Min value</td><td></td></tr><tr><td>Excl min</td><td></td></tr><tr><td>Max value</td><td></td></tr><tr><td>Excl max</td><td></td></tr><tr><td>Multiple of</td><td></td></tr><tr><td>Divisible by</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="41fb5b3d-5980-4613-a425-a820b2d379bb"></a>2.1.2.1.3.11 Field **owner\_name**

##### 2.1.2.1.3.11.1 **owner\_name** Tree Diagram

![Hackolade image](/assets/image15.png?raw=true)

##### 2.1.2.1.3.11.2 **owner\_name** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Owner Name</td></tr><tr><td>Technical name</td><td>owner_name</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="cd0eb209-7fc4-4009-99f0-1c63d142a0f0"></a>2.1.2.1.3.12 Field **npwp\_no**

##### 2.1.2.1.3.12.1 **npwp\_no** Tree Diagram

![Hackolade image](/assets/image16.png?raw=true)

##### 2.1.2.1.3.12.2 **npwp\_no** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>NPWP Number</td></tr><tr><td>Technical name</td><td>npwp_no</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="d82b993b-9f63-43b9-a0a4-786012ee3eba"></a>2.1.2.1.3.13 Field **document**

##### 2.1.2.1.3.13.1 **document** Tree Diagram

![Hackolade image](/assets/image17.png?raw=true)

##### 2.1.2.1.3.13.2 **document** Hierarchy

Parent field: **brand**

Child field(s):

<table class="field-properties-table"><thead><tr><td>Field</td><td>Type</td><td>Req</td><td>Key</td><td>Description</td><td>Comments</td></tr></thead><tbody><tr><td><a href=#dbc85d27-0bb0-401e-a5be-b4b47b33e999 class="margin-NaN">NPWP</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#ad0dfa7d-7428-4ee8-9772-6d76cb8a1eb3 class="margin-NaN">KTP</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#c7d1ced8-ebd9-4617-91c6-ef3347a562bb class="margin-NaN">NIB</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#f2a9f892-711a-49a6-b54c-9a2b8c11febc class="margin-NaN">Akta</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#25b999be-a442-4a0d-9707-54e546a54a9f class="margin-NaN">TDP</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#e6f5dd14-659f-486c-b429-8b4d24b5d049 class="margin-NaN">SIUP</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.1.2.1.3.13.3 **document** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Document</td></tr><tr><td>Technical name</td><td>document</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>document</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>DBRef</td><td></td></tr><tr><td>Min Properties</td><td></td></tr><tr><td>Max Properties</td><td></td></tr><tr><td>Additional properties</td><td>false</td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="dbc85d27-0bb0-401e-a5be-b4b47b33e999"></a>2.1.2.1.3.14 Field **npwp\_doc\_id**

##### 2.1.2.1.3.14.1 **npwp\_doc\_id** Tree Diagram

![Hackolade image](/assets/image18.png?raw=true)

##### 2.1.2.1.3.14.2 **npwp\_doc\_id** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>NPWP</td></tr><tr><td>Technical name</td><td>npwp_doc_id</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="ad0dfa7d-7428-4ee8-9772-6d76cb8a1eb3"></a>2.1.2.1.3.15 Field **ktp\_doc\_id**

##### 2.1.2.1.3.15.1 **ktp\_doc\_id** Tree Diagram

![Hackolade image](/assets/image19.png?raw=true)

##### 2.1.2.1.3.15.2 **ktp\_doc\_id** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>KTP</td></tr><tr><td>Technical name</td><td>ktp_doc_id</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="c7d1ced8-ebd9-4617-91c6-ef3347a562bb"></a>2.1.2.1.3.16 Field **nib\_doc\_id**

##### 2.1.2.1.3.16.1 **nib\_doc\_id** Tree Diagram

![Hackolade image](/assets/image20.png?raw=true)

##### 2.1.2.1.3.16.2 **nib\_doc\_id** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>NIB</td></tr><tr><td>Technical name</td><td>nib_doc_id</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="f2a9f892-711a-49a6-b54c-9a2b8c11febc"></a>2.1.2.1.3.17 Field **cert\_doc\_id**

##### 2.1.2.1.3.17.1 **cert\_doc\_id** Tree Diagram

![Hackolade image](/assets/image21.png?raw=true)

##### 2.1.2.1.3.17.2 **cert\_doc\_id** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Akta</td></tr><tr><td>Technical name</td><td>cert_doc_id</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="25b999be-a442-4a0d-9707-54e546a54a9f"></a>2.1.2.1.3.18 Field **tdp\_doc\_id**

##### 2.1.2.1.3.18.1 **tdp\_doc\_id** Tree Diagram

![Hackolade image](/assets/image22.png?raw=true)

##### 2.1.2.1.3.18.2 **tdp\_doc\_id** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>TDP</td></tr><tr><td>Technical name</td><td>tdp_doc_id</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="e6f5dd14-659f-486c-b429-8b4d24b5d049"></a>2.1.2.1.3.19 Field **siup\_doc\_id**

##### 2.1.2.1.3.19.1 **siup\_doc\_id** Tree Diagram

![Hackolade image](/assets/image23.png?raw=true)

##### 2.1.2.1.3.19.2 **siup\_doc\_id** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>SIUP</td></tr><tr><td>Technical name</td><td>siup_doc_id</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="1528e350-485a-4578-9ef0-9e7c36c71487"></a>2.1.2.1.3.20 Field **pic**

##### 2.1.2.1.3.20.1 **pic** Tree Diagram

![Hackolade image](/assets/image24.png?raw=true)

##### 2.1.2.1.3.20.2 **pic** Hierarchy

Parent field: **brand**

Child field(s):

<table class="field-properties-table"><thead><tr><td>Field</td><td>Type</td><td>Req</td><td>Key</td><td>Description</td><td>Comments</td></tr></thead><tbody><tr><td><a href=#d1dfc430-cb4b-404c-8050-de70aab5872b class="margin-NaN">Name</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#3e8e39df-7076-43e9-af7d-aaa208dcec43 class="margin-NaN">Email</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#e1e3332a-e4f4-45f8-b8b6-b9e230d166b6 class="margin-NaN">Occupation</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#7a1b4880-605d-40c1-82c2-33a1186e63a8 class="margin-NaN">Phone&nbsp;Number</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#8a606241-4e57-4a59-a5ea-d7733f659ead class="margin-NaN">Is&nbsp;Whatsapp?</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.1.2.1.3.20.3 **pic** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>PIC</td></tr><tr><td>Technical name</td><td>pic</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>document</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>DBRef</td><td></td></tr><tr><td>Min Properties</td><td></td></tr><tr><td>Max Properties</td><td></td></tr><tr><td>Additional properties</td><td>false</td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="d1dfc430-cb4b-404c-8050-de70aab5872b"></a>2.1.2.1.3.21 Field **name**

##### 2.1.2.1.3.21.1 **name** Tree Diagram

![Hackolade image](/assets/image25.png?raw=true)

##### 2.1.2.1.3.21.2 **name** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Name</td></tr><tr><td>Technical name</td><td>name</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="3e8e39df-7076-43e9-af7d-aaa208dcec43"></a>2.1.2.1.3.22 Field **email**

##### 2.1.2.1.3.22.1 **email** Tree Diagram

![Hackolade image](/assets/image26.png?raw=true)

##### 2.1.2.1.3.22.2 **email** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Email</td></tr><tr><td>Technical name</td><td>email</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="e1e3332a-e4f4-45f8-b8b6-b9e230d166b6"></a>2.1.2.1.3.23 Field **occupation**

##### 2.1.2.1.3.23.1 **occupation** Tree Diagram

![Hackolade image](/assets/image27.png?raw=true)

##### 2.1.2.1.3.23.2 **occupation** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Occupation</td></tr><tr><td>Technical name</td><td>occupation</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="7a1b4880-605d-40c1-82c2-33a1186e63a8"></a>2.1.2.1.3.24 Field **phone\_number**

##### 2.1.2.1.3.24.1 **phone\_number** Tree Diagram

![Hackolade image](/assets/image28.png?raw=true)

##### 2.1.2.1.3.24.2 **phone\_number** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Phone Number</td></tr><tr><td>Technical name</td><td>phone_number</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="8a606241-4e57-4a59-a5ea-d7733f659ead"></a>2.1.2.1.3.25 Field **is\_whatsapp**

##### 2.1.2.1.3.25.1 **is\_whatsapp** Tree Diagram

![Hackolade image](/assets/image29.png?raw=true)

##### 2.1.2.1.3.25.2 **is\_whatsapp** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Is Whatsapp?</td></tr><tr><td>Technical name</td><td>is_whatsapp</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="53e6c9a2-949b-4296-ad61-e17a52887876"></a>2.1.2.1.3.26 Field **created\_date**

##### 2.1.2.1.3.26.1 **created\_date** Tree Diagram

![Hackolade image](/assets/image30.png?raw=true)

##### 2.1.2.1.3.26.2 **created\_date** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Created Date</td></tr><tr><td>Technical name</td><td>created_date</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td>date-time</td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td>false</td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="71ef3346-f92c-4140-a59d-ca566425170a"></a>2.1.2.1.3.27 Field **updated\_date**

##### 2.1.2.1.3.27.1 **updated\_date** Tree Diagram

![Hackolade image](/assets/image31.png?raw=true)

##### 2.1.2.1.3.27.2 **updated\_date** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Updated Date</td></tr><tr><td>Technical name</td><td>updated_date</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td>date-time</td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td>false</td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="7627a5d8-b9cf-49ff-9278-97a2fdcf91e3"></a>2.1.2.1.3.28 Field **is\_active**

##### 2.1.2.1.3.28.1 **is\_active** Tree Diagram

![Hackolade image](/assets/image32.png?raw=true)

##### 2.1.2.1.3.28.2 **is\_active** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Is Active?</td></tr><tr><td>Technical name</td><td>is_active</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>boolean</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.1.2.1.4 **brand** JSON Schema

```
{
    "$schema": "http://json-schema.org/draft-04/schema#",
    "type": "object",
    "title": "Brand",
    "properties": {
        "_id": {
            "type": "string",
            "title": "Id",
            "pattern": "^[a-fA-F0-9]{24}$"
        },
        "name": {
            "type": "string",
            "title": "Name"
        },
        "email": {
            "type": "string",
            "title": "Email"
        },
        "password": {
            "type": "string",
            "title": "Password"
        },
        "phone_number": {
            "type": "string",
            "title": "Phone Number"
        },
        "is_whatsapp": {
            "type": "boolean",
            "title": "Is Whatsapp?"
        },
        "location": {
            "type": "object",
            "title": "Location",
            "properties": {
                "address": {
                    "type": "string",
                    "title": "Address"
                },
                "latitude": {
                    "type": "number",
                    "title": "Latitude"
                },
                "longitude": {
                    "type": "number",
                    "title": "Longitude"
                }
            },
            "additionalProperties": false
        },
        "owner_name": {
            "type": "string",
            "title": "Owner Name"
        },
        "npwp_no": {
            "type": "string",
            "title": "NPWP Number"
        },
        "document": {
            "type": "object",
            "title": "Document",
            "properties": {
                "npwp_doc_id": {
                    "type": "string",
                    "title": "NPWP"
                },
                "ktp_doc_id": {
                    "type": "string",
                    "title": "KTP"
                },
                "nib_doc_id": {
                    "type": "string",
                    "title": "NIB"
                },
                "cert_doc_id": {
                    "type": "string",
                    "title": "Akta"
                },
                "tdp_doc_id": {
                    "type": "string",
                    "title": "TDP"
                },
                "siup_doc_id": {
                    "type": "string",
                    "title": "SIUP"
                }
            },
            "additionalProperties": false
        },
        "pic": {
            "type": "object",
            "title": "PIC",
            "properties": {
                "name": {
                    "type": "string",
                    "title": "Name"
                },
                "email": {
                    "type": "string",
                    "title": "Email"
                },
                "occupation": {
                    "type": "string",
                    "title": "Occupation"
                },
                "phone_number": {
                    "type": "string",
                    "title": "Phone Number"
                },
                "is_whatsapp": {
                    "type": "string",
                    "title": "Is Whatsapp?"
                }
            },
            "additionalProperties": false
        },
        "created_date": {
            "type": "string",
            "title": "Created Date",
            "format": "date-time"
        },
        "updated_date": {
            "type": "string",
            "title": "Updated Date",
            "format": "date-time"
        },
        "is_active": {
            "type": "boolean",
            "title": "Is Active?"
        }
    },
    "additionalProperties": false
}
```

##### 2.1.2.1.5 **brand** JSON data

```
{
    "_id": ObjectId("dc2da7fee8a6ae3fdd714b7e"),
    "name": "Lorem",
    "email": "Lorem",
    "password": "Lorem",
    "phone_number": "Lorem",
    "is_whatsapp": true,
    "location": {
        "address": "Lorem",
        "latitude": Double(-9.751837393486543e+29),
        "longitude": Double(-9.761888613009306e+29)
    },
    "owner_name": "Lorem",
    "npwp_no": "Lorem",
    "document": {
        "npwp_doc_id": "Lorem",
        "ktp_doc_id": "Lorem",
        "nib_doc_id": "Lorem",
        "cert_doc_id": "Lorem",
        "tdp_doc_id": "Lorem",
        "siup_doc_id": "Lorem"
    },
    "pic": {
        "name": "Lorem",
        "email": "Lorem",
        "occupation": "Lorem",
        "phone_number": "Lorem",
        "is_whatsapp": "Lorem"
    },
    "created_date": "2011-06-14T04:12:36.123Z",
    "updated_date": "2011-06-14T04:12:36.123Z",
    "is_active": true
}
```

##### 2.1.2.1.6 **brand** Target Script

```
use brand_management;

db.createCollection("brand", {
    "capped": false,
    "validator": {
        "$jsonSchema": {
            "bsonType": "object",
            "title": "Brand",
            "properties": {
                "_id": {
                    "bsonType": "objectId",
                    "title": "Id"
                },
                "name": {
                    "bsonType": "string",
                    "title": "Name"
                },
                "email": {
                    "bsonType": "string",
                    "title": "Email"
                },
                "password": {
                    "bsonType": "string",
                    "title": "Password"
                },
                "phone_number": {
                    "bsonType": "string",
                    "title": "Phone Number"
                },
                "is_whatsapp": {
                    "bsonType": "bool",
                    "title": "Is Whatsapp?"
                },
                "location": {
                    "bsonType": "object",
                    "title": "Location",
                    "properties": {
                        "address": {
                            "bsonType": "string",
                            "title": "Address"
                        },
                        "latitude": {
                            "bsonType": "double",
                            "title": "Latitude"
                        },
                        "longitude": {
                            "bsonType": "double",
                            "title": "Longitude"
                        }
                    },
                    "additionalProperties": false
                },
                "owner_name": {
                    "bsonType": "string",
                    "title": "Owner Name"
                },
                "npwp_no": {
                    "bsonType": "string",
                    "title": "NPWP Number"
                },
                "document": {
                    "bsonType": "object",
                    "title": "Document",
                    "properties": {
                        "npwp_doc_id": {
                            "bsonType": "string",
                            "title": "NPWP"
                        },
                        "ktp_doc_id": {
                            "bsonType": "string",
                            "title": "KTP"
                        },
                        "nib_doc_id": {
                            "bsonType": "string",
                            "title": "NIB"
                        },
                        "cert_doc_id": {
                            "bsonType": "string",
                            "title": "Akta"
                        },
                        "tdp_doc_id": {
                            "bsonType": "string",
                            "title": "TDP"
                        },
                        "siup_doc_id": {
                            "bsonType": "string",
                            "title": "SIUP"
                        }
                    },
                    "additionalProperties": false
                },
                "pic": {
                    "bsonType": "object",
                    "title": "PIC",
                    "properties": {
                        "name": {
                            "bsonType": "string",
                            "title": "Name"
                        },
                        "email": {
                            "bsonType": "string",
                            "title": "Email"
                        },
                        "occupation": {
                            "bsonType": "string",
                            "title": "Occupation"
                        },
                        "phone_number": {
                            "bsonType": "string",
                            "title": "Phone Number"
                        },
                        "is_whatsapp": {
                            "bsonType": "string",
                            "title": "Is Whatsapp?"
                        }
                    },
                    "additionalProperties": false
                },
                "created_date": {
                    "bsonType": "string",
                    "title": "Created Date"
                },
                "updated_date": {
                    "bsonType": "string",
                    "title": "Updated Date"
                },
                "is_active": {
                    "bsonType": "bool",
                    "title": "Is Active?"
                }
            },
            "additionalProperties": false
        }
    },
    "validationLevel": "off",
    "validationAction": "warn"
});
```

### <a id="084dbd81-74a7-446b-bbfc-8c3e96add4a2"></a>2.1.2.2 Collection **magic\_token\_verif**

##### 2.1.2.2.1 **magic\_token\_verif** Tree Diagram

![Hackolade image](/assets/image33.png?raw=true)

##### 2.1.2.2.2 **magic\_token\_verif** Properties

<table class="collection-properties-table"><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Collection name</td><td>Magic Token Verification</td></tr><tr><td>Technical name</td><td>magic_token_verif</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>$ref</td><td></td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Database</td><td><a href=#f4eb80a3-d474-42e2-b254-8b7516beb3f4><span class="name-container">brand_management</span></a></td></tr><tr><td>Capped</td><td></td></tr><tr><td>Time series</td><td></td></tr><tr><td>Size</td><td></td></tr><tr><td>Max</td><td></td></tr><tr><td>Storage engine</td><td>WiredTiger</td></tr><tr><td>Config String</td><td></td></tr><tr><td>Validation level</td><td>Off</td></tr><tr><td>Validation action</td><td>Warn</td></tr><tr><td>Encryption metadata</td><td></td></tr><tr><td>Additional properties</td><td>false</td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.1.2.2.3 **magic\_token\_verif** Fields

<table><thead><tr><td>Field</td><td>Type</td><td>Req</td><td>Key</td><td>Description</td><td>Comments</td></tr></thead><tbody><tr><td><a href=#d372857e-4473-4c81-8dc2-372484d2be5c class="margin-0">_id</a></td><td class="no-break-word">objectId</td><td>false</td><td>pk</td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#b9a18993-6e43-4df5-9402-6e8180241195 class="margin-0">brand_id</a></td><td class="no-break-word">objectId</td><td>false</td><td>fk</td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#cdaee5ad-7081-44ca-9334-c00bf5b5fb5d class="margin-0">magic_token</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#9f12789f-5b94-4861-96c5-9de162e9d2e2 class="margin-0">is_used</a></td><td class="no-break-word">boolean</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#881e8c5e-e0f4-4392-9196-3975dcbdf56f class="margin-0">expiration_date</a></td><td class="no-break-word">date-time</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="d372857e-4473-4c81-8dc2-372484d2be5c"></a>2.1.2.2.3.1 Field **\_id**

##### 2.1.2.2.3.1.1 **\_id** Tree Diagram

![Hackolade image](/assets/image34.png?raw=true)

##### 2.1.2.2.3.1.2 **\_id** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Id</td></tr><tr><td>Technical name</td><td>_id</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>objectId</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td>true</td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="b9a18993-6e43-4df5-9402-6e8180241195"></a>2.1.2.2.3.2 Field **brand\_id**

##### 2.1.2.2.3.2.1 **brand\_id** Tree Diagram

![Hackolade image](/assets/image35.png?raw=true)

##### 2.1.2.2.3.2.2 **brand\_id** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Brand Id</td></tr><tr><td>Technical name</td><td>brand_id</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td>false</td></tr><tr><td>Type</td><td>objectId</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Foreign collection</td><td><a href=#687c8e38-bfb5-476a-b4d6-af6ef99ae244>brand</a></td></tr><tr><td>Foreign field</td><td><a href=#714db236-d303-4735-83a5-ce5b56c59fc6>_id</a></td></tr><tr><td>Relationship type</td><td>Foreign Key</td></tr><tr><td>Relationship name</td><td>fk Magic Token Verification.Brand Id to Brand.Id</td></tr><tr><td>Cardinality</td><td>1</td></tr><tr><td>Default</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="cdaee5ad-7081-44ca-9334-c00bf5b5fb5d"></a>2.1.2.2.3.3 Field **magic\_token**

##### 2.1.2.2.3.3.1 **magic\_token** Tree Diagram

![Hackolade image](/assets/image36.png?raw=true)

##### 2.1.2.2.3.3.2 **magic\_token** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Magic Token</td></tr><tr><td>Technical name</td><td>magic_token</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="9f12789f-5b94-4861-96c5-9de162e9d2e2"></a>2.1.2.2.3.4 Field **is\_used**

##### 2.1.2.2.3.4.1 **is\_used** Tree Diagram

![Hackolade image](/assets/image37.png?raw=true)

##### 2.1.2.2.3.4.2 **is\_used** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Is Used?</td></tr><tr><td>Technical name</td><td>is_used</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>boolean</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="881e8c5e-e0f4-4392-9196-3975dcbdf56f"></a>2.1.2.2.3.5 Field **expiration\_date**

##### 2.1.2.2.3.5.1 **expiration\_date** Tree Diagram

![Hackolade image](/assets/image38.png?raw=true)

##### 2.1.2.2.3.5.2 **expiration\_date** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Expiration Date</td></tr><tr><td>Technical name</td><td>expiration_date</td></tr><tr><td>Activated</td><td>false</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td>date-time</td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.1.2.2.4 **magic\_token\_verif** Indexes

<table class="index-table"><thead><tr><td class="table-property-column">Property</td><td class="table-column-property">User Id Unique</td></tr></thead><tbody><tr><td>Name</td><td class="table-column-indexes">User Id Unique</td></tr><tr><td>Activated</td><td class="table-column-indexes">true</td></tr><tr><td>Key</td><td class="table-column-indexes">brand_id('ascending')</td></tr><tr><td>Hashed</td><td class="table-column-indexes"></td></tr><tr><td>Unique</td><td class="table-column-indexes">true</td></tr><tr><td>Drop duplicates</td><td class="table-column-indexes"></td></tr><tr><td>Sparse</td><td class="table-column-indexes"></td></tr><tr><td>Background indexing</td><td class="table-column-indexes"></td></tr><tr><td>Partial filter exp</td><td class="table-column-indexes"></td></tr><tr><td>Expire after (seconds)</td><td class="table-column-indexes"></td></tr><tr><td>Storage engine</td><td class="table-column-indexes">WiredTiger</td></tr><tr><td>Comments</td><td class="table-column-indexes"></td></tr></tbody></table>

##### 2.1.2.2.5 **magic\_token\_verif** JSON Schema

```
{
    "$schema": "http://json-schema.org/draft-04/schema#",
    "type": "object",
    "title": "Magic Token Verification",
    "properties": {
        "_id": {
            "type": "string",
            "title": "Id",
            "pattern": "^[a-fA-F0-9]{24}$"
        },
        "brand_id": {
            "type": "string",
            "title": "Brand Id",
            "pattern": "^[a-fA-F0-9]{24}$"
        },
        "magic_token": {
            "type": "string",
            "title": "Magic Token"
        },
        "is_used": {
            "type": "boolean",
            "title": "Is Used?"
        },
        "expiration_date": {
            "type": "string",
            "title": "Expiration Date",
            "format": "date-time"
        }
    },
    "additionalProperties": false
}
```

##### 2.1.2.2.6 **magic\_token\_verif** JSON data

```
{
    "_id": ObjectId("8dbe1176a3ab45d3fbadc104"),
    "brand_id": ObjectId("6ecdc8edadddbaf969eefbfe"),
    "magic_token": "Lorem",
    "is_used": true,
    "expiration_date": "2011-06-14T04:12:36.123Z"
}
```

##### 2.1.2.2.7 **magic\_token\_verif** Target Script

```
use brand_management;

db.createCollection("magic_token_verif", {
    "capped": false,
    "validator": {
        "$jsonSchema": {
            "bsonType": "object",
            "title": "Magic Token Verification",
            "properties": {
                "_id": {
                    "bsonType": "objectId",
                    "title": "Id"
                },
                "brand_id": {
                    "bsonType": "objectId",
                    "title": "Brand Id"
                },
                "magic_token": {
                    "bsonType": "string",
                    "title": "Magic Token"
                },
                "is_used": {
                    "bsonType": "bool",
                    "title": "Is Used?"
                }
            },
            "additionalProperties": false
        }
    },
    "validationLevel": "off"
});

db.magic_token_verif.createIndex({
    "brand_id": 1
},
{
    "name": "User Id Unique",
    "unique": true
});
```

### <a id="5f1e22f4-0879-40e9-aaf6-94dbcd99cd3f"></a>2.1.2.3 Collection **otp\_verif**

##### 2.1.2.3.1 **otp\_verif** Tree Diagram

![Hackolade image](/assets/image39.png?raw=true)

##### 2.1.2.3.2 **otp\_verif** Properties

<table class="collection-properties-table"><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Collection name</td><td>OTP Verification</td></tr><tr><td>Technical name</td><td>otp_verif</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>$ref</td><td></td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Database</td><td><a href=#f4eb80a3-d474-42e2-b254-8b7516beb3f4><span class="name-container">brand_management</span></a></td></tr><tr><td>Capped</td><td></td></tr><tr><td>Time series</td><td></td></tr><tr><td>Size</td><td></td></tr><tr><td>Max</td><td></td></tr><tr><td>Storage engine</td><td>WiredTiger</td></tr><tr><td>Config String</td><td></td></tr><tr><td>Validation level</td><td>Off</td></tr><tr><td>Validation action</td><td>Warn</td></tr><tr><td>Encryption metadata</td><td></td></tr><tr><td>Additional properties</td><td>false</td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.1.2.3.3 **otp\_verif** Fields

<table><thead><tr><td>Field</td><td>Type</td><td>Req</td><td>Key</td><td>Description</td><td>Comments</td></tr></thead><tbody><tr><td><a href=#abc7118c-2d6c-470d-b95b-56c8b0487d49 class="margin-0">_id</a></td><td class="no-break-word">objectId</td><td>false</td><td>pk, dk</td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#5e3ffeba-6616-4874-81f1-055189857f33 class="margin-0">email</a></td><td class="no-break-word">string</td><td>false</td><td>fk</td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#e1aae438-a89d-47ea-8cb1-8d28304baf5c class="margin-0">otp</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#f23d70e5-5532-4242-8dde-455ca44b8ce7 class="margin-0">is_used</a></td><td class="no-break-word">boolean</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#67894014-0015-4377-af12-2b033686c16d class="margin-0">expiration_date</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="abc7118c-2d6c-470d-b95b-56c8b0487d49"></a>2.1.2.3.3.1 Field **\_id**

##### 2.1.2.3.3.1.1 **\_id** Tree Diagram

![Hackolade image](/assets/image40.png?raw=true)

##### 2.1.2.3.3.1.2 **\_id** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Id</td></tr><tr><td>Technical name</td><td>_id</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>objectId</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td>true</td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="5e3ffeba-6616-4874-81f1-055189857f33"></a>2.1.2.3.3.2 Field **email**

##### 2.1.2.3.3.2.1 **email** Tree Diagram

![Hackolade image](/assets/image41.png?raw=true)

##### 2.1.2.3.3.2.2 **email** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Email</td></tr><tr><td>Technical name</td><td>email</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td><a href=#687c8e38-bfb5-476a-b4d6-af6ef99ae244>brand</a></td></tr><tr><td>Foreign field</td><td><a href=#fdfdad1d-376a-44f0-9112-77543732a6e6>email</a></td></tr><tr><td>Relationship type</td><td>Foreign Key</td></tr><tr><td>Relationship name</td><td>fk OTP Verification.Email to Brand.Merchant Email</td></tr><tr><td>Cardinality</td><td>1</td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="e1aae438-a89d-47ea-8cb1-8d28304baf5c"></a>2.1.2.3.3.3 Field **otp**

##### 2.1.2.3.3.3.1 **otp** Tree Diagram

![Hackolade image](/assets/image42.png?raw=true)

##### 2.1.2.3.3.3.2 **otp** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>OTP</td></tr><tr><td>Technical name</td><td>otp</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="f23d70e5-5532-4242-8dde-455ca44b8ce7"></a>2.1.2.3.3.4 Field **is\_used**

##### 2.1.2.3.3.4.1 **is\_used** Tree Diagram

![Hackolade image](/assets/image43.png?raw=true)

##### 2.1.2.3.3.4.2 **is\_used** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Is Used?</td></tr><tr><td>Technical name</td><td>is_used</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>boolean</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="67894014-0015-4377-af12-2b033686c16d"></a>2.1.2.3.3.5 Field **expiration\_date**

##### 2.1.2.3.3.5.1 **expiration\_date** Tree Diagram

![Hackolade image](/assets/image44.png?raw=true)

##### 2.1.2.3.3.5.2 **expiration\_date** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Expiration Date</td></tr><tr><td>Technical name</td><td>expiration_date</td></tr><tr><td>Activated</td><td>false</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.1.2.3.4 **otp\_verif** Indexes

<table class="index-table"><thead><tr><td class="table-property-column">Property</td><td class="table-column-property">Email Unique</td></tr></thead><tbody><tr><td>Name</td><td class="table-column-indexes">Email Unique</td></tr><tr><td>Activated</td><td class="table-column-indexes">true</td></tr><tr><td>Key</td><td class="table-column-indexes">email('ascending')</td></tr><tr><td>Hashed</td><td class="table-column-indexes"></td></tr><tr><td>Unique</td><td class="table-column-indexes">true</td></tr><tr><td>Drop duplicates</td><td class="table-column-indexes"></td></tr><tr><td>Sparse</td><td class="table-column-indexes"></td></tr><tr><td>Background indexing</td><td class="table-column-indexes"></td></tr><tr><td>Partial filter exp</td><td class="table-column-indexes"></td></tr><tr><td>Expire after (seconds)</td><td class="table-column-indexes"></td></tr><tr><td>Storage engine</td><td class="table-column-indexes">WiredTiger</td></tr><tr><td>Comments</td><td class="table-column-indexes"></td></tr></tbody></table>

##### 2.1.2.3.5 **otp\_verif** JSON Schema

```
{
    "$schema": "http://json-schema.org/draft-04/schema#",
    "type": "object",
    "title": "OTP Verification",
    "properties": {
        "_id": {
            "type": "string",
            "title": "Id",
            "pattern": "^[a-fA-F0-9]{24}$"
        },
        "email": {
            "type": "string",
            "title": "Email"
        },
        "otp": {
            "type": "string",
            "title": "OTP"
        },
        "is_used": {
            "type": "boolean",
            "title": "Is Used?"
        },
        "expiration_date": {
            "type": "string",
            "title": "Expiration Date"
        }
    },
    "additionalProperties": false
}
```

##### 2.1.2.3.6 **otp\_verif** JSON data

```
{
    "_id": ObjectId("c59fcd316b7e056ae8fe587e"),
    "email": "Lorem",
    "otp": "Lorem",
    "is_used": true,
    "expiration_date": "Lorem"
}
```

##### 2.1.2.3.7 **otp\_verif** Target Script

```
use brand_management;

db.createCollection("otp_verif", {
    "capped": false,
    "validator": {
        "$jsonSchema": {
            "bsonType": "object",
            "title": "OTP Verification",
            "properties": {
                "_id": {
                    "bsonType": "objectId",
                    "title": "Id"
                },
                "email": {
                    "bsonType": "string",
                    "title": "Email"
                },
                "otp": {
                    "bsonType": "string",
                    "title": "OTP"
                },
                "is_used": {
                    "bsonType": "bool",
                    "title": "Is Used?"
                }
            },
            "additionalProperties": false
        }
    },
    "validationLevel": "off",
    "validationAction": "warn"
});

db.otp_verif.createIndex({
    "email": 1
},
{
    "name": "Email Unique",
    "unique": true
});
```

### <a id="6dbd8e45-0d33-4936-a245-7717243bafe8"></a>2.1.2.4 Collection **temp\_register\_verif**

##### 2.1.2.4.1 **temp\_register\_verif** Tree Diagram

![Hackolade image](/assets/image45.png?raw=true)

##### 2.1.2.4.2 **temp\_register\_verif** Properties

<table class="collection-properties-table"><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Collection name</td><td>Temp Register Verification</td></tr><tr><td>Technical name</td><td>temp_register_verif</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>$ref</td><td></td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Database</td><td><a href=#f4eb80a3-d474-42e2-b254-8b7516beb3f4><span class="name-container">brand_management</span></a></td></tr><tr><td>Capped</td><td></td></tr><tr><td>Time series</td><td></td></tr><tr><td>Size</td><td></td></tr><tr><td>Max</td><td></td></tr><tr><td>Storage engine</td><td>WiredTiger</td></tr><tr><td>Config String</td><td></td></tr><tr><td>Validation level</td><td>Off</td></tr><tr><td>Validation action</td><td>Warn</td></tr><tr><td>Encryption metadata</td><td></td></tr><tr><td>Additional properties</td><td>false</td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.1.2.4.3 **temp\_register\_verif** Fields

<table><thead><tr><td>Field</td><td>Type</td><td>Req</td><td>Key</td><td>Description</td><td>Comments</td></tr></thead><tbody><tr><td><a href=#0708dfd9-1d0b-42ad-86b8-804236106310 class="margin-0">_id</a></td><td class="no-break-word">objectId</td><td>false</td><td>pk</td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#dcadbb78-86d4-447f-a31c-5921aa62b926 class="margin-0">otp_id</a></td><td class="no-break-word">objectId</td><td>false</td><td>fk</td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#bb1b74a4-dc2c-4b67-9d1a-2a5a7e5ed9e2 class="margin-0">register_id</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#18f4bc77-5eac-4bbd-89d6-83f7e84a8e74 class="margin-0">email</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#29a5a3aa-444d-4e62-b62c-af0551cd2d45 class="margin-0">is_verified</a></td><td class="no-break-word">boolean</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="0708dfd9-1d0b-42ad-86b8-804236106310"></a>2.1.2.4.3.1 Field **\_id**

##### 2.1.2.4.3.1.1 **\_id** Tree Diagram

![Hackolade image](/assets/image46.png?raw=true)

##### 2.1.2.4.3.1.2 **\_id** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Id</td></tr><tr><td>Technical name</td><td>_id</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>objectId</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td>true</td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="dcadbb78-86d4-447f-a31c-5921aa62b926"></a>2.1.2.4.3.2 Field **otp\_id**

##### 2.1.2.4.3.2.1 **otp\_id** Tree Diagram

![Hackolade image](/assets/image47.png?raw=true)

##### 2.1.2.4.3.2.2 **otp\_id** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>OTP Id</td></tr><tr><td>Technical name</td><td>otp_id</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>objectId</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td>Foreign Key</td></tr><tr><td>Relationship name</td><td>fk Temp Register Verification.OTP Id to OTP Verification.Id</td></tr><tr><td>Cardinality</td><td>0..1</td></tr><tr><td>Default</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="bb1b74a4-dc2c-4b67-9d1a-2a5a7e5ed9e2"></a>2.1.2.4.3.3 Field **register\_id**

##### 2.1.2.4.3.3.1 **register\_id** Tree Diagram

![Hackolade image](/assets/image48.png?raw=true)

##### 2.1.2.4.3.3.2 **register\_id** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Register Id</td></tr><tr><td>Technical name</td><td>register_id</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="18f4bc77-5eac-4bbd-89d6-83f7e84a8e74"></a>2.1.2.4.3.4 Field **email**

##### 2.1.2.4.3.4.1 **email** Tree Diagram

![Hackolade image](/assets/image49.png?raw=true)

##### 2.1.2.4.3.4.2 **email** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Email</td></tr><tr><td>Technical name</td><td>email</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="29a5a3aa-444d-4e62-b62c-af0551cd2d45"></a>2.1.2.4.3.5 Field **is\_verified**

##### 2.1.2.4.3.5.1 **is\_verified** Tree Diagram

![Hackolade image](/assets/image50.png?raw=true)

##### 2.1.2.4.3.5.2 **is\_verified** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Is Verified?</td></tr><tr><td>Technical name</td><td>is_verified</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>boolean</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.1.2.4.4 **temp\_register\_verif** JSON Schema

```
{
    "$schema": "http://json-schema.org/draft-04/schema#",
    "type": "object",
    "title": "Temp Register Verification",
    "properties": {
        "_id": {
            "type": "string",
            "title": "Id",
            "pattern": "^[a-fA-F0-9]{24}$"
        },
        "otp_id": {
            "type": "string",
            "title": "OTP Id",
            "pattern": "^[a-fA-F0-9]{24}$"
        },
        "register_id": {
            "type": "string",
            "title": "Register Id"
        },
        "email": {
            "type": "string",
            "title": "Email"
        },
        "is_verified": {
            "type": "boolean",
            "title": "Is Verified?"
        }
    },
    "additionalProperties": false
}
```

##### 2.1.2.4.5 **temp\_register\_verif** JSON data

```
{
    "_id": ObjectId("a42d53a132d3d4f256acff98"),
    "otp_id": ObjectId("a7c7872d7cf7a43dadeabc04"),
    "register_id": "Lorem",
    "email": "Lorem",
    "is_verified": true
}
```

##### 2.1.2.4.6 **temp\_register\_verif** Target Script

```
use brand_management;

db.createCollection("temp_register_verif", {
    "capped": false,
    "validator": {
        "$jsonSchema": {
            "bsonType": "object",
            "title": "Temp Register Verification",
            "properties": {
                "_id": {
                    "bsonType": "objectId",
                    "title": "Id"
                },
                "otp_id": {
                    "bsonType": "objectId",
                    "title": "OTP Id"
                },
                "register_id": {
                    "bsonType": "string",
                    "title": "Register Id"
                },
                "email": {
                    "bsonType": "string",
                    "title": "Email"
                },
                "is_verified": {
                    "bsonType": "bool",
                    "title": "Is Verified?"
                }
            },
            "additionalProperties": false
        }
    },
    "validationLevel": "off",
    "validationAction": "warn"
});
```

### <a id="a980dbf3-04b3-4a12-a7b0-6a9a116d207c"></a>2.2 Database **clinic\_central\_auth**

![Hackolade image](/assets/image51.png?raw=true)

##### 2.2.1 **clinic\_central\_auth** Properties

<table class="collection-properties-table"><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Database name</td><td>Clinic Central Auth</td></tr><tr><td>Technical name</td><td>clinic_central_auth</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Enable sharding</td><td></td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="a980dbf3-04b3-4a12-a7b0-6a9a116d207c-children"></a>2.2.2 **clinic\_central\_auth** Collections

### <a id="b7efdaa5-e68f-4611-b6a3-d3b58fbdf646"></a>2.2.2.1 Collection **brand**

##### 2.2.2.1.1 **brand** Tree Diagram

![Hackolade image](/assets/image52.png?raw=true)

##### 2.2.2.1.2 **brand** Properties

<table class="collection-properties-table"><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Collection name</td><td>Brand</td></tr><tr><td>Technical name</td><td>brand</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>$ref</td><td></td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Database</td><td><a href=#a980dbf3-04b3-4a12-a7b0-6a9a116d207c><span class="name-container">clinic_central_auth</span></a></td></tr><tr><td>Capped</td><td></td></tr><tr><td>Time series</td><td></td></tr><tr><td>Size</td><td></td></tr><tr><td>Max</td><td></td></tr><tr><td>Storage engine</td><td>WiredTiger</td></tr><tr><td>Config String</td><td></td></tr><tr><td>Validation level</td><td>Off</td></tr><tr><td>Validation action</td><td>Warn</td></tr><tr><td>Encryption metadata</td><td></td></tr><tr><td>Additional properties</td><td>false</td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.2.2.1.3 **brand** Fields

<table><thead><tr><td>Field</td><td>Type</td><td>Req</td><td>Key</td><td>Description</td><td>Comments</td></tr></thead><tbody><tr><td><a href=#28b3bf05-7f6b-4397-aa84-2f8c835e30e1 class="margin-0">+&nbsp;refresh_token</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#ae031c11-87fc-4760-a9d5-7bd79517c122 class="margin-0">+&nbsp;account</a></td><td class="no-break-word">array</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#10ed60f6-b536-43a5-a316-f7b05720cacc class="margin-5">[0]</a></td><td class="no-break-word">document</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#4569d21e-1778-4c8e-9c7f-8e2c7be8621f class="margin-10">oauth_id</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#919fce2d-4404-4565-8fe6-e51f0ea23631 class="margin-10">oauth_code</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="28b3bf05-7f6b-4397-aa84-2f8c835e30e1"></a>2.2.2.1.3.1 Field **\+ refresh\_token**

##### 2.2.2.1.3.1.1 **\+ refresh\_token** Tree Diagram

![Hackolade image](/assets/image53.png?raw=true)

##### 2.2.2.1.3.1.2 **\+ refresh\_token** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>+ Refresh Token</td></tr><tr><td>Technical name</td><td>+ refresh_token</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="ae031c11-87fc-4760-a9d5-7bd79517c122"></a>2.2.2.1.3.2 Field **\+ account**

##### 2.2.2.1.3.2.1 **\+ account** Tree Diagram

![Hackolade image](/assets/image54.png?raw=true)

##### 2.2.2.1.3.2.2 **\+ account** Hierarchy

Parent field: **brand**

Child field(s):

<table class="field-properties-table"><thead><tr><td>Field</td><td>Type</td><td>Req</td><td>Key</td><td>Description</td><td>Comments</td></tr></thead><tbody><tr><td><a href=#10ed60f6-b536-43a5-a316-f7b05720cacc class="margin-NaN">[0]</a></td><td class="no-break-word">document</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.2.2.1.3.2.3 **\+ account** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>+ OAuth Account</td></tr><tr><td>Technical name</td><td>+ account</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>array</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Min items</td><td></td></tr><tr><td>Max items</td><td></td></tr><tr><td>Unique items</td><td></td></tr><tr><td>Additional items</td><td>true</td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="10ed60f6-b536-43a5-a316-f7b05720cacc"></a>2.2.2.1.3.3 Field **\[0\]**

##### 2.2.2.1.3.3.1 **\[0\]** Tree Diagram

![Hackolade image](/assets/image55.png?raw=true)

##### 2.2.2.1.3.3.2 **\[0\]** Hierarchy

Parent field: **+ account**

Child field(s):

<table class="field-properties-table"><thead><tr><td>Field</td><td>Type</td><td>Req</td><td>Key</td><td>Description</td><td>Comments</td></tr></thead><tbody><tr><td><a href=#4569d21e-1778-4c8e-9c7f-8e2c7be8621f class="margin-NaN">OAuth&nbsp;Id</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#919fce2d-4404-4565-8fe6-e51f0ea23631 class="margin-NaN">OAuth&nbsp;Code</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.2.2.1.3.3.3 **\[0\]** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Display name</td><td></td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>document</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>DBRef</td><td></td></tr><tr><td>Min Properties</td><td></td></tr><tr><td>Max Properties</td><td></td></tr><tr><td>Additional properties</td><td>false</td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="4569d21e-1778-4c8e-9c7f-8e2c7be8621f"></a>2.2.2.1.3.4 Field **oauth\_id**

##### 2.2.2.1.3.4.1 **oauth\_id** Tree Diagram

![Hackolade image](/assets/image56.png?raw=true)

##### 2.2.2.1.3.4.2 **oauth\_id** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>OAuth Id</td></tr><tr><td>Technical name</td><td>oauth_id</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="919fce2d-4404-4565-8fe6-e51f0ea23631"></a>2.2.2.1.3.5 Field **oauth\_code**

##### 2.2.2.1.3.5.1 **oauth\_code** Tree Diagram

![Hackolade image](/assets/image57.png?raw=true)

##### 2.2.2.1.3.5.2 **oauth\_code** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>OAuth Code</td></tr><tr><td>Technical name</td><td>oauth_code</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.2.2.1.4 **brand** JSON Schema

```
{
    "$schema": "http://json-schema.org/draft-04/schema#",
    "type": "object",
    "title": "Brand",
    "properties": {
        "+ refresh_token": {
            "type": "string",
            "title": "+ Refresh Token"
        },
        "+ account": {
            "type": "array",
            "title": "+ OAuth Account",
            "additionalItems": true,
            "items": {
                "type": "object",
                "properties": {
                    "oauth_id": {
                        "type": "string",
                        "title": "OAuth Id"
                    },
                    "oauth_code": {
                        "type": "string",
                        "title": "OAuth Code"
                    }
                },
                "additionalProperties": false
            }
        }
    },
    "additionalProperties": false
}
```

##### 2.2.2.1.5 **brand** JSON data

```
{
    "+ refresh_token": "Lorem",
    "+ account": [
        {
            "oauth_id": "Lorem",
            "oauth_code": "Lorem"
        }
    ]
}
```

##### 2.2.2.1.6 **brand** Target Script

```
use clinic_central_auth;

db.createCollection("brand", {
    "capped": false,
    "validator": {
        "$jsonSchema": {
            "bsonType": "object",
            "title": "Brand",
            "properties": {
                "_id": {
                    "bsonType": "objectId"
                },
                "+ refresh_token": {
                    "bsonType": "string",
                    "title": "+ Refresh Token"
                },
                "+ account": {
                    "bsonType": "array",
                    "title": "+ OAuth Account",
                    "additionalItems": true,
                    "items": {
                        "bsonType": "object",
                        "properties": {
                            "oauth_id": {
                                "bsonType": "string",
                                "title": "OAuth Id"
                            },
                            "oauth_code": {
                                "bsonType": "string",
                                "title": "OAuth Code"
                            }
                        },
                        "additionalProperties": false
                    }
                }
            },
            "additionalProperties": false
        }
    },
    "validationLevel": "off",
    "validationAction": "warn"
});
```

### <a id="11dcebf5-9936-4ccf-a530-730dd01db5e9"></a>2.2.2.2 Collection **central\_auth\_view**

##### 2.2.2.2.1 **central\_auth\_view** Tree Diagram

![Hackolade image](/assets/image58.png?raw=true)

##### 2.2.2.2.2 **central\_auth\_view** Properties

<table class="collection-properties-table"><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Collection name</td><td>Central Auth View</td></tr><tr><td>Technical name</td><td>central_auth_view</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>$ref</td><td></td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Database</td><td><a href=#a980dbf3-04b3-4a12-a7b0-6a9a116d207c><span class="name-container">clinic_central_auth</span></a></td></tr><tr><td>Capped</td><td></td></tr><tr><td>Time series</td><td></td></tr><tr><td>Size</td><td></td></tr><tr><td>Max</td><td></td></tr><tr><td>Storage engine</td><td>WiredTiger</td></tr><tr><td>Config String</td><td></td></tr><tr><td>Validation level</td><td>Off</td></tr><tr><td>Validation action</td><td>Warn</td></tr><tr><td>Encryption metadata</td><td></td></tr><tr><td>Additional properties</td><td>false</td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.2.2.2.3 **central\_auth\_view** Fields

<table><thead><tr><td>Field</td><td>Type</td><td>Req</td><td>Key</td><td>Description</td><td>Comments</td></tr></thead><tbody><tr><td><a href=#5d7f1c1a-df52-46ac-8821-b2fbbf8fb1e8 class="margin-0">_id</a></td><td class="no-break-word">objectId</td><td>false</td><td>pk</td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#79637b56-ad98-4655-ab52-2468bb89dec3 class="margin-0">email</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#0f6a0652-5122-44f7-a454-00d7509e9b9a class="margin-0">password</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#28e725c2-2476-463e-b617-bafffd5fe067 class="margin-0">role</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#1710252a-bf50-4ee5-bbd0-e901cfd9c63d class="margin-0">refresh_token</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#9838b088-3b52-437f-ba97-a37c5a9ce138 class="margin-0">account</a></td><td class="no-break-word">array</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#8f02a095-1789-4cbe-9275-4da89f86299e class="margin-5">[0]</a></td><td class="no-break-word">document</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#81cac4f8-4c2a-4488-9b38-a0fb7b599da8 class="margin-10">oauth_id</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#f1e14eec-f64b-4537-a08c-566aa57a584f class="margin-10">oauth_code</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#6ca6b5f2-4e1a-4bf8-877b-45ead53a4230 class="margin-0">is_active</a></td><td class="no-break-word">boolean</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="5d7f1c1a-df52-46ac-8821-b2fbbf8fb1e8"></a>2.2.2.2.3.1 Field **\_id**

##### 2.2.2.2.3.1.1 **\_id** Tree Diagram

![Hackolade image](/assets/image59.png?raw=true)

##### 2.2.2.2.3.1.2 **\_id** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Id</td></tr><tr><td>Technical name</td><td>_id</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>objectId</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td>false</td></tr><tr><td>Primary key</td><td>true</td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="79637b56-ad98-4655-ab52-2468bb89dec3"></a>2.2.2.2.3.2 Field **email**

##### 2.2.2.2.3.2.1 **email** Tree Diagram

![Hackolade image](/assets/image60.png?raw=true)

##### 2.2.2.2.3.2.2 **email** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Email</td></tr><tr><td>Technical name</td><td>email</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="0f6a0652-5122-44f7-a454-00d7509e9b9a"></a>2.2.2.2.3.3 Field **password**

##### 2.2.2.2.3.3.1 **password** Tree Diagram

![Hackolade image](/assets/image61.png?raw=true)

##### 2.2.2.2.3.3.2 **password** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Password</td></tr><tr><td>Technical name</td><td>password</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="28e725c2-2476-463e-b617-bafffd5fe067"></a>2.2.2.2.3.4 Field **role**

##### 2.2.2.2.3.4.1 **role** Tree Diagram

![Hackolade image](/assets/image62.png?raw=true)

##### 2.2.2.2.3.4.2 **role** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Role</td></tr><tr><td>Technical name</td><td>role</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td>brand,merchant,clinic</td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="1710252a-bf50-4ee5-bbd0-e901cfd9c63d"></a>2.2.2.2.3.5 Field **refresh\_token**

##### 2.2.2.2.3.5.1 **refresh\_token** Tree Diagram

![Hackolade image](/assets/image63.png?raw=true)

##### 2.2.2.2.3.5.2 **refresh\_token** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Refresh Token</td></tr><tr><td>Technical name</td><td>refresh_token</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="9838b088-3b52-437f-ba97-a37c5a9ce138"></a>2.2.2.2.3.6 Field **account**

##### 2.2.2.2.3.6.1 **account** Tree Diagram

![Hackolade image](/assets/image64.png?raw=true)

##### 2.2.2.2.3.6.2 **account** Hierarchy

Parent field: **central\_auth\_view**

Child field(s):

<table class="field-properties-table"><thead><tr><td>Field</td><td>Type</td><td>Req</td><td>Key</td><td>Description</td><td>Comments</td></tr></thead><tbody><tr><td><a href=#8f02a095-1789-4cbe-9275-4da89f86299e class="margin-NaN">[0]</a></td><td class="no-break-word">document</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.2.2.2.3.6.3 **account** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>OAuth Account</td></tr><tr><td>Technical name</td><td>account</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>array</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Min items</td><td></td></tr><tr><td>Max items</td><td></td></tr><tr><td>Unique items</td><td></td></tr><tr><td>Additional items</td><td>true</td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="8f02a095-1789-4cbe-9275-4da89f86299e"></a>2.2.2.2.3.7 Field **\[0\]**

##### 2.2.2.2.3.7.1 **\[0\]** Tree Diagram

![Hackolade image](/assets/image65.png?raw=true)

##### 2.2.2.2.3.7.2 **\[0\]** Hierarchy

Parent field: **account**

Child field(s):

<table class="field-properties-table"><thead><tr><td>Field</td><td>Type</td><td>Req</td><td>Key</td><td>Description</td><td>Comments</td></tr></thead><tbody><tr><td><a href=#81cac4f8-4c2a-4488-9b38-a0fb7b599da8 class="margin-NaN">OAuth&nbsp;Id</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#f1e14eec-f64b-4537-a08c-566aa57a584f class="margin-NaN">OAuth&nbsp;Code</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.2.2.2.3.7.3 **\[0\]** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Display name</td><td></td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>document</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>DBRef</td><td></td></tr><tr><td>Min Properties</td><td></td></tr><tr><td>Max Properties</td><td></td></tr><tr><td>Additional properties</td><td>false</td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="81cac4f8-4c2a-4488-9b38-a0fb7b599da8"></a>2.2.2.2.3.8 Field **oauth\_id**

##### 2.2.2.2.3.8.1 **oauth\_id** Tree Diagram

![Hackolade image](/assets/image66.png?raw=true)

##### 2.2.2.2.3.8.2 **oauth\_id** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>OAuth Id</td></tr><tr><td>Technical name</td><td>oauth_id</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="f1e14eec-f64b-4537-a08c-566aa57a584f"></a>2.2.2.2.3.9 Field **oauth\_code**

##### 2.2.2.2.3.9.1 **oauth\_code** Tree Diagram

![Hackolade image](/assets/image67.png?raw=true)

##### 2.2.2.2.3.9.2 **oauth\_code** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>OAuth Code</td></tr><tr><td>Technical name</td><td>oauth_code</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="6ca6b5f2-4e1a-4bf8-877b-45ead53a4230"></a>2.2.2.2.3.10 Field **is\_active**

##### 2.2.2.2.3.10.1 **is\_active** Tree Diagram

![Hackolade image](/assets/image68.png?raw=true)

##### 2.2.2.2.3.10.2 **is\_active** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Is Active?</td></tr><tr><td>Technical name</td><td>is_active</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>boolean</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.2.2.2.4 **central\_auth\_view** JSON Schema

```
{
    "$schema": "http://json-schema.org/draft-04/schema#",
    "type": "object",
    "title": "Central Auth View",
    "properties": {
        "_id": {
            "type": "string",
            "title": "Id",
            "pattern": "^[a-fA-F0-9]{24}$"
        },
        "email": {
            "type": "string",
            "title": "Email"
        },
        "password": {
            "type": "string",
            "title": "Password"
        },
        "role": {
            "type": "string",
            "title": "Role",
            "enum": [
                "brand",
                "merchant",
                "clinic"
            ]
        },
        "refresh_token": {
            "type": "string",
            "title": "Refresh Token"
        },
        "account": {
            "type": "array",
            "title": "OAuth Account",
            "additionalItems": true,
            "items": {
                "type": "object",
                "properties": {
                    "oauth_id": {
                        "type": "string",
                        "title": "OAuth Id"
                    },
                    "oauth_code": {
                        "type": "string",
                        "title": "OAuth Code"
                    }
                },
                "additionalProperties": false
            }
        },
        "is_active": {
            "type": "boolean",
            "title": "Is Active?"
        }
    },
    "additionalProperties": false
}
```

##### 2.2.2.2.5 **central\_auth\_view** JSON data

```
{
    "_id": ObjectId("4be7cf5cb7bf535ec0dc7b74"),
    "email": "Lorem",
    "password": "Lorem",
    "role": "clinic",
    "refresh_token": "Lorem",
    "account": [
        {
            "oauth_id": "Lorem",
            "oauth_code": "Lorem"
        }
    ],
    "is_active": true
}
```

##### 2.2.2.2.6 **central\_auth\_view** Target Script

```
use clinic_central_auth;

db.createCollection("central_auth_view", {
    "capped": false,
    "validator": {
        "$jsonSchema": {
            "bsonType": "object",
            "title": "Central Auth View",
            "properties": {
                "_id": {
                    "bsonType": "objectId",
                    "title": "Id"
                },
                "email": {
                    "bsonType": "string",
                    "title": "Email"
                },
                "password": {
                    "bsonType": "string",
                    "title": "Password"
                },
                "role": {
                    "bsonType": "string",
                    "title": "Role",
                    "enum": [
                        "brand",
                        "merchant",
                        "clinic"
                    ]
                },
                "refresh_token": {
                    "bsonType": "string",
                    "title": "Refresh Token"
                },
                "account": {
                    "bsonType": "array",
                    "title": "OAuth Account",
                    "additionalItems": true,
                    "items": {
                        "bsonType": "object",
                        "properties": {
                            "oauth_id": {
                                "bsonType": "string",
                                "title": "OAuth Id"
                            },
                            "oauth_code": {
                                "bsonType": "string",
                                "title": "OAuth Code"
                            }
                        },
                        "additionalProperties": false
                    }
                },
                "is_active": {
                    "bsonType": "bool",
                    "title": "Is Active?"
                }
            },
            "additionalProperties": false
        }
    },
    "validationLevel": "off",
    "validationAction": "warn"
});
```

### <a id="2eac7b06-e370-4e3b-b145-3d6a77c998f0"></a>2.2.2.3 Collection **clinic**

##### 2.2.2.3.1 **clinic** Tree Diagram

![Hackolade image](/assets/image69.png?raw=true)

##### 2.2.2.3.2 **clinic** Properties

<table class="collection-properties-table"><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Collection name</td><td>Clinic</td></tr><tr><td>Technical name</td><td>clinic</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>$ref</td><td></td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Database</td><td><a href=#a980dbf3-04b3-4a12-a7b0-6a9a116d207c><span class="name-container">clinic_central_auth</span></a></td></tr><tr><td>Capped</td><td></td></tr><tr><td>Time series</td><td></td></tr><tr><td>Size</td><td></td></tr><tr><td>Max</td><td></td></tr><tr><td>Storage engine</td><td>WiredTiger</td></tr><tr><td>Config String</td><td></td></tr><tr><td>Validation level</td><td>Off</td></tr><tr><td>Validation action</td><td>Warn</td></tr><tr><td>Encryption metadata</td><td></td></tr><tr><td>Additional properties</td><td>false</td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.2.2.3.3 **clinic** Fields

<table><thead><tr><td>Field</td><td>Type</td><td>Req</td><td>Key</td><td>Description</td><td>Comments</td></tr></thead><tbody><tr><td><a href=#18a3f513-8a58-4699-8fcb-aac464404761 class="margin-0">+&nbsp;refresh_token</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#63f15d50-f436-4e75-9ec2-92a476e3367a class="margin-0">+&nbsp;account</a></td><td class="no-break-word">array</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#2a4b927c-145e-4c77-be63-52f190ebbca6 class="margin-5">[0]</a></td><td class="no-break-word">document</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#41f8d083-ce76-46a6-98ef-83f8a88d4bc3 class="margin-10">oauth_id</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#805d9b3f-299b-4f15-a8ad-5aa171732272 class="margin-10">oauth_code</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="18a3f513-8a58-4699-8fcb-aac464404761"></a>2.2.2.3.3.1 Field **\+ refresh\_token**

##### 2.2.2.3.3.1.1 **\+ refresh\_token** Tree Diagram

![Hackolade image](/assets/image70.png?raw=true)

##### 2.2.2.3.3.1.2 **\+ refresh\_token** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>+ Refresh Token</td></tr><tr><td>Technical name</td><td>+ refresh_token</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="63f15d50-f436-4e75-9ec2-92a476e3367a"></a>2.2.2.3.3.2 Field **\+ account**

##### 2.2.2.3.3.2.1 **\+ account** Tree Diagram

![Hackolade image](/assets/image71.png?raw=true)

##### 2.2.2.3.3.2.2 **\+ account** Hierarchy

Parent field: **clinic**

Child field(s):

<table class="field-properties-table"><thead><tr><td>Field</td><td>Type</td><td>Req</td><td>Key</td><td>Description</td><td>Comments</td></tr></thead><tbody><tr><td><a href=#2a4b927c-145e-4c77-be63-52f190ebbca6 class="margin-NaN">[0]</a></td><td class="no-break-word">document</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.2.2.3.3.2.3 **\+ account** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>+ OAuth Account</td></tr><tr><td>Technical name</td><td>+ account</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>array</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Min items</td><td></td></tr><tr><td>Max items</td><td></td></tr><tr><td>Unique items</td><td></td></tr><tr><td>Additional items</td><td>true</td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="2a4b927c-145e-4c77-be63-52f190ebbca6"></a>2.2.2.3.3.3 Field **\[0\]**

##### 2.2.2.3.3.3.1 **\[0\]** Tree Diagram

![Hackolade image](/assets/image72.png?raw=true)

##### 2.2.2.3.3.3.2 **\[0\]** Hierarchy

Parent field: **+ account**

Child field(s):

<table class="field-properties-table"><thead><tr><td>Field</td><td>Type</td><td>Req</td><td>Key</td><td>Description</td><td>Comments</td></tr></thead><tbody><tr><td><a href=#41f8d083-ce76-46a6-98ef-83f8a88d4bc3 class="margin-NaN">OAuth&nbsp;Id</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#805d9b3f-299b-4f15-a8ad-5aa171732272 class="margin-NaN">OAuth&nbsp;Code</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.2.2.3.3.3.3 **\[0\]** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Display name</td><td></td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>document</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>DBRef</td><td></td></tr><tr><td>Min Properties</td><td></td></tr><tr><td>Max Properties</td><td></td></tr><tr><td>Additional properties</td><td>false</td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="41f8d083-ce76-46a6-98ef-83f8a88d4bc3"></a>2.2.2.3.3.4 Field **oauth\_id**

##### 2.2.2.3.3.4.1 **oauth\_id** Tree Diagram

![Hackolade image](/assets/image73.png?raw=true)

##### 2.2.2.3.3.4.2 **oauth\_id** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>OAuth Id</td></tr><tr><td>Technical name</td><td>oauth_id</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="805d9b3f-299b-4f15-a8ad-5aa171732272"></a>2.2.2.3.3.5 Field **oauth\_code**

##### 2.2.2.3.3.5.1 **oauth\_code** Tree Diagram

![Hackolade image](/assets/image74.png?raw=true)

##### 2.2.2.3.3.5.2 **oauth\_code** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>OAuth Code</td></tr><tr><td>Technical name</td><td>oauth_code</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.2.2.3.4 **clinic** JSON Schema

```
{
    "$schema": "http://json-schema.org/draft-04/schema#",
    "type": "object",
    "title": "Clinic",
    "properties": {
        "+ refresh_token": {
            "type": "string",
            "title": "+ Refresh Token"
        },
        "+ account": {
            "type": "array",
            "title": "+ OAuth Account",
            "additionalItems": true,
            "items": {
                "type": "object",
                "properties": {
                    "oauth_id": {
                        "type": "string",
                        "title": "OAuth Id"
                    },
                    "oauth_code": {
                        "type": "string",
                        "title": "OAuth Code"
                    }
                },
                "additionalProperties": false
            }
        }
    },
    "additionalProperties": false
}
```

##### 2.2.2.3.5 **clinic** JSON data

```
{
    "+ refresh_token": "Lorem",
    "+ account": [
        {
            "oauth_id": "Lorem",
            "oauth_code": "Lorem"
        }
    ]
}
```

##### 2.2.2.3.6 **clinic** Target Script

```
use clinic_central_auth;

db.createCollection("clinic", {
    "capped": false,
    "validator": {
        "$jsonSchema": {
            "bsonType": "object",
            "title": "Clinic",
            "properties": {
                "_id": {
                    "bsonType": "objectId"
                },
                "+ refresh_token": {
                    "bsonType": "string",
                    "title": "+ Refresh Token"
                },
                "+ account": {
                    "bsonType": "array",
                    "title": "+ OAuth Account",
                    "additionalItems": true,
                    "items": {
                        "bsonType": "object",
                        "properties": {
                            "oauth_id": {
                                "bsonType": "string",
                                "title": "OAuth Id"
                            },
                            "oauth_code": {
                                "bsonType": "string",
                                "title": "OAuth Code"
                            }
                        },
                        "additionalProperties": false
                    }
                }
            },
            "additionalProperties": false
        }
    },
    "validationLevel": "off",
    "validationAction": "warn"
});
```

### <a id="262baf94-3e39-4a4b-9fdd-9989aa11b9c1"></a>2.2.2.4 Collection **merchant**

##### 2.2.2.4.1 **merchant** Tree Diagram

![Hackolade image](/assets/image75.png?raw=true)

##### 2.2.2.4.2 **merchant** Properties

<table class="collection-properties-table"><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Collection name</td><td>Merchant</td></tr><tr><td>Technical name</td><td>merchant</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>$ref</td><td></td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Database</td><td><a href=#a980dbf3-04b3-4a12-a7b0-6a9a116d207c><span class="name-container">clinic_central_auth</span></a></td></tr><tr><td>Capped</td><td></td></tr><tr><td>Time series</td><td></td></tr><tr><td>Size</td><td></td></tr><tr><td>Max</td><td></td></tr><tr><td>Storage engine</td><td>WiredTiger</td></tr><tr><td>Config String</td><td></td></tr><tr><td>Validation level</td><td>Off</td></tr><tr><td>Validation action</td><td>Warn</td></tr><tr><td>Encryption metadata</td><td></td></tr><tr><td>Additional properties</td><td>false</td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.2.2.4.3 **merchant** Fields

<table><thead><tr><td>Field</td><td>Type</td><td>Req</td><td>Key</td><td>Description</td><td>Comments</td></tr></thead><tbody><tr><td><a href=#28155a93-7052-4a5f-9b69-6e1c1d7a859f class="margin-0">+&nbsp;refresh&nbsp;token</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#5ee601be-920f-4720-8160-1c31e9961ffa class="margin-0">+&nbsp;account</a></td><td class="no-break-word">array</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#9ab7d652-803f-48ba-ac4f-5d35ce4d123b class="margin-5">[0]</a></td><td class="no-break-word">document</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#d4d168e1-82c0-49ff-8993-55cba61f6f2a class="margin-10">oauth_id</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#08eb6603-8094-4a70-b156-5c0d79546b36 class="margin-10">oauth_code</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="28155a93-7052-4a5f-9b69-6e1c1d7a859f"></a>2.2.2.4.3.1 Field **\+ refresh token**

##### 2.2.2.4.3.1.1 **\+ refresh token** Tree Diagram

![Hackolade image](/assets/image76.png?raw=true)

##### 2.2.2.4.3.1.2 **\+ refresh token** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>+ Refresh Token</td></tr><tr><td>Technical name</td><td>+ refresh token</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="5ee601be-920f-4720-8160-1c31e9961ffa"></a>2.2.2.4.3.2 Field **\+ account**

##### 2.2.2.4.3.2.1 **\+ account** Tree Diagram

![Hackolade image](/assets/image77.png?raw=true)

##### 2.2.2.4.3.2.2 **\+ account** Hierarchy

Parent field: **merchant**

Child field(s):

<table class="field-properties-table"><thead><tr><td>Field</td><td>Type</td><td>Req</td><td>Key</td><td>Description</td><td>Comments</td></tr></thead><tbody><tr><td><a href=#9ab7d652-803f-48ba-ac4f-5d35ce4d123b class="margin-NaN">[0]</a></td><td class="no-break-word">document</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.2.2.4.3.2.3 **\+ account** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>+ OAuth Account</td></tr><tr><td>Technical name</td><td>+ account</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>array</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Min items</td><td></td></tr><tr><td>Max items</td><td></td></tr><tr><td>Unique items</td><td></td></tr><tr><td>Additional items</td><td>true</td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="9ab7d652-803f-48ba-ac4f-5d35ce4d123b"></a>2.2.2.4.3.3 Field **\[0\]**

##### 2.2.2.4.3.3.1 **\[0\]** Tree Diagram

![Hackolade image](/assets/image78.png?raw=true)

##### 2.2.2.4.3.3.2 **\[0\]** Hierarchy

Parent field: **+ account**

Child field(s):

<table class="field-properties-table"><thead><tr><td>Field</td><td>Type</td><td>Req</td><td>Key</td><td>Description</td><td>Comments</td></tr></thead><tbody><tr><td><a href=#d4d168e1-82c0-49ff-8993-55cba61f6f2a class="margin-NaN">OAuth&nbsp;Id</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#08eb6603-8094-4a70-b156-5c0d79546b36 class="margin-NaN">OAuth&nbsp;Code</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.2.2.4.3.3.3 **\[0\]** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Display name</td><td></td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>document</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>DBRef</td><td></td></tr><tr><td>Min Properties</td><td></td></tr><tr><td>Max Properties</td><td></td></tr><tr><td>Additional properties</td><td>false</td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="d4d168e1-82c0-49ff-8993-55cba61f6f2a"></a>2.2.2.4.3.4 Field **oauth\_id**

##### 2.2.2.4.3.4.1 **oauth\_id** Tree Diagram

![Hackolade image](/assets/image79.png?raw=true)

##### 2.2.2.4.3.4.2 **oauth\_id** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>OAuth Id</td></tr><tr><td>Technical name</td><td>oauth_id</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="08eb6603-8094-4a70-b156-5c0d79546b36"></a>2.2.2.4.3.5 Field **oauth\_code**

##### 2.2.2.4.3.5.1 **oauth\_code** Tree Diagram

![Hackolade image](/assets/image80.png?raw=true)

##### 2.2.2.4.3.5.2 **oauth\_code** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>OAuth Code</td></tr><tr><td>Technical name</td><td>oauth_code</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.2.2.4.4 **merchant** JSON Schema

```
{
    "$schema": "http://json-schema.org/draft-04/schema#",
    "type": "object",
    "title": "Merchant",
    "properties": {
        "+ refresh token": {
            "type": "string",
            "title": "+ Refresh Token"
        },
        "+ account": {
            "type": "array",
            "title": "+ OAuth Account",
            "additionalItems": true,
            "items": {
                "type": "object",
                "properties": {
                    "oauth_id": {
                        "type": "string",
                        "title": "OAuth Id"
                    },
                    "oauth_code": {
                        "type": "string",
                        "title": "OAuth Code"
                    }
                },
                "additionalProperties": false
            }
        }
    },
    "additionalProperties": false
}
```

##### 2.2.2.4.5 **merchant** JSON data

```
{
    "+ refresh token": "Lorem",
    "+ account": [
        {
            "oauth_id": "Lorem",
            "oauth_code": "Lorem"
        }
    ]
}
```

##### 2.2.2.4.6 **merchant** Target Script

```
use clinic_central_auth;

db.createCollection("merchant", {
    "capped": false,
    "validator": {
        "$jsonSchema": {
            "bsonType": "object",
            "title": "Merchant",
            "properties": {
                "_id": {
                    "bsonType": "objectId"
                },
                "+ refresh token": {
                    "bsonType": "string",
                    "title": "+ Refresh Token"
                },
                "+ account": {
                    "bsonType": "array",
                    "title": "+ OAuth Account",
                    "additionalItems": true,
                    "items": {
                        "bsonType": "object",
                        "properties": {
                            "oauth_id": {
                                "bsonType": "string",
                                "title": "OAuth Id"
                            },
                            "oauth_code": {
                                "bsonType": "string",
                                "title": "OAuth Code"
                            }
                        },
                        "additionalProperties": false
                    }
                }
            },
            "additionalProperties": false
        }
    },
    "validationLevel": "off",
    "validationAction": "warn"
});
```

### <a id="15bed2f5-714c-42eb-8540-90b101910d8b"></a>2.3 Database **doctor\_management**

![Hackolade image](/assets/image81.png?raw=true)

##### 2.3.1 **doctor\_management** Properties

<table class="collection-properties-table"><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Database name</td><td>Doctor Management</td></tr><tr><td>Technical name</td><td>doctor_management</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Enable sharding</td><td></td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="15bed2f5-714c-42eb-8540-90b101910d8b-children"></a>2.3.2 **doctor\_management** Collections

### <a id="56d72f17-dfa0-4234-9b36-66a8d2bc45dd"></a>2.3.2.1 Collection **doctor**

##### 2.3.2.1.1 **doctor** Tree Diagram

![Hackolade image](/assets/image82.png?raw=true)

##### 2.3.2.1.2 **doctor** Properties

<table class="collection-properties-table"><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Collection name</td><td>Doctor</td></tr><tr><td>Technical name</td><td>doctor</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>$ref</td><td></td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Database</td><td><a href=#15bed2f5-714c-42eb-8540-90b101910d8b><span class="name-container">doctor_management</span></a></td></tr><tr><td>Capped</td><td></td></tr><tr><td>Time series</td><td></td></tr><tr><td>Size</td><td></td></tr><tr><td>Max</td><td></td></tr><tr><td>Storage engine</td><td>WiredTiger</td></tr><tr><td>Config String</td><td></td></tr><tr><td>Validation level</td><td>Off</td></tr><tr><td>Validation action</td><td>Warn</td></tr><tr><td>Encryption metadata</td><td></td></tr><tr><td>Additional properties</td><td>false</td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.3.2.1.3 **doctor** Fields

<table><thead><tr><td>Field</td><td>Type</td><td>Req</td><td>Key</td><td>Description</td><td>Comments</td></tr></thead><tbody><tr><td><a href=#3dc3ee6f-34b3-4997-82b2-3a6d9c03807a class="margin-0">_id</a></td><td class="no-break-word">objectId</td><td>false</td><td>pk, dk</td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#65b5f308-9db7-4d7f-9f6b-0bc42e2dc112 class="margin-0">brand_id</a></td><td class="no-break-word">objectId</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#a5bc4bab-6c41-4766-83c6-905180e66dfe class="margin-0">display_picture_id</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#5b729c4b-70a4-42f2-b3e7-2d85b8de36a7 class="margin-0">email</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#99c517e6-80d5-4a52-af94-38d9c86cb345 class="margin-0">password</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#8597241b-4301-47f7-a54e-41b1b02d9b0d class="margin-0">phone_number</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#1d8b019d-570a-47dc-a43e-4e47808914c5 class="margin-0">full_name</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#1e34dcea-3ecf-4108-a58e-ac292e1c3fca class="margin-0">date_of_birth</a></td><td class="no-break-word">date-time</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#f017e286-cd92-40a6-9f1e-05cb9c71ea93 class="margin-0">gender</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#38a2006b-dfdc-4827-8a8c-44c2d5ac2862 class="margin-0">specialization</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#c43b616a-f23e-4591-b6d0-aeae1a880bd8 class="margin-0">university</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#f167a854-dcc4-416f-a9f3-4bcdcb7e12dd class="margin-0">npwp_no</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#762717e5-9dd3-4136-997e-bf7e90ac1a59 class="margin-0">str_no</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#85d867d6-69b2-4a12-98ac-0d6e4a03260c class="margin-0">sip_no</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#982fe1c4-70b0-4f5c-93ae-cac0c0c22998 class="margin-0">document</a></td><td class="no-break-word">document</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#bc52a21d-1212-4ca9-9044-bdd16ebf18d3 class="margin-5">npwp_doc_id</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#5e030161-7af5-4cd0-a06b-1f6463b01bff class="margin-5">str_doc_id</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#81431f04-739e-4dd5-ae29-c353bf92fe71 class="margin-5">sip_doc_id</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#9def6f31-c2d2-4624-8b31-dc31c57dbc6a class="margin-0">created_date</a></td><td class="no-break-word">date-time</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#a52bff47-1ce2-40b1-a03f-767c7ad36832 class="margin-0">updated_date</a></td><td class="no-break-word">date-time</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#ae3573a4-90b9-43c2-b964-c4a30ea2eba3 class="margin-0">is_active</a></td><td class="no-break-word">boolean</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="3dc3ee6f-34b3-4997-82b2-3a6d9c03807a"></a>2.3.2.1.3.1 Field **\_id**

##### 2.3.2.1.3.1.1 **\_id** Tree Diagram

![Hackolade image](/assets/image83.png?raw=true)

##### 2.3.2.1.3.1.2 **\_id** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Id</td></tr><tr><td>Technical name</td><td>_id</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>objectId</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td>true</td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="65b5f308-9db7-4d7f-9f6b-0bc42e2dc112"></a>2.3.2.1.3.2 Field **brand\_id**

##### 2.3.2.1.3.2.1 **brand\_id** Tree Diagram

![Hackolade image](/assets/image84.png?raw=true)

##### 2.3.2.1.3.2.2 **brand\_id** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Brand Id</td></tr><tr><td>Technical name</td><td>brand_id</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>objectId</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="a5bc4bab-6c41-4766-83c6-905180e66dfe"></a>2.3.2.1.3.3 Field **display\_picture\_id**

##### 2.3.2.1.3.3.1 **display\_picture\_id** Tree Diagram

![Hackolade image](/assets/image85.png?raw=true)

##### 2.3.2.1.3.3.2 **display\_picture\_id** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Display Picture</td></tr><tr><td>Technical name</td><td>display_picture_id</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="5b729c4b-70a4-42f2-b3e7-2d85b8de36a7"></a>2.3.2.1.3.4 Field **email**

##### 2.3.2.1.3.4.1 **email** Tree Diagram

![Hackolade image](/assets/image86.png?raw=true)

##### 2.3.2.1.3.4.2 **email** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Email</td></tr><tr><td>Technical name</td><td>email</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="99c517e6-80d5-4a52-af94-38d9c86cb345"></a>2.3.2.1.3.5 Field **password**

##### 2.3.2.1.3.5.1 **password** Tree Diagram

![Hackolade image](/assets/image87.png?raw=true)

##### 2.3.2.1.3.5.2 **password** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Password</td></tr><tr><td>Technical name</td><td>password</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="8597241b-4301-47f7-a54e-41b1b02d9b0d"></a>2.3.2.1.3.6 Field **phone\_number**

##### 2.3.2.1.3.6.1 **phone\_number** Tree Diagram

![Hackolade image](/assets/image88.png?raw=true)

##### 2.3.2.1.3.6.2 **phone\_number** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Phone Number</td></tr><tr><td>Technical name</td><td>phone_number</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="1d8b019d-570a-47dc-a43e-4e47808914c5"></a>2.3.2.1.3.7 Field **full\_name**

##### 2.3.2.1.3.7.1 **full\_name** Tree Diagram

![Hackolade image](/assets/image89.png?raw=true)

##### 2.3.2.1.3.7.2 **full\_name** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Full Name</td></tr><tr><td>Technical name</td><td>full_name</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="1e34dcea-3ecf-4108-a58e-ac292e1c3fca"></a>2.3.2.1.3.8 Field **date\_of\_birth**

##### 2.3.2.1.3.8.1 **date\_of\_birth** Tree Diagram

![Hackolade image](/assets/image90.png?raw=true)

##### 2.3.2.1.3.8.2 **date\_of\_birth** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Date of Birth</td></tr><tr><td>Technical name</td><td>date_of_birth</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td>date-time</td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="f017e286-cd92-40a6-9f1e-05cb9c71ea93"></a>2.3.2.1.3.9 Field **gender**

##### 2.3.2.1.3.9.1 **gender** Tree Diagram

![Hackolade image](/assets/image91.png?raw=true)

##### 2.3.2.1.3.9.2 **gender** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Gender</td></tr><tr><td>Technical name</td><td>gender</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="38a2006b-dfdc-4827-8a8c-44c2d5ac2862"></a>2.3.2.1.3.10 Field **specialization**

##### 2.3.2.1.3.10.1 **specialization** Tree Diagram

![Hackolade image](/assets/image92.png?raw=true)

##### 2.3.2.1.3.10.2 **specialization** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Specialization</td></tr><tr><td>Technical name</td><td>specialization</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="c43b616a-f23e-4591-b6d0-aeae1a880bd8"></a>2.3.2.1.3.11 Field **university**

##### 2.3.2.1.3.11.1 **university** Tree Diagram

![Hackolade image](/assets/image93.png?raw=true)

##### 2.3.2.1.3.11.2 **university** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>University</td></tr><tr><td>Technical name</td><td>university</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="f167a854-dcc4-416f-a9f3-4bcdcb7e12dd"></a>2.3.2.1.3.12 Field **npwp\_no**

##### 2.3.2.1.3.12.1 **npwp\_no** Tree Diagram

![Hackolade image](/assets/image94.png?raw=true)

##### 2.3.2.1.3.12.2 **npwp\_no** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>NPWP Number</td></tr><tr><td>Technical name</td><td>npwp_no</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="762717e5-9dd3-4136-997e-bf7e90ac1a59"></a>2.3.2.1.3.13 Field **str\_no**

##### 2.3.2.1.3.13.1 **str\_no** Tree Diagram

![Hackolade image](/assets/image95.png?raw=true)

##### 2.3.2.1.3.13.2 **str\_no** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>STR Number</td></tr><tr><td>Technical name</td><td>str_no</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="85d867d6-69b2-4a12-98ac-0d6e4a03260c"></a>2.3.2.1.3.14 Field **sip\_no**

##### 2.3.2.1.3.14.1 **sip\_no** Tree Diagram

![Hackolade image](/assets/image96.png?raw=true)

##### 2.3.2.1.3.14.2 **sip\_no** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>SIP Number</td></tr><tr><td>Technical name</td><td>sip_no</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="982fe1c4-70b0-4f5c-93ae-cac0c0c22998"></a>2.3.2.1.3.15 Field **document**

##### 2.3.2.1.3.15.1 **document** Tree Diagram

![Hackolade image](/assets/image97.png?raw=true)

##### 2.3.2.1.3.15.2 **document** Hierarchy

Parent field: **doctor**

Child field(s):

<table class="field-properties-table"><thead><tr><td>Field</td><td>Type</td><td>Req</td><td>Key</td><td>Description</td><td>Comments</td></tr></thead><tbody><tr><td><a href=#bc52a21d-1212-4ca9-9044-bdd16ebf18d3 class="margin-NaN">NPWP</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#5e030161-7af5-4cd0-a06b-1f6463b01bff class="margin-NaN">STR</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#81431f04-739e-4dd5-ae29-c353bf92fe71 class="margin-NaN">SIP</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.3.2.1.3.15.3 **document** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Document</td></tr><tr><td>Technical name</td><td>document</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>document</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>DBRef</td><td></td></tr><tr><td>Min Properties</td><td></td></tr><tr><td>Max Properties</td><td></td></tr><tr><td>Additional properties</td><td>false</td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="bc52a21d-1212-4ca9-9044-bdd16ebf18d3"></a>2.3.2.1.3.16 Field **npwp\_doc\_id**

##### 2.3.2.1.3.16.1 **npwp\_doc\_id** Tree Diagram

![Hackolade image](/assets/image98.png?raw=true)

##### 2.3.2.1.3.16.2 **npwp\_doc\_id** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>NPWP</td></tr><tr><td>Technical name</td><td>npwp_doc_id</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="5e030161-7af5-4cd0-a06b-1f6463b01bff"></a>2.3.2.1.3.17 Field **str\_doc\_id**

##### 2.3.2.1.3.17.1 **str\_doc\_id** Tree Diagram

![Hackolade image](/assets/image99.png?raw=true)

##### 2.3.2.1.3.17.2 **str\_doc\_id** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>STR</td></tr><tr><td>Technical name</td><td>str_doc_id</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="81431f04-739e-4dd5-ae29-c353bf92fe71"></a>2.3.2.1.3.18 Field **sip\_doc\_id**

##### 2.3.2.1.3.18.1 **sip\_doc\_id** Tree Diagram

![Hackolade image](/assets/image100.png?raw=true)

##### 2.3.2.1.3.18.2 **sip\_doc\_id** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>SIP</td></tr><tr><td>Technical name</td><td>sip_doc_id</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="9def6f31-c2d2-4624-8b31-dc31c57dbc6a"></a>2.3.2.1.3.19 Field **created\_date**

##### 2.3.2.1.3.19.1 **created\_date** Tree Diagram

![Hackolade image](/assets/image101.png?raw=true)

##### 2.3.2.1.3.19.2 **created\_date** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Created Date</td></tr><tr><td>Technical name</td><td>created_date</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td>date-time</td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td>false</td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="a52bff47-1ce2-40b1-a03f-767c7ad36832"></a>2.3.2.1.3.20 Field **updated\_date**

##### 2.3.2.1.3.20.1 **updated\_date** Tree Diagram

![Hackolade image](/assets/image102.png?raw=true)

##### 2.3.2.1.3.20.2 **updated\_date** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Updated Date</td></tr><tr><td>Technical name</td><td>updated_date</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td>date-time</td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td>false</td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="ae3573a4-90b9-43c2-b964-c4a30ea2eba3"></a>2.3.2.1.3.21 Field **is\_active**

##### 2.3.2.1.3.21.1 **is\_active** Tree Diagram

![Hackolade image](/assets/image103.png?raw=true)

##### 2.3.2.1.3.21.2 **is\_active** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Is Active?</td></tr><tr><td>Technical name</td><td>is_active</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>boolean</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.3.2.1.4 **doctor** JSON Schema

```
{
    "$schema": "http://json-schema.org/draft-04/schema#",
    "type": "object",
    "title": "Doctor",
    "properties": {
        "_id": {
            "type": "string",
            "title": "Id",
            "pattern": "^[a-fA-F0-9]{24}$"
        },
        "brand_id": {
            "type": "string",
            "title": "Brand Id",
            "pattern": "^[a-fA-F0-9]{24}$"
        },
        "display_picture_id": {
            "type": "string",
            "title": "Display Picture"
        },
        "email": {
            "type": "string",
            "title": "Email"
        },
        "password": {
            "type": "string",
            "title": "Password"
        },
        "phone_number": {
            "type": "string",
            "title": "Phone Number"
        },
        "full_name": {
            "type": "string",
            "title": "Full Name"
        },
        "date_of_birth": {
            "type": "string",
            "title": "Date of Birth",
            "format": "date-time"
        },
        "gender": {
            "type": "string",
            "title": "Gender"
        },
        "specialization": {
            "type": "string",
            "title": "Specialization"
        },
        "university": {
            "type": "string",
            "title": "University"
        },
        "npwp_no": {
            "type": "string",
            "title": "NPWP Number"
        },
        "str_no": {
            "type": "string",
            "title": "STR Number"
        },
        "sip_no": {
            "type": "string",
            "title": "SIP Number"
        },
        "document": {
            "type": "object",
            "title": "Document",
            "properties": {
                "npwp_doc_id": {
                    "type": "string",
                    "title": "NPWP"
                },
                "str_doc_id": {
                    "type": "string",
                    "title": "STR"
                },
                "sip_doc_id": {
                    "type": "string",
                    "title": "SIP"
                }
            },
            "additionalProperties": false
        },
        "created_date": {
            "type": "string",
            "title": "Created Date",
            "format": "date-time"
        },
        "updated_date": {
            "type": "string",
            "title": "Updated Date",
            "format": "date-time"
        },
        "is_active": {
            "type": "boolean",
            "title": "Is Active?"
        }
    },
    "additionalProperties": false
}
```

##### 2.3.2.1.5 **doctor** JSON data

```
{
    "_id": ObjectId("c478fa9dae1e5f0cd5f6aa8e"),
    "brand_id": ObjectId("dfed6f859d4efee5373d277e"),
    "display_picture_id": "Lorem",
    "email": "Lorem",
    "password": "Lorem",
    "phone_number": "Lorem",
    "full_name": "Lorem",
    "date_of_birth": "2011-06-14T04:12:36.123Z",
    "gender": "Lorem",
    "specialization": "Lorem",
    "university": "Lorem",
    "npwp_no": "Lorem",
    "str_no": "Lorem",
    "sip_no": "Lorem",
    "document": {
        "npwp_doc_id": "Lorem",
        "str_doc_id": "Lorem",
        "sip_doc_id": "Lorem"
    },
    "created_date": "2011-06-14T04:12:36.123Z",
    "updated_date": "2011-06-14T04:12:36.123Z",
    "is_active": true
}
```

##### 2.3.2.1.6 **doctor** Target Script

```
use doctor_management;

db.createCollection("doctor", {
    "capped": false,
    "validator": {
        "$jsonSchema": {
            "bsonType": "object",
            "title": "Doctor",
            "properties": {
                "_id": {
                    "bsonType": "objectId",
                    "title": "Id"
                },
                "brand_id": {
                    "bsonType": "objectId",
                    "title": "Brand Id"
                },
                "display_picture_id": {
                    "bsonType": "string",
                    "title": "Display Picture"
                },
                "email": {
                    "bsonType": "string",
                    "title": "Email"
                },
                "password": {
                    "bsonType": "string",
                    "title": "Password"
                },
                "phone_number": {
                    "bsonType": "string",
                    "title": "Phone Number"
                },
                "full_name": {
                    "bsonType": "string",
                    "title": "Full Name"
                },
                "date_of_birth": {
                    "bsonType": "string",
                    "title": "Date of Birth"
                },
                "gender": {
                    "bsonType": "string",
                    "title": "Gender"
                },
                "specialization": {
                    "bsonType": "string",
                    "title": "Specialization"
                },
                "university": {
                    "bsonType": "string",
                    "title": "University"
                },
                "npwp_no": {
                    "bsonType": "string",
                    "title": "NPWP Number"
                },
                "str_no": {
                    "bsonType": "string",
                    "title": "STR Number"
                },
                "sip_no": {
                    "bsonType": "string",
                    "title": "SIP Number"
                },
                "document": {
                    "bsonType": "object",
                    "title": "Document",
                    "properties": {
                        "npwp_doc_id": {
                            "bsonType": "string",
                            "title": "NPWP"
                        },
                        "str_doc_id": {
                            "bsonType": "string",
                            "title": "STR"
                        },
                        "sip_doc_id": {
                            "bsonType": "string",
                            "title": "SIP"
                        }
                    },
                    "additionalProperties": false
                },
                "created_date": {
                    "bsonType": "string",
                    "title": "Created Date"
                },
                "updated_date": {
                    "bsonType": "string",
                    "title": "Updated Date"
                },
                "is_active": {
                    "bsonType": "bool",
                    "title": "Is Active?"
                }
            },
            "additionalProperties": false
        }
    },
    "validationLevel": "off",
    "validationAction": "warn"
});
```

### <a id="0b999a7b-fa7b-419d-9f6a-ef6df8bdbac7"></a>2.3.2.2 Collection **magic\_token\_verif**

##### 2.3.2.2.1 **magic\_token\_verif** Tree Diagram

![Hackolade image](/assets/image104.png?raw=true)

##### 2.3.2.2.2 **magic\_token\_verif** Properties

<table class="collection-properties-table"><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Collection name</td><td>Magic Token Verification</td></tr><tr><td>Technical name</td><td>magic_token_verif</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>$ref</td><td></td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Database</td><td><a href=#15bed2f5-714c-42eb-8540-90b101910d8b><span class="name-container">doctor_management</span></a></td></tr><tr><td>Capped</td><td></td></tr><tr><td>Time series</td><td></td></tr><tr><td>Size</td><td></td></tr><tr><td>Max</td><td></td></tr><tr><td>Storage engine</td><td>WiredTiger</td></tr><tr><td>Config String</td><td></td></tr><tr><td>Validation level</td><td>Off</td></tr><tr><td>Validation action</td><td>Warn</td></tr><tr><td>Encryption metadata</td><td></td></tr><tr><td>Additional properties</td><td>false</td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.3.2.2.3 **magic\_token\_verif** Fields

<table><thead><tr><td>Field</td><td>Type</td><td>Req</td><td>Key</td><td>Description</td><td>Comments</td></tr></thead><tbody><tr><td><a href=#a2b6ea34-4fa8-406c-97d7-328716c3b3b2 class="margin-0">_id</a></td><td class="no-break-word">objectId</td><td>false</td><td>pk</td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#66f606d5-bfd4-4e58-9732-9a328dd9c0b7 class="margin-0">doctor_id</a></td><td class="no-break-word">objectId</td><td>false</td><td>fk</td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#f2987907-640b-4477-ac0c-44d80f3f0395 class="margin-0">magic_token</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#df521d3e-ab79-41d2-b219-da8f20d6a400 class="margin-0">is_used</a></td><td class="no-break-word">boolean</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#84406702-4752-49af-95e1-21533438b695 class="margin-0">expiration_date</a></td><td class="no-break-word">date-time</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="a2b6ea34-4fa8-406c-97d7-328716c3b3b2"></a>2.3.2.2.3.1 Field **\_id**

##### 2.3.2.2.3.1.1 **\_id** Tree Diagram

![Hackolade image](/assets/image105.png?raw=true)

##### 2.3.2.2.3.1.2 **\_id** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Id</td></tr><tr><td>Technical name</td><td>_id</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>objectId</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td>true</td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="66f606d5-bfd4-4e58-9732-9a328dd9c0b7"></a>2.3.2.2.3.2 Field **doctor\_id**

##### 2.3.2.2.3.2.1 **doctor\_id** Tree Diagram

![Hackolade image](/assets/image106.png?raw=true)

##### 2.3.2.2.3.2.2 **doctor\_id** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Doctor Id</td></tr><tr><td>Technical name</td><td>doctor_id</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td>false</td></tr><tr><td>Type</td><td>objectId</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Foreign collection</td><td><a href=#56d72f17-dfa0-4234-9b36-66a8d2bc45dd>doctor</a></td></tr><tr><td>Foreign field</td><td><a href=#3dc3ee6f-34b3-4997-82b2-3a6d9c03807a>_id</a></td></tr><tr><td>Relationship type</td><td>Foreign Key</td></tr><tr><td>Relationship name</td><td>fk Magic Token Verification(1).Doctor Id to Doctor.Id</td></tr><tr><td>Cardinality</td><td>1</td></tr><tr><td>Default</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="f2987907-640b-4477-ac0c-44d80f3f0395"></a>2.3.2.2.3.3 Field **magic\_token**

##### 2.3.2.2.3.3.1 **magic\_token** Tree Diagram

![Hackolade image](/assets/image107.png?raw=true)

##### 2.3.2.2.3.3.2 **magic\_token** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Magic Token</td></tr><tr><td>Technical name</td><td>magic_token</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="df521d3e-ab79-41d2-b219-da8f20d6a400"></a>2.3.2.2.3.4 Field **is\_used**

##### 2.3.2.2.3.4.1 **is\_used** Tree Diagram

![Hackolade image](/assets/image108.png?raw=true)

##### 2.3.2.2.3.4.2 **is\_used** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Is Used?</td></tr><tr><td>Technical name</td><td>is_used</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>boolean</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="84406702-4752-49af-95e1-21533438b695"></a>2.3.2.2.3.5 Field **expiration\_date**

##### 2.3.2.2.3.5.1 **expiration\_date** Tree Diagram

![Hackolade image](/assets/image109.png?raw=true)

##### 2.3.2.2.3.5.2 **expiration\_date** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Expiration Date</td></tr><tr><td>Technical name</td><td>expiration_date</td></tr><tr><td>Activated</td><td>false</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td>date-time</td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.3.2.2.4 **magic\_token\_verif** Indexes

<table class="index-table"><thead><tr><td class="table-property-column">Property</td><td class="table-column-property">User Id Unique</td></tr></thead><tbody><tr><td>Name</td><td class="table-column-indexes">User Id Unique</td></tr><tr><td>Activated</td><td class="table-column-indexes">true</td></tr><tr><td>Key</td><td class="table-column-indexes">doctor_id('ascending')</td></tr><tr><td>Hashed</td><td class="table-column-indexes"></td></tr><tr><td>Unique</td><td class="table-column-indexes">true</td></tr><tr><td>Drop duplicates</td><td class="table-column-indexes"></td></tr><tr><td>Sparse</td><td class="table-column-indexes"></td></tr><tr><td>Background indexing</td><td class="table-column-indexes"></td></tr><tr><td>Partial filter exp</td><td class="table-column-indexes"></td></tr><tr><td>Expire after (seconds)</td><td class="table-column-indexes"></td></tr><tr><td>Storage engine</td><td class="table-column-indexes">WiredTiger</td></tr><tr><td>Comments</td><td class="table-column-indexes"></td></tr></tbody></table>

##### 2.3.2.2.5 **magic\_token\_verif** JSON Schema

```
{
    "$schema": "http://json-schema.org/draft-04/schema#",
    "type": "object",
    "title": "Magic Token Verification",
    "properties": {
        "_id": {
            "type": "string",
            "title": "Id",
            "pattern": "^[a-fA-F0-9]{24}$"
        },
        "doctor_id": {
            "type": "string",
            "title": "Doctor Id",
            "pattern": "^[a-fA-F0-9]{24}$"
        },
        "magic_token": {
            "type": "string",
            "title": "Magic Token"
        },
        "is_used": {
            "type": "boolean",
            "title": "Is Used?"
        },
        "expiration_date": {
            "type": "string",
            "title": "Expiration Date",
            "format": "date-time"
        }
    },
    "additionalProperties": false
}
```

##### 2.3.2.2.6 **magic\_token\_verif** JSON data

```
{
    "_id": ObjectId("c7bcbd220caca8add1bb5440"),
    "doctor_id": ObjectId("c1d3ed8a5fdc6bbaf6bbc4fd"),
    "magic_token": "Lorem",
    "is_used": true,
    "expiration_date": "2011-06-14T04:12:36.123Z"
}
```

##### 2.3.2.2.7 **magic\_token\_verif** Target Script

```
use doctor_management;

db.createCollection("magic_token_verif", {
    "capped": false,
    "validator": {
        "$jsonSchema": {
            "bsonType": "object",
            "title": "Magic Token Verification",
            "properties": {
                "_id": {
                    "bsonType": "objectId",
                    "title": "Id"
                },
                "doctor_id": {
                    "bsonType": "objectId",
                    "title": "Doctor Id"
                },
                "magic_token": {
                    "bsonType": "string",
                    "title": "Magic Token"
                },
                "is_used": {
                    "bsonType": "bool",
                    "title": "Is Used?"
                }
            },
            "additionalProperties": false
        }
    },
    "validationLevel": "off"
});

db.magic_token_verif.createIndex({
    "doctor_id": 1
},
{
    "name": "User Id Unique",
    "unique": true
});
```

### <a id="3f008a3d-bd44-4d20-9e8a-39d56386fe29"></a>2.4 Database **merchant\_management**

![Hackolade image](/assets/image110.png?raw=true)

##### 2.4.1 **merchant\_management** Properties

<table class="collection-properties-table"><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Database name</td><td>Merchant Management</td></tr><tr><td>Technical name</td><td>merchant_management</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Enable sharding</td><td></td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="3f008a3d-bd44-4d20-9e8a-39d56386fe29-children"></a>2.4.2 **merchant\_management** Collections

### <a id="93d85f88-2fd3-4050-b2dc-bb43ee06bf2f"></a>2.4.2.1 Collection **magic\_token\_verif**

##### 2.4.2.1.1 **magic\_token\_verif** Tree Diagram

![Hackolade image](/assets/image111.png?raw=true)

##### 2.4.2.1.2 **magic\_token\_verif** Properties

<table class="collection-properties-table"><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Collection name</td><td>Magic Token Verification</td></tr><tr><td>Technical name</td><td>magic_token_verif</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>$ref</td><td></td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Database</td><td><a href=#3f008a3d-bd44-4d20-9e8a-39d56386fe29><span class="name-container">merchant_management</span></a></td></tr><tr><td>Capped</td><td></td></tr><tr><td>Time series</td><td></td></tr><tr><td>Size</td><td></td></tr><tr><td>Max</td><td></td></tr><tr><td>Storage engine</td><td>WiredTiger</td></tr><tr><td>Config String</td><td></td></tr><tr><td>Validation level</td><td>Off</td></tr><tr><td>Validation action</td><td>Warn</td></tr><tr><td>Encryption metadata</td><td></td></tr><tr><td>Additional properties</td><td>false</td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.4.2.1.3 **magic\_token\_verif** Fields

<table><thead><tr><td>Field</td><td>Type</td><td>Req</td><td>Key</td><td>Description</td><td>Comments</td></tr></thead><tbody><tr><td><a href=#a71d495f-2e4d-47c8-9746-eeb4a73b5f4b class="margin-0">_id</a></td><td class="no-break-word">objectId</td><td>false</td><td>pk</td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#69143d01-6049-454c-99b6-d5b05f33b0a9 class="margin-0">merchant_id</a></td><td class="no-break-word">objectId</td><td>false</td><td>fk</td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#04bb098c-6557-4d72-8313-c273cff466d4 class="margin-0">magic_token</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#46eaa9c4-756c-4e5b-ba8f-347f958a155e class="margin-0">is_used</a></td><td class="no-break-word">boolean</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#9072ba53-6a21-4a4b-b774-426ff79315ff class="margin-0">expiration_date</a></td><td class="no-break-word">date-time</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="a71d495f-2e4d-47c8-9746-eeb4a73b5f4b"></a>2.4.2.1.3.1 Field **\_id**

##### 2.4.2.1.3.1.1 **\_id** Tree Diagram

![Hackolade image](/assets/image112.png?raw=true)

##### 2.4.2.1.3.1.2 **\_id** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Id</td></tr><tr><td>Technical name</td><td>_id</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>objectId</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td>true</td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="69143d01-6049-454c-99b6-d5b05f33b0a9"></a>2.4.2.1.3.2 Field **merchant\_id**

##### 2.4.2.1.3.2.1 **merchant\_id** Tree Diagram

![Hackolade image](/assets/image113.png?raw=true)

##### 2.4.2.1.3.2.2 **merchant\_id** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Merchant Id</td></tr><tr><td>Technical name</td><td>merchant_id</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td>false</td></tr><tr><td>Type</td><td>objectId</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Foreign collection</td><td><a href=#94f57272-e536-4bec-a992-620568b4a916>merchant</a></td></tr><tr><td>Foreign field</td><td><a href=#67c74360-af95-4586-b309-df297267051f>_id</a></td></tr><tr><td>Relationship type</td><td>Foreign Key</td></tr><tr><td>Relationship name</td><td>fk Magic Token Verification.Merchant Id to Merchant.Id</td></tr><tr><td>Cardinality</td><td>1</td></tr><tr><td>Default</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="04bb098c-6557-4d72-8313-c273cff466d4"></a>2.4.2.1.3.3 Field **magic\_token**

##### 2.4.2.1.3.3.1 **magic\_token** Tree Diagram

![Hackolade image](/assets/image114.png?raw=true)

##### 2.4.2.1.3.3.2 **magic\_token** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Magic Token</td></tr><tr><td>Technical name</td><td>magic_token</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="46eaa9c4-756c-4e5b-ba8f-347f958a155e"></a>2.4.2.1.3.4 Field **is\_used**

##### 2.4.2.1.3.4.1 **is\_used** Tree Diagram

![Hackolade image](/assets/image115.png?raw=true)

##### 2.4.2.1.3.4.2 **is\_used** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Is Used?</td></tr><tr><td>Technical name</td><td>is_used</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>boolean</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="9072ba53-6a21-4a4b-b774-426ff79315ff"></a>2.4.2.1.3.5 Field **expiration\_date**

##### 2.4.2.1.3.5.1 **expiration\_date** Tree Diagram

![Hackolade image](/assets/image116.png?raw=true)

##### 2.4.2.1.3.5.2 **expiration\_date** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Expiration Date</td></tr><tr><td>Technical name</td><td>expiration_date</td></tr><tr><td>Activated</td><td>false</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td>date-time</td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.4.2.1.4 **magic\_token\_verif** Indexes

<table class="index-table"><thead><tr><td class="table-property-column">Property</td><td class="table-column-property">User Id Unique</td></tr></thead><tbody><tr><td>Name</td><td class="table-column-indexes">User Id Unique</td></tr><tr><td>Activated</td><td class="table-column-indexes">true</td></tr><tr><td>Key</td><td class="table-column-indexes">merchant_id('ascending')</td></tr><tr><td>Hashed</td><td class="table-column-indexes"></td></tr><tr><td>Unique</td><td class="table-column-indexes">true</td></tr><tr><td>Drop duplicates</td><td class="table-column-indexes"></td></tr><tr><td>Sparse</td><td class="table-column-indexes"></td></tr><tr><td>Background indexing</td><td class="table-column-indexes"></td></tr><tr><td>Partial filter exp</td><td class="table-column-indexes"></td></tr><tr><td>Expire after (seconds)</td><td class="table-column-indexes"></td></tr><tr><td>Storage engine</td><td class="table-column-indexes">WiredTiger</td></tr><tr><td>Comments</td><td class="table-column-indexes"></td></tr></tbody></table>

##### 2.4.2.1.5 **magic\_token\_verif** JSON Schema

```
{
    "$schema": "http://json-schema.org/draft-04/schema#",
    "type": "object",
    "title": "Magic Token Verification",
    "properties": {
        "_id": {
            "type": "string",
            "title": "Id",
            "pattern": "^[a-fA-F0-9]{24}$"
        },
        "merchant_id": {
            "type": "string",
            "title": "Merchant Id",
            "pattern": "^[a-fA-F0-9]{24}$"
        },
        "magic_token": {
            "type": "string",
            "title": "Magic Token"
        },
        "is_used": {
            "type": "boolean",
            "title": "Is Used?"
        },
        "expiration_date": {
            "type": "string",
            "title": "Expiration Date",
            "format": "date-time"
        }
    },
    "additionalProperties": false
}
```

##### 2.4.2.1.6 **magic\_token\_verif** JSON data

```
{
    "_id": ObjectId("af61ecb22bee7e0b0deded41"),
    "merchant_id": ObjectId("becd52c0f6b0fbff41d405b4"),
    "magic_token": "Lorem",
    "is_used": true,
    "expiration_date": "2011-06-14T04:12:36.123Z"
}
```

##### 2.4.2.1.7 **magic\_token\_verif** Target Script

```
use merchant_management;

db.createCollection("magic_token_verif", {
    "capped": false,
    "validator": {
        "$jsonSchema": {
            "bsonType": "object",
            "title": "Magic Token Verification",
            "properties": {
                "_id": {
                    "bsonType": "objectId",
                    "title": "Id"
                },
                "merchant_id": {
                    "bsonType": "objectId",
                    "title": "Merchant Id"
                },
                "magic_token": {
                    "bsonType": "string",
                    "title": "Magic Token"
                },
                "is_used": {
                    "bsonType": "bool",
                    "title": "Is Used?"
                }
            },
            "additionalProperties": false
        }
    },
    "validationLevel": "off"
});

db.magic_token_verif.createIndex({
    "merchant_id": 1
},
{
    "name": "User Id Unique",
    "unique": true
});
```

### <a id="94f57272-e536-4bec-a992-620568b4a916"></a>2.4.2.2 Collection **merchant**

##### 2.4.2.2.1 **merchant** Tree Diagram

![Hackolade image](/assets/image117.png?raw=true)

##### 2.4.2.2.2 **merchant** Properties

<table class="collection-properties-table"><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Collection name</td><td>Merchant</td></tr><tr><td>Technical name</td><td>merchant</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>$ref</td><td></td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Database</td><td><a href=#3f008a3d-bd44-4d20-9e8a-39d56386fe29><span class="name-container">merchant_management</span></a></td></tr><tr><td>Capped</td><td></td></tr><tr><td>Time series</td><td></td></tr><tr><td>Size</td><td></td></tr><tr><td>Max</td><td></td></tr><tr><td>Storage engine</td><td>WiredTiger</td></tr><tr><td>Config String</td><td></td></tr><tr><td>Validation level</td><td>Off</td></tr><tr><td>Validation action</td><td>Warn</td></tr><tr><td>Encryption metadata</td><td></td></tr><tr><td>Additional properties</td><td>false</td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.4.2.2.3 **merchant** Fields

<table><thead><tr><td>Field</td><td>Type</td><td>Req</td><td>Key</td><td>Description</td><td>Comments</td></tr></thead><tbody><tr><td><a href=#67c74360-af95-4586-b309-df297267051f class="margin-0">_id</a></td><td class="no-break-word">objectId</td><td>false</td><td>pk, dk</td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#00eb854d-eae7-486e-b037-1c5d6edecef3 class="margin-0">brand_id</a></td><td class="no-break-word">objectId</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#766998ba-4082-4733-9785-455d78b24f4e class="margin-0">name</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#a9d05eb9-2190-42e0-91ea-18fd5e134bc7 class="margin-0">email</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#f7ac88fd-48d6-4967-b21c-0f9e8a37f999 class="margin-0">password</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#1468ad18-2c59-4e06-a584-7ee33456becf class="margin-0">phone_number</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#6fc04edf-d6cb-4b8e-a477-0c96603ae977 class="margin-0">owner_name</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#2d024d07-22e2-4e9e-86f1-9065807844c6 class="margin-0">npwp_no</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#182b3d9a-c32c-4436-a570-f0e193bdf731 class="margin-0">document</a></td><td class="no-break-word">document</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#058d4d05-fb9a-443a-b9ab-9a5b5ffc10dd class="margin-5">npwp_doc_id</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#9e7b861f-fc99-4d82-b423-eec7ba480fae class="margin-5">ktp_doc_id</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#b5322414-b7a2-44b6-bd3b-b908f223542d class="margin-5">nib_doc_id</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#a77dd37f-f5d8-40bc-8192-e879e5fb27d2 class="margin-5">cert_doc_id</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#56678aac-58d4-4c2f-9c83-826233c72e0b class="margin-5">tdp_doc_id</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#0c738a0e-ea19-4628-a004-ecd1635fdf09 class="margin-5">siup_doc_id</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#479fb7d9-3b35-499d-84ea-5f814cc00aaf class="margin-0">bank</a></td><td class="no-break-word">array</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#09e0b2e2-2a38-4cf0-9ecf-4a720166f154 class="margin-5">[0]</a></td><td class="no-break-word">document</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#7eab0e76-ef8b-493e-ad5f-ce66d49818f5 class="margin-10">bank_code</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#281a0087-a15f-498b-ab80-fd2fd3b3a3ba class="margin-10">account_no</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#7ed3d04a-0156-4296-b836-fabf4c87f3ce class="margin-10">account_name</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#20d2a8b1-292a-4c10-90b7-b4d66eb6fc69 class="margin-0">created_date</a></td><td class="no-break-word">date-time</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#4ed8deb5-993c-470f-ba23-eb56c17ded51 class="margin-0">updated_date</a></td><td class="no-break-word">date-time</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#3b90aec4-21c3-45cd-9866-3a895c67fdbb class="margin-0">is_active</a></td><td class="no-break-word">boolean</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="67c74360-af95-4586-b309-df297267051f"></a>2.4.2.2.3.1 Field **\_id**

##### 2.4.2.2.3.1.1 **\_id** Tree Diagram

![Hackolade image](/assets/image118.png?raw=true)

##### 2.4.2.2.3.1.2 **\_id** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Id</td></tr><tr><td>Technical name</td><td>_id</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>objectId</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td>true</td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="00eb854d-eae7-486e-b037-1c5d6edecef3"></a>2.4.2.2.3.2 Field **brand\_id**

##### 2.4.2.2.3.2.1 **brand\_id** Tree Diagram

![Hackolade image](/assets/image119.png?raw=true)

##### 2.4.2.2.3.2.2 **brand\_id** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Brand Id</td></tr><tr><td>Technical name</td><td>brand_id</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>objectId</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="766998ba-4082-4733-9785-455d78b24f4e"></a>2.4.2.2.3.3 Field **name**

##### 2.4.2.2.3.3.1 **name** Tree Diagram

![Hackolade image](/assets/image120.png?raw=true)

##### 2.4.2.2.3.3.2 **name** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Merchant Name</td></tr><tr><td>Technical name</td><td>name</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="a9d05eb9-2190-42e0-91ea-18fd5e134bc7"></a>2.4.2.2.3.4 Field **email**

##### 2.4.2.2.3.4.1 **email** Tree Diagram

![Hackolade image](/assets/image121.png?raw=true)

##### 2.4.2.2.3.4.2 **email** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Merchant Email</td></tr><tr><td>Technical name</td><td>email</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="f7ac88fd-48d6-4967-b21c-0f9e8a37f999"></a>2.4.2.2.3.5 Field **password**

##### 2.4.2.2.3.5.1 **password** Tree Diagram

![Hackolade image](/assets/image122.png?raw=true)

##### 2.4.2.2.3.5.2 **password** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Merchant Password</td></tr><tr><td>Technical name</td><td>password</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="1468ad18-2c59-4e06-a584-7ee33456becf"></a>2.4.2.2.3.6 Field **phone\_number**

##### 2.4.2.2.3.6.1 **phone\_number** Tree Diagram

![Hackolade image](/assets/image123.png?raw=true)

##### 2.4.2.2.3.6.2 **phone\_number** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Merchant Phone Number</td></tr><tr><td>Technical name</td><td>phone_number</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="6fc04edf-d6cb-4b8e-a477-0c96603ae977"></a>2.4.2.2.3.7 Field **owner\_name**

##### 2.4.2.2.3.7.1 **owner\_name** Tree Diagram

![Hackolade image](/assets/image124.png?raw=true)

##### 2.4.2.2.3.7.2 **owner\_name** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Owner Name</td></tr><tr><td>Technical name</td><td>owner_name</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="2d024d07-22e2-4e9e-86f1-9065807844c6"></a>2.4.2.2.3.8 Field **npwp\_no**

##### 2.4.2.2.3.8.1 **npwp\_no** Tree Diagram

![Hackolade image](/assets/image125.png?raw=true)

##### 2.4.2.2.3.8.2 **npwp\_no** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>NPWP Number</td></tr><tr><td>Technical name</td><td>npwp_no</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="182b3d9a-c32c-4436-a570-f0e193bdf731"></a>2.4.2.2.3.9 Field **document**

##### 2.4.2.2.3.9.1 **document** Tree Diagram

![Hackolade image](/assets/image126.png?raw=true)

##### 2.4.2.2.3.9.2 **document** Hierarchy

Parent field: **merchant**

Child field(s):

<table class="field-properties-table"><thead><tr><td>Field</td><td>Type</td><td>Req</td><td>Key</td><td>Description</td><td>Comments</td></tr></thead><tbody><tr><td><a href=#058d4d05-fb9a-443a-b9ab-9a5b5ffc10dd class="margin-NaN">NPWP</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#9e7b861f-fc99-4d82-b423-eec7ba480fae class="margin-NaN">KTP</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#b5322414-b7a2-44b6-bd3b-b908f223542d class="margin-NaN">NIB</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#a77dd37f-f5d8-40bc-8192-e879e5fb27d2 class="margin-NaN">Akta</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#56678aac-58d4-4c2f-9c83-826233c72e0b class="margin-NaN">TDP</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#0c738a0e-ea19-4628-a004-ecd1635fdf09 class="margin-NaN">SIUP</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.4.2.2.3.9.3 **document** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Document</td></tr><tr><td>Technical name</td><td>document</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>document</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>DBRef</td><td></td></tr><tr><td>Min Properties</td><td></td></tr><tr><td>Max Properties</td><td></td></tr><tr><td>Additional properties</td><td>false</td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="058d4d05-fb9a-443a-b9ab-9a5b5ffc10dd"></a>2.4.2.2.3.10 Field **npwp\_doc\_id**

##### 2.4.2.2.3.10.1 **npwp\_doc\_id** Tree Diagram

![Hackolade image](/assets/image127.png?raw=true)

##### 2.4.2.2.3.10.2 **npwp\_doc\_id** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>NPWP</td></tr><tr><td>Technical name</td><td>npwp_doc_id</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="9e7b861f-fc99-4d82-b423-eec7ba480fae"></a>2.4.2.2.3.11 Field **ktp\_doc\_id**

##### 2.4.2.2.3.11.1 **ktp\_doc\_id** Tree Diagram

![Hackolade image](/assets/image128.png?raw=true)

##### 2.4.2.2.3.11.2 **ktp\_doc\_id** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>KTP</td></tr><tr><td>Technical name</td><td>ktp_doc_id</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="b5322414-b7a2-44b6-bd3b-b908f223542d"></a>2.4.2.2.3.12 Field **nib\_doc\_id**

##### 2.4.2.2.3.12.1 **nib\_doc\_id** Tree Diagram

![Hackolade image](/assets/image129.png?raw=true)

##### 2.4.2.2.3.12.2 **nib\_doc\_id** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>NIB</td></tr><tr><td>Technical name</td><td>nib_doc_id</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="a77dd37f-f5d8-40bc-8192-e879e5fb27d2"></a>2.4.2.2.3.13 Field **cert\_doc\_id**

##### 2.4.2.2.3.13.1 **cert\_doc\_id** Tree Diagram

![Hackolade image](/assets/image130.png?raw=true)

##### 2.4.2.2.3.13.2 **cert\_doc\_id** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Akta</td></tr><tr><td>Technical name</td><td>cert_doc_id</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="56678aac-58d4-4c2f-9c83-826233c72e0b"></a>2.4.2.2.3.14 Field **tdp\_doc\_id**

##### 2.4.2.2.3.14.1 **tdp\_doc\_id** Tree Diagram

![Hackolade image](/assets/image131.png?raw=true)

##### 2.4.2.2.3.14.2 **tdp\_doc\_id** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>TDP</td></tr><tr><td>Technical name</td><td>tdp_doc_id</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="0c738a0e-ea19-4628-a004-ecd1635fdf09"></a>2.4.2.2.3.15 Field **siup\_doc\_id**

##### 2.4.2.2.3.15.1 **siup\_doc\_id** Tree Diagram

![Hackolade image](/assets/image132.png?raw=true)

##### 2.4.2.2.3.15.2 **siup\_doc\_id** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>SIUP</td></tr><tr><td>Technical name</td><td>siup_doc_id</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="479fb7d9-3b35-499d-84ea-5f814cc00aaf"></a>2.4.2.2.3.16 Field **bank**

##### 2.4.2.2.3.16.1 **bank** Tree Diagram

![Hackolade image](/assets/image133.png?raw=true)

##### 2.4.2.2.3.16.2 **bank** Hierarchy

Parent field: **merchant**

Child field(s):

<table class="field-properties-table"><thead><tr><td>Field</td><td>Type</td><td>Req</td><td>Key</td><td>Description</td><td>Comments</td></tr></thead><tbody><tr><td><a href=#09e0b2e2-2a38-4cf0-9ecf-4a720166f154 class="margin-NaN">[0]</a></td><td class="no-break-word">document</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.4.2.2.3.16.3 **bank** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Bank</td></tr><tr><td>Technical name</td><td>bank</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>array</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Min items</td><td></td></tr><tr><td>Max items</td><td></td></tr><tr><td>Unique items</td><td></td></tr><tr><td>Additional items</td><td>true</td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="09e0b2e2-2a38-4cf0-9ecf-4a720166f154"></a>2.4.2.2.3.17 Field **\[0\]**

##### 2.4.2.2.3.17.1 **\[0\]** Tree Diagram

![Hackolade image](/assets/image134.png?raw=true)

##### 2.4.2.2.3.17.2 **\[0\]** Hierarchy

Parent field: **bank**

Child field(s):

<table class="field-properties-table"><thead><tr><td>Field</td><td>Type</td><td>Req</td><td>Key</td><td>Description</td><td>Comments</td></tr></thead><tbody><tr><td><a href=#7eab0e76-ef8b-493e-ad5f-ce66d49818f5 class="margin-NaN">Bank&nbsp;Code</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#281a0087-a15f-498b-ab80-fd2fd3b3a3ba class="margin-NaN">Account&nbsp;Number</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#7ed3d04a-0156-4296-b836-fabf4c87f3ce class="margin-NaN">Account&nbsp;Name</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.4.2.2.3.17.3 **\[0\]** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Display name</td><td></td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>document</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>DBRef</td><td></td></tr><tr><td>Min Properties</td><td></td></tr><tr><td>Max Properties</td><td></td></tr><tr><td>Additional properties</td><td>false</td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="7eab0e76-ef8b-493e-ad5f-ce66d49818f5"></a>2.4.2.2.3.18 Field **bank\_code**

##### 2.4.2.2.3.18.1 **bank\_code** Tree Diagram

![Hackolade image](/assets/image135.png?raw=true)

##### 2.4.2.2.3.18.2 **bank\_code** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Bank Code</td></tr><tr><td>Technical name</td><td>bank_code</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="281a0087-a15f-498b-ab80-fd2fd3b3a3ba"></a>2.4.2.2.3.19 Field **account\_no**

##### 2.4.2.2.3.19.1 **account\_no** Tree Diagram

![Hackolade image](/assets/image136.png?raw=true)

##### 2.4.2.2.3.19.2 **account\_no** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Account Number</td></tr><tr><td>Technical name</td><td>account_no</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="7ed3d04a-0156-4296-b836-fabf4c87f3ce"></a>2.4.2.2.3.20 Field **account\_name**

##### 2.4.2.2.3.20.1 **account\_name** Tree Diagram

![Hackolade image](/assets/image137.png?raw=true)

##### 2.4.2.2.3.20.2 **account\_name** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Account Name</td></tr><tr><td>Technical name</td><td>account_name</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="20d2a8b1-292a-4c10-90b7-b4d66eb6fc69"></a>2.4.2.2.3.21 Field **created\_date**

##### 2.4.2.2.3.21.1 **created\_date** Tree Diagram

![Hackolade image](/assets/image138.png?raw=true)

##### 2.4.2.2.3.21.2 **created\_date** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Created Date</td></tr><tr><td>Technical name</td><td>created_date</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td>date-time</td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td>false</td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="4ed8deb5-993c-470f-ba23-eb56c17ded51"></a>2.4.2.2.3.22 Field **updated\_date**

##### 2.4.2.2.3.22.1 **updated\_date** Tree Diagram

![Hackolade image](/assets/image139.png?raw=true)

##### 2.4.2.2.3.22.2 **updated\_date** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Updated Date</td></tr><tr><td>Technical name</td><td>updated_date</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td>date-time</td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td>false</td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="3b90aec4-21c3-45cd-9866-3a895c67fdbb"></a>2.4.2.2.3.23 Field **is\_active**

##### 2.4.2.2.3.23.1 **is\_active** Tree Diagram

![Hackolade image](/assets/image140.png?raw=true)

##### 2.4.2.2.3.23.2 **is\_active** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Is Active</td></tr><tr><td>Technical name</td><td>is_active</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>boolean</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.4.2.2.4 **merchant** JSON Schema

```
{
    "$schema": "http://json-schema.org/draft-04/schema#",
    "type": "object",
    "title": "Merchant",
    "properties": {
        "_id": {
            "type": "string",
            "title": "Id",
            "pattern": "^[a-fA-F0-9]{24}$"
        },
        "brand_id": {
            "type": "string",
            "title": "Brand Id",
            "pattern": "^[a-fA-F0-9]{24}$"
        },
        "name": {
            "type": "string",
            "title": "Merchant Name"
        },
        "email": {
            "type": "string",
            "title": "Merchant Email"
        },
        "password": {
            "type": "string",
            "title": "Merchant Password"
        },
        "phone_number": {
            "type": "string",
            "title": "Merchant Phone Number"
        },
        "owner_name": {
            "type": "string",
            "title": "Owner Name"
        },
        "npwp_no": {
            "type": "string",
            "title": "NPWP Number"
        },
        "document": {
            "type": "object",
            "title": "Document",
            "properties": {
                "npwp_doc_id": {
                    "type": "string",
                    "title": "NPWP"
                },
                "ktp_doc_id": {
                    "type": "string",
                    "title": "KTP"
                },
                "nib_doc_id": {
                    "type": "string",
                    "title": "NIB"
                },
                "cert_doc_id": {
                    "type": "string",
                    "title": "Akta"
                },
                "tdp_doc_id": {
                    "type": "string",
                    "title": "TDP"
                },
                "siup_doc_id": {
                    "type": "string",
                    "title": "SIUP"
                }
            },
            "additionalProperties": false
        },
        "bank": {
            "type": "array",
            "title": "Bank",
            "additionalItems": true,
            "items": {
                "type": "object",
                "properties": {
                    "bank_code": {
                        "type": "string",
                        "title": "Bank Code"
                    },
                    "account_no": {
                        "type": "string",
                        "title": "Account Number"
                    },
                    "account_name": {
                        "type": "string",
                        "title": "Account Name"
                    }
                },
                "additionalProperties": false
            }
        },
        "created_date": {
            "type": "string",
            "title": "Created Date",
            "format": "date-time"
        },
        "updated_date": {
            "type": "string",
            "title": "Updated Date",
            "format": "date-time"
        },
        "is_active": {
            "type": "boolean",
            "title": "Is Active"
        }
    },
    "additionalProperties": false
}
```

##### 2.4.2.2.5 **merchant** JSON data

```
{
    "_id": ObjectId("0f18a3e07e7a7e76e76e7a4e"),
    "brand_id": ObjectId("defa5b892c5faeafa505b7af"),
    "name": "Lorem",
    "email": "Lorem",
    "password": "Lorem",
    "phone_number": "Lorem",
    "owner_name": "Lorem",
    "npwp_no": "Lorem",
    "document": {
        "npwp_doc_id": "Lorem",
        "ktp_doc_id": "Lorem",
        "nib_doc_id": "Lorem",
        "cert_doc_id": "Lorem",
        "tdp_doc_id": "Lorem",
        "siup_doc_id": "Lorem"
    },
    "bank": [
        {
            "bank_code": "Lorem",
            "account_no": "Lorem",
            "account_name": "Lorem"
        }
    ],
    "created_date": "2011-06-14T04:12:36.123Z",
    "updated_date": "2011-06-14T04:12:36.123Z",
    "is_active": true
}
```

##### 2.4.2.2.6 **merchant** Target Script

```
use merchant_management;

db.createCollection("merchant", {
    "capped": false,
    "validator": {
        "$jsonSchema": {
            "bsonType": "object",
            "title": "Merchant",
            "properties": {
                "_id": {
                    "bsonType": "objectId",
                    "title": "Id"
                },
                "brand_id": {
                    "bsonType": "objectId",
                    "title": "Brand Id"
                },
                "name": {
                    "bsonType": "string",
                    "title": "Merchant Name"
                },
                "email": {
                    "bsonType": "string",
                    "title": "Merchant Email"
                },
                "password": {
                    "bsonType": "string",
                    "title": "Merchant Password"
                },
                "phone_number": {
                    "bsonType": "string",
                    "title": "Merchant Phone Number"
                },
                "owner_name": {
                    "bsonType": "string",
                    "title": "Owner Name"
                },
                "npwp_no": {
                    "bsonType": "string",
                    "title": "NPWP Number"
                },
                "document": {
                    "bsonType": "object",
                    "title": "Document",
                    "properties": {
                        "npwp_doc_id": {
                            "bsonType": "string",
                            "title": "NPWP"
                        },
                        "ktp_doc_id": {
                            "bsonType": "string",
                            "title": "KTP"
                        },
                        "nib_doc_id": {
                            "bsonType": "string",
                            "title": "NIB"
                        },
                        "cert_doc_id": {
                            "bsonType": "string",
                            "title": "Akta"
                        },
                        "tdp_doc_id": {
                            "bsonType": "string",
                            "title": "TDP"
                        },
                        "siup_doc_id": {
                            "bsonType": "string",
                            "title": "SIUP"
                        }
                    },
                    "additionalProperties": false
                },
                "bank": {
                    "bsonType": "array",
                    "title": "Bank",
                    "additionalItems": true,
                    "items": {
                        "bsonType": "object",
                        "properties": {
                            "bank_code": {
                                "bsonType": "string",
                                "title": "Bank Code"
                            },
                            "account_no": {
                                "bsonType": "string",
                                "title": "Account Number"
                            },
                            "account_name": {
                                "bsonType": "string",
                                "title": "Account Name"
                            }
                        },
                        "additionalProperties": false
                    }
                },
                "created_date": {
                    "bsonType": "string",
                    "title": "Created Date"
                },
                "updated_date": {
                    "bsonType": "string",
                    "title": "Updated Date"
                },
                "is_active": {
                    "bsonType": "bool",
                    "title": "Is Active"
                }
            },
            "additionalProperties": false
        }
    },
    "validationLevel": "off",
    "validationAction": "warn"
});
```

### <a id="df27f8e5-371f-430b-a520-e67cdd12ff67"></a>2.5 Database **payment\_processing**

![Hackolade image](/assets/image141.png?raw=true)

##### 2.5.1 **payment\_processing** Properties

<table class="collection-properties-table"><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Database name</td><td>Payment Processing</td></tr><tr><td>Technical name</td><td>payment_processing</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Enable sharding</td><td></td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="df27f8e5-371f-430b-a520-e67cdd12ff67-children"></a>2.5.2 **payment\_processing** Collections

### <a id="0aacc000-c080-402f-ae1d-90997d4daf54"></a>2.5.2.1 Collection **payment**

##### 2.5.2.1.1 **payment** Tree Diagram

![Hackolade image](/assets/image142.png?raw=true)

##### 2.5.2.1.2 **payment** Properties

<table class="collection-properties-table"><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Collection name</td><td>Payment</td></tr><tr><td>Technical name</td><td>payment</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>$ref</td><td></td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Database</td><td><a href=#df27f8e5-371f-430b-a520-e67cdd12ff67><span class="name-container">payment_processing</span></a></td></tr><tr><td>Capped</td><td></td></tr><tr><td>Time series</td><td></td></tr><tr><td>Size</td><td></td></tr><tr><td>Max</td><td></td></tr><tr><td>Storage engine</td><td>WiredTiger</td></tr><tr><td>Config String</td><td></td></tr><tr><td>Validation level</td><td>Off</td></tr><tr><td>Validation action</td><td>Warn</td></tr><tr><td>Encryption metadata</td><td></td></tr><tr><td>Additional properties</td><td>false</td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.5.2.1.3 **payment** Fields

<table><thead><tr><td>Field</td><td>Type</td><td>Req</td><td>Key</td><td>Description</td><td>Comments</td></tr></thead><tbody><tr><td><a href=#498e5333-1f77-4ea2-8b76-f8edc8589500 class="margin-0">_id</a></td><td class="no-break-word">objectId</td><td>false</td><td>pk</td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#302eede6-e057-4112-946b-773b1682b14d class="margin-0">order_id</a></td><td class="no-break-word">objectId</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#fab3fc67-6fe2-4257-b314-5c0508635b9f class="margin-0">amount</a></td><td class="no-break-word">decimal128</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#12ff6896-4ee2-4f5a-812b-d6b88a3ce112 class="margin-0">payment_method_code</a></td><td class="no-break-word">string</td><td>false</td><td>fk</td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#25bd6183-438e-4b8a-99f6-b17622d33045 class="margin-0">payment_gateway</a></td><td class="no-break-word">document</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#50c68aab-0447-4a9d-96d9-03e37b060830 class="margin-5">transaction_id</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#c7e360fb-53ea-42be-96fa-8e7e8c7ec3d7 class="margin-5">response_code</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#80736c6a-ae7b-49b2-9539-1fd9992037ea class="margin-5">response_message</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#d40e4046-ab1b-45c6-be39-c1f0dc5cd0a6 class="margin-5">response_payload?</a></td><td class="no-break-word">document</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#719a3564-55f9-4d71-be9c-4b3a39989f19 class="margin-0">status</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#a6eb424d-5846-4429-a3ed-38786fffe7bb class="margin-0">created_date</a></td><td class="no-break-word">date-time</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#45578b73-a94b-4242-b2ca-2e360af40fd3 class="margin-0">updated_date</a></td><td class="no-break-word">date-time</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="498e5333-1f77-4ea2-8b76-f8edc8589500"></a>2.5.2.1.3.1 Field **\_id**

##### 2.5.2.1.3.1.1 **\_id** Tree Diagram

![Hackolade image](/assets/image143.png?raw=true)

##### 2.5.2.1.3.1.2 **\_id** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Payment Id</td></tr><tr><td>Technical name</td><td>_id</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>objectId</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td>true</td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="302eede6-e057-4112-946b-773b1682b14d"></a>2.5.2.1.3.2 Field **order\_id**

##### 2.5.2.1.3.2.1 **order\_id** Tree Diagram

![Hackolade image](/assets/image144.png?raw=true)

##### 2.5.2.1.3.2.2 **order\_id** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Order Id</td></tr><tr><td>Technical name</td><td>order_id</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>objectId</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="fab3fc67-6fe2-4257-b314-5c0508635b9f"></a>2.5.2.1.3.3 Field **amount**

##### 2.5.2.1.3.3.1 **amount** Tree Diagram

![Hackolade image](/assets/image145.png?raw=true)

##### 2.5.2.1.3.3.2 **amount** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Amount</td></tr><tr><td>Technical name</td><td>amount</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>numeric</td></tr><tr><td>Subtype</td><td>decimal128</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Unit</td><td></td></tr><tr><td>Min value</td><td></td></tr><tr><td>Excl min</td><td></td></tr><tr><td>Max value</td><td></td></tr><tr><td>Excl max</td><td></td></tr><tr><td>Multiple of</td><td></td></tr><tr><td>Divisible by</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="12ff6896-4ee2-4f5a-812b-d6b88a3ce112"></a>2.5.2.1.3.4 Field **payment\_method\_code**

##### 2.5.2.1.3.4.1 **payment\_method\_code** Tree Diagram

![Hackolade image](/assets/image146.png?raw=true)

##### 2.5.2.1.3.4.2 **payment\_method\_code** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Payment Method</td></tr><tr><td>Technical name</td><td>payment_method_code</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td><a href=#5bc92ff3-77a2-461f-b396-ecf1af9deb2b>payment_method</a></td></tr><tr><td>Foreign field</td><td><a href=#74b8f0ba-fd04-408f-a536-90b65b1da3b4>code</a></td></tr><tr><td>Relationship type</td><td>Foreign Key</td></tr><tr><td>Relationship name</td><td>fk Payment.Payment Method to Payment Method.Payment Method Code</td></tr><tr><td>Cardinality</td><td>0..n</td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="25bd6183-438e-4b8a-99f6-b17622d33045"></a>2.5.2.1.3.5 Field **payment\_gateway**

##### 2.5.2.1.3.5.1 **payment\_gateway** Tree Diagram

![Hackolade image](/assets/image147.png?raw=true)

##### 2.5.2.1.3.5.2 **payment\_gateway** Hierarchy

Parent field: **payment**

Child field(s):

<table class="field-properties-table"><thead><tr><td>Field</td><td>Type</td><td>Req</td><td>Key</td><td>Description</td><td>Comments</td></tr></thead><tbody><tr><td><a href=#50c68aab-0447-4a9d-96d9-03e37b060830 class="margin-NaN">Transaction&nbsp;Id</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#c7e360fb-53ea-42be-96fa-8e7e8c7ec3d7 class="margin-NaN">Response&nbsp;Code</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#80736c6a-ae7b-49b2-9539-1fd9992037ea class="margin-NaN">Response&nbsp;Message</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#d40e4046-ab1b-45c6-be39-c1f0dc5cd0a6 class="margin-NaN">Response</a></td><td class="no-break-word">document</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.5.2.1.3.5.3 **payment\_gateway** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Payment Gateway</td></tr><tr><td>Technical name</td><td>payment_gateway</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>document</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>DBRef</td><td></td></tr><tr><td>Min Properties</td><td></td></tr><tr><td>Max Properties</td><td></td></tr><tr><td>Additional properties</td><td>false</td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="50c68aab-0447-4a9d-96d9-03e37b060830"></a>2.5.2.1.3.6 Field **transaction\_id**

##### 2.5.2.1.3.6.1 **transaction\_id** Tree Diagram

![Hackolade image](/assets/image148.png?raw=true)

##### 2.5.2.1.3.6.2 **transaction\_id** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Transaction Id</td></tr><tr><td>Technical name</td><td>transaction_id</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="c7e360fb-53ea-42be-96fa-8e7e8c7ec3d7"></a>2.5.2.1.3.7 Field **response\_code**

##### 2.5.2.1.3.7.1 **response\_code** Tree Diagram

![Hackolade image](/assets/image149.png?raw=true)

##### 2.5.2.1.3.7.2 **response\_code** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Response Code</td></tr><tr><td>Technical name</td><td>response_code</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="80736c6a-ae7b-49b2-9539-1fd9992037ea"></a>2.5.2.1.3.8 Field **response\_message**

##### 2.5.2.1.3.8.1 **response\_message** Tree Diagram

![Hackolade image](/assets/image150.png?raw=true)

##### 2.5.2.1.3.8.2 **response\_message** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Response Message</td></tr><tr><td>Technical name</td><td>response_message</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="d40e4046-ab1b-45c6-be39-c1f0dc5cd0a6"></a>2.5.2.1.3.9 Field **response\_payload?**

##### 2.5.2.1.3.9.1 **response\_payload?** Tree Diagram

![Hackolade image](/assets/image151.png?raw=true)

##### 2.5.2.1.3.9.2 **response\_payload?** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Response</td></tr><tr><td>Technical name</td><td>response_payload?</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>document</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>DBRef</td><td></td></tr><tr><td>Min Properties</td><td></td></tr><tr><td>Max Properties</td><td></td></tr><tr><td>Additional properties</td><td>false</td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="719a3564-55f9-4d71-be9c-4b3a39989f19"></a>2.5.2.1.3.10 Field **status**

##### 2.5.2.1.3.10.1 **status** Tree Diagram

![Hackolade image](/assets/image152.png?raw=true)

##### 2.5.2.1.3.10.2 **status** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Payment Status</td></tr><tr><td>Technical name</td><td>status</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td>failed,pending,success</td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="a6eb424d-5846-4429-a3ed-38786fffe7bb"></a>2.5.2.1.3.11 Field **created\_date**

##### 2.5.2.1.3.11.1 **created\_date** Tree Diagram

![Hackolade image](/assets/image153.png?raw=true)

##### 2.5.2.1.3.11.2 **created\_date** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Created Date</td></tr><tr><td>Technical name</td><td>created_date</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td>date-time</td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="45578b73-a94b-4242-b2ca-2e360af40fd3"></a>2.5.2.1.3.12 Field **updated\_date**

##### 2.5.2.1.3.12.1 **updated\_date** Tree Diagram

![Hackolade image](/assets/image154.png?raw=true)

##### 2.5.2.1.3.12.2 **updated\_date** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Updated Date</td></tr><tr><td>Technical name</td><td>updated_date</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td>date-time</td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.5.2.1.4 **payment** JSON Schema

```
{
    "$schema": "http://json-schema.org/draft-04/schema#",
    "type": "object",
    "title": "Payment",
    "properties": {
        "_id": {
            "type": "string",
            "title": "Payment Id",
            "pattern": "^[a-fA-F0-9]{24}$"
        },
        "order_id": {
            "type": "string",
            "title": "Order Id",
            "pattern": "^[a-fA-F0-9]{24}$"
        },
        "amount": {
            "type": "number",
            "title": "Amount"
        },
        "payment_method_code": {
            "type": "string",
            "title": "Payment Method"
        },
        "payment_gateway": {
            "type": "object",
            "title": "Payment Gateway",
            "properties": {
                "transaction_id": {
                    "type": "string",
                    "title": "Transaction Id"
                },
                "response_code": {
                    "type": "string",
                    "title": "Response Code"
                },
                "response_message": {
                    "type": "string",
                    "title": "Response Message"
                },
                "response_payload?": {
                    "type": "object",
                    "title": "Response",
                    "additionalProperties": false
                }
            },
            "additionalProperties": false
        },
        "status": {
            "type": "string",
            "title": "Payment Status",
            "enum": [
                "failed",
                "pending",
                "success"
            ]
        },
        "created_date": {
            "type": "string",
            "title": "Created Date",
            "format": "date-time"
        },
        "updated_date": {
            "type": "string",
            "title": "Updated Date",
            "format": "date-time"
        }
    },
    "additionalProperties": false
}
```

##### 2.5.2.1.5 **payment** JSON data

```
{
    "_id": ObjectId("7aa1a63ffb3e5428ba9f27b0"),
    "order_id": ObjectId("cf9b2aabd9ec2afb20d2d7ea"),
    "amount": Decimal128("-2.6072550954978585e+49"),
    "payment_method_code": "Lorem",
    "payment_gateway": {
        "transaction_id": "Lorem",
        "response_code": "Lorem",
        "response_message": "Lorem",
        "response_payload?": {}
    },
    "status": "success",
    "created_date": "2011-06-14T04:12:36.123Z",
    "updated_date": "2011-06-14T04:12:36.123Z"
}
```

##### 2.5.2.1.6 **payment** Target Script

```
use payment_processing;

db.createCollection("payment", {
    "capped": false,
    "validator": {
        "$jsonSchema": {
            "bsonType": "object",
            "title": "Payment",
            "properties": {
                "_id": {
                    "bsonType": "objectId",
                    "title": "Payment Id"
                },
                "order_id": {
                    "bsonType": "objectId",
                    "title": "Order Id"
                },
                "amount": {
                    "bsonType": "decimal",
                    "title": "Amount"
                },
                "payment_method_code": {
                    "bsonType": "string",
                    "title": "Payment Method"
                },
                "payment_gateway": {
                    "bsonType": "object",
                    "title": "Payment Gateway",
                    "properties": {
                        "transaction_id": {
                            "bsonType": "string",
                            "title": "Transaction Id"
                        },
                        "response_code": {
                            "bsonType": "string",
                            "title": "Response Code"
                        },
                        "response_message": {
                            "bsonType": "string",
                            "title": "Response Message"
                        },
                        "response_payload?": {
                            "bsonType": "object",
                            "title": "Response",
                            "additionalProperties": false
                        }
                    },
                    "additionalProperties": false
                },
                "status": {
                    "bsonType": "string",
                    "title": "Payment Status",
                    "enum": [
                        "failed",
                        "pending",
                        "success"
                    ]
                },
                "created_date": {
                    "bsonType": "string",
                    "title": "Created Date"
                },
                "updated_date": {
                    "bsonType": "string",
                    "title": "Updated Date"
                }
            },
            "additionalProperties": false
        }
    },
    "validationLevel": "off",
    "validationAction": "warn"
});
```

### <a id="18f9a5af-8c70-44bc-a6b8-28a673f9eb13"></a>2.5.2.2 Collection **payment\_gateway**

##### 2.5.2.2.1 **payment\_gateway** Tree Diagram

![Hackolade image](/assets/image155.png?raw=true)

##### 2.5.2.2.2 **payment\_gateway** Properties

<table class="collection-properties-table"><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Collection name</td><td>payment_gateway</td></tr><tr><td>Technical name</td><td></td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>$ref</td><td></td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Database</td><td><a href=#df27f8e5-371f-430b-a520-e67cdd12ff67><span class="name-container">payment_processing</span></a></td></tr><tr><td>Capped</td><td></td></tr><tr><td>Time series</td><td></td></tr><tr><td>Size</td><td></td></tr><tr><td>Max</td><td></td></tr><tr><td>Storage engine</td><td>WiredTiger</td></tr><tr><td>Config String</td><td></td></tr><tr><td>Validation level</td><td>Off</td></tr><tr><td>Validation action</td><td>Warn</td></tr><tr><td>Encryption metadata</td><td></td></tr><tr><td>Additional properties</td><td>false</td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.5.2.2.3 **payment\_gateway** Fields

<table><thead><tr><td>Field</td><td>Type</td><td>Req</td><td>Key</td><td>Description</td><td>Comments</td></tr></thead><tbody><tr><td><a href=#d6e24d82-3f20-44de-b9d8-5cd967944e2e class="margin-0">_id</a></td><td class="no-break-word">objectId</td><td>false</td><td>pk</td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#7a1dcbc7-ef4c-4386-81e8-f81d83ff681c class="margin-0">code</a></td><td class="no-break-word">string</td><td>false</td><td>dk</td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#7f36ec11-7f53-4b41-8b0e-e64a66559679 class="margin-0">name</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="d6e24d82-3f20-44de-b9d8-5cd967944e2e"></a>2.5.2.2.3.1 Field **\_id**

##### 2.5.2.2.3.1.1 **\_id** Tree Diagram

![Hackolade image](/assets/image156.png?raw=true)

##### 2.5.2.2.3.1.2 **\_id** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Id</td></tr><tr><td>Technical name</td><td>_id</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>objectId</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td>true</td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="7a1dcbc7-ef4c-4386-81e8-f81d83ff681c"></a>2.5.2.2.3.2 Field **code**

##### 2.5.2.2.3.2.1 **code** Tree Diagram

![Hackolade image](/assets/image157.png?raw=true)

##### 2.5.2.2.3.2.2 **code** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Payment Gateway Code</td></tr><tr><td>Technical name</td><td>code</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="7f36ec11-7f53-4b41-8b0e-e64a66559679"></a>2.5.2.2.3.3 Field **name**

##### 2.5.2.2.3.3.1 **name** Tree Diagram

![Hackolade image](/assets/image158.png?raw=true)

##### 2.5.2.2.3.3.2 **name** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Payment Gateway Name</td></tr><tr><td>Technical name</td><td>name</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.5.2.2.4 **payment\_gateway** JSON Schema

```
{
    "$schema": "http://json-schema.org/draft-04/schema#",
    "type": "object",
    "title": "payment_gateway",
    "properties": {
        "_id": {
            "type": "string",
            "title": "Id",
            "pattern": "^[a-fA-F0-9]{24}$"
        },
        "code": {
            "type": "string",
            "title": "Payment Gateway Code"
        },
        "name": {
            "type": "string",
            "title": "Payment Gateway Name"
        }
    },
    "additionalProperties": false
}
```

##### 2.5.2.2.5 **payment\_gateway** JSON data

```
{
    "_id": ObjectId("afff4ceb15b4bffeb5f0e4c1"),
    "code": "Lorem",
    "name": "Lorem"
}
```

##### 2.5.2.2.6 **payment\_gateway** Target Script

```
use payment_processing;

db.createCollection("payment_gateway", {
    "capped": false,
    "validator": {
        "$jsonSchema": {
            "bsonType": "object",
            "title": "payment_gateway",
            "properties": {
                "_id": {
                    "bsonType": "objectId",
                    "title": "Id"
                },
                "code": {
                    "bsonType": "string",
                    "title": "Payment Gateway Code"
                },
                "name": {
                    "bsonType": "string",
                    "title": "Payment Gateway Name"
                }
            },
            "additionalProperties": false
        }
    },
    "validationLevel": "off",
    "validationAction": "warn"
});
```

### <a id="5bc92ff3-77a2-461f-b396-ecf1af9deb2b"></a>2.5.2.3 Collection **payment\_method**

##### 2.5.2.3.1 **payment\_method** Tree Diagram

![Hackolade image](/assets/image159.png?raw=true)

##### 2.5.2.3.2 **payment\_method** Properties

<table class="collection-properties-table"><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Collection name</td><td>Payment Method</td></tr><tr><td>Technical name</td><td>payment_method</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>$ref</td><td></td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Database</td><td><a href=#df27f8e5-371f-430b-a520-e67cdd12ff67><span class="name-container">payment_processing</span></a></td></tr><tr><td>Capped</td><td></td></tr><tr><td>Time series</td><td></td></tr><tr><td>Size</td><td></td></tr><tr><td>Max</td><td></td></tr><tr><td>Storage engine</td><td>WiredTiger</td></tr><tr><td>Config String</td><td></td></tr><tr><td>Validation level</td><td>Off</td></tr><tr><td>Validation action</td><td>Warn</td></tr><tr><td>Encryption metadata</td><td></td></tr><tr><td>Additional properties</td><td>false</td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.5.2.3.3 **payment\_method** Fields

<table><thead><tr><td>Field</td><td>Type</td><td>Req</td><td>Key</td><td>Description</td><td>Comments</td></tr></thead><tbody><tr><td><a href=#aec4144e-aaee-4e39-83b9-534b8e78c335 class="margin-0">_id</a></td><td class="no-break-word">objectId</td><td>false</td><td>pk</td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#74b8f0ba-fd04-408f-a536-90b65b1da3b4 class="margin-0">code</a></td><td class="no-break-word">string</td><td>false</td><td>dk</td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#be98e646-7563-4746-add3-809066fe656d class="margin-0">name</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#004ffb05-f8e6-4699-bbef-d6c55d13a3eb class="margin-0">logo_id</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#50aae6ac-b599-4f87-93f2-c854679432be class="margin-0">type</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#55240d42-cdba-4972-b39b-c5f0aa73847e class="margin-0">payment_gateway_code</a></td><td class="no-break-word">string</td><td>false</td><td>fk</td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="aec4144e-aaee-4e39-83b9-534b8e78c335"></a>2.5.2.3.3.1 Field **\_id**

##### 2.5.2.3.3.1.1 **\_id** Tree Diagram

![Hackolade image](/assets/image160.png?raw=true)

##### 2.5.2.3.3.1.2 **\_id** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Id</td></tr><tr><td>Technical name</td><td>_id</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>objectId</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td>true</td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="74b8f0ba-fd04-408f-a536-90b65b1da3b4"></a>2.5.2.3.3.2 Field **code**

##### 2.5.2.3.3.2.1 **code** Tree Diagram

![Hackolade image](/assets/image161.png?raw=true)

##### 2.5.2.3.3.2.2 **code** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Payment Method Code</td></tr><tr><td>Technical name</td><td>code</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="be98e646-7563-4746-add3-809066fe656d"></a>2.5.2.3.3.3 Field **name**

##### 2.5.2.3.3.3.1 **name** Tree Diagram

![Hackolade image](/assets/image162.png?raw=true)

##### 2.5.2.3.3.3.2 **name** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Payment Method Name</td></tr><tr><td>Technical name</td><td>name</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="004ffb05-f8e6-4699-bbef-d6c55d13a3eb"></a>2.5.2.3.3.4 Field **logo\_id**

##### 2.5.2.3.3.4.1 **logo\_id** Tree Diagram

![Hackolade image](/assets/image163.png?raw=true)

##### 2.5.2.3.3.4.2 **logo\_id** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Payment Method Logo</td></tr><tr><td>Technical name</td><td>logo_id</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="50aae6ac-b599-4f87-93f2-c854679432be"></a>2.5.2.3.3.5 Field **type**

##### 2.5.2.3.3.5.1 **type** Tree Diagram

![Hackolade image](/assets/image164.png?raw=true)

##### 2.5.2.3.3.5.2 **type** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Payment Method Type</td></tr><tr><td>Technical name</td><td>type</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td>E-Money,Virtual Account,Credit Card</td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="55240d42-cdba-4972-b39b-c5f0aa73847e"></a>2.5.2.3.3.6 Field **payment\_gateway\_code**

##### 2.5.2.3.3.6.1 **payment\_gateway\_code** Tree Diagram

![Hackolade image](/assets/image165.png?raw=true)

##### 2.5.2.3.3.6.2 **payment\_gateway\_code** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Payment Gateway</td></tr><tr><td>Technical name</td><td>payment_gateway_code</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td><a href=#18f9a5af-8c70-44bc-a6b8-28a673f9eb13>payment_gateway</a></td></tr><tr><td>Foreign field</td><td><a href=#7a1dcbc7-ef4c-4386-81e8-f81d83ff681c>code</a></td></tr><tr><td>Relationship type</td><td>Foreign Key</td></tr><tr><td>Relationship name</td><td>fk Payment Method.Payment Gateway to payment_gateway.Payment Gateway Code</td></tr><tr><td>Cardinality</td><td>0..n</td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.5.2.3.4 **payment\_method** JSON Schema

```
{
    "$schema": "http://json-schema.org/draft-04/schema#",
    "type": "object",
    "title": "Payment Method",
    "properties": {
        "_id": {
            "type": "string",
            "title": "Id",
            "pattern": "^[a-fA-F0-9]{24}$"
        },
        "code": {
            "type": "string",
            "title": "Payment Method Code"
        },
        "name": {
            "type": "string",
            "title": "Payment Method Name"
        },
        "logo_id": {
            "type": "string",
            "title": "Payment Method Logo"
        },
        "type": {
            "type": "string",
            "title": "Payment Method Type",
            "enum": [
                "E-Money",
                "Virtual Account",
                "Credit Card"
            ]
        },
        "payment_gateway_code": {
            "type": "string",
            "title": "Payment Gateway"
        }
    },
    "additionalProperties": false
}
```

##### 2.5.2.3.5 **payment\_method** JSON data

```
{
    "_id": ObjectId("339327ebf46435cb457bc0cc"),
    "code": "Lorem",
    "name": "Lorem",
    "logo_id": "Lorem",
    "type": "Credit Card",
    "payment_gateway_code": "Lorem"
}
```

##### 2.5.2.3.6 **payment\_method** Target Script

```
use payment_processing;

db.createCollection("payment_method", {
    "capped": false,
    "validator": {
        "$jsonSchema": {
            "bsonType": "object",
            "title": "Payment Method",
            "properties": {
                "_id": {
                    "bsonType": "objectId",
                    "title": "Id"
                },
                "code": {
                    "bsonType": "string",
                    "title": "Payment Method Code"
                },
                "name": {
                    "bsonType": "string",
                    "title": "Payment Method Name"
                },
                "logo_id": {
                    "bsonType": "string",
                    "title": "Payment Method Logo"
                },
                "type": {
                    "bsonType": "string",
                    "title": "Payment Method Type",
                    "enum": [
                        "E-Money",
                        "Virtual Account",
                        "Credit Card"
                    ]
                },
                "payment_gateway_code": {
                    "bsonType": "string",
                    "title": "Payment Gateway"
                }
            },
            "additionalProperties": false
        }
    },
    "validationLevel": "off",
    "validationAction": "warn"
});
```

### <a id="f671992c-2f83-4667-a12c-e7d809cfc82a"></a>2.6 Database **user\_management**

![Hackolade image](/assets/image166.png?raw=true)

##### 2.6.1 **user\_management** Properties

<table class="collection-properties-table"><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Database name</td><td>User Management</td></tr><tr><td>Technical name</td><td>user_management</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Enable sharding</td><td></td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="f671992c-2f83-4667-a12c-e7d809cfc82a-children"></a>2.6.2 **user\_management** Collections

### <a id="71559dab-09ef-4f31-85fd-3585c5537361"></a>2.6.2.1 Collection **magic\_token\_verif**

##### 2.6.2.1.1 **magic\_token\_verif** Tree Diagram

![Hackolade image](/assets/image167.png?raw=true)

##### 2.6.2.1.2 **magic\_token\_verif** Properties

<table class="collection-properties-table"><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Collection name</td><td>Magic Token Verification</td></tr><tr><td>Technical name</td><td>magic_token_verif</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>$ref</td><td></td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Database</td><td><a href=#f671992c-2f83-4667-a12c-e7d809cfc82a><span class="name-container">user_management</span></a></td></tr><tr><td>Capped</td><td></td></tr><tr><td>Time series</td><td></td></tr><tr><td>Size</td><td></td></tr><tr><td>Max</td><td></td></tr><tr><td>Storage engine</td><td>WiredTiger</td></tr><tr><td>Config String</td><td></td></tr><tr><td>Validation level</td><td>Off</td></tr><tr><td>Validation action</td><td>Warn</td></tr><tr><td>Encryption metadata</td><td></td></tr><tr><td>Additional properties</td><td>false</td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.6.2.1.3 **magic\_token\_verif** Fields

<table><thead><tr><td>Field</td><td>Type</td><td>Req</td><td>Key</td><td>Description</td><td>Comments</td></tr></thead><tbody><tr><td><a href=#c5097ac2-2bbd-4457-940e-42a6cce67281 class="margin-0">_id</a></td><td class="no-break-word">objectId</td><td>false</td><td>pk</td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#c780c7e2-cc91-4a55-a79b-45d186686788 class="margin-0">user_id</a></td><td class="no-break-word">objectId</td><td>false</td><td>fk</td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#3fa0c167-b2f9-4c43-b523-4a5c5d2f9b91 class="margin-0">magic_token</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#7fd65d60-d4c3-4634-aed1-9dd6a8e9da43 class="margin-0">is_used</a></td><td class="no-break-word">boolean</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#f42c6b6a-882f-43c8-8fcf-0f364d7ca2c9 class="margin-0">expiration_date</a></td><td class="no-break-word">date-time</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="c5097ac2-2bbd-4457-940e-42a6cce67281"></a>2.6.2.1.3.1 Field **\_id**

##### 2.6.2.1.3.1.1 **\_id** Tree Diagram

![Hackolade image](/assets/image168.png?raw=true)

##### 2.6.2.1.3.1.2 **\_id** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Id</td></tr><tr><td>Technical name</td><td>_id</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>objectId</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td>true</td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="c780c7e2-cc91-4a55-a79b-45d186686788"></a>2.6.2.1.3.2 Field **user\_id**

##### 2.6.2.1.3.2.1 **user\_id** Tree Diagram

![Hackolade image](/assets/image169.png?raw=true)

##### 2.6.2.1.3.2.2 **user\_id** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>User Id</td></tr><tr><td>Technical name</td><td>user_id</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td>false</td></tr><tr><td>Type</td><td>objectId</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Foreign collection</td><td><a href=#bf2c22f1-3ab1-499d-9310-1d6fe75f718a>user</a></td></tr><tr><td>Foreign field</td><td><a href=#e876e38e-06d0-4e34-87be-2cb6ba19a5a4>_id</a></td></tr><tr><td>Relationship type</td><td>Foreign Key</td></tr><tr><td>Relationship name</td><td>fk Reset.User Id to user.Id</td></tr><tr><td>Cardinality</td><td>1</td></tr><tr><td>Default</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="3fa0c167-b2f9-4c43-b523-4a5c5d2f9b91"></a>2.6.2.1.3.3 Field **magic\_token**

##### 2.6.2.1.3.3.1 **magic\_token** Tree Diagram

![Hackolade image](/assets/image170.png?raw=true)

##### 2.6.2.1.3.3.2 **magic\_token** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Magic Token</td></tr><tr><td>Technical name</td><td>magic_token</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="7fd65d60-d4c3-4634-aed1-9dd6a8e9da43"></a>2.6.2.1.3.4 Field **is\_used**

##### 2.6.2.1.3.4.1 **is\_used** Tree Diagram

![Hackolade image](/assets/image171.png?raw=true)

##### 2.6.2.1.3.4.2 **is\_used** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Is Used?</td></tr><tr><td>Technical name</td><td>is_used</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>boolean</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="f42c6b6a-882f-43c8-8fcf-0f364d7ca2c9"></a>2.6.2.1.3.5 Field **expiration\_date**

##### 2.6.2.1.3.5.1 **expiration\_date** Tree Diagram

![Hackolade image](/assets/image172.png?raw=true)

##### 2.6.2.1.3.5.2 **expiration\_date** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Expiration Date</td></tr><tr><td>Technical name</td><td>expiration_date</td></tr><tr><td>Activated</td><td>false</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td>date-time</td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.6.2.1.4 **magic\_token\_verif** Indexes

<table class="index-table"><thead><tr><td class="table-property-column">Property</td><td class="table-column-property">User Id Unique</td></tr></thead><tbody><tr><td>Name</td><td class="table-column-indexes">User Id Unique</td></tr><tr><td>Activated</td><td class="table-column-indexes">true</td></tr><tr><td>Key</td><td class="table-column-indexes">user_id('ascending')</td></tr><tr><td>Hashed</td><td class="table-column-indexes"></td></tr><tr><td>Unique</td><td class="table-column-indexes">true</td></tr><tr><td>Drop duplicates</td><td class="table-column-indexes"></td></tr><tr><td>Sparse</td><td class="table-column-indexes"></td></tr><tr><td>Background indexing</td><td class="table-column-indexes"></td></tr><tr><td>Partial filter exp</td><td class="table-column-indexes"></td></tr><tr><td>Expire after (seconds)</td><td class="table-column-indexes"></td></tr><tr><td>Storage engine</td><td class="table-column-indexes">WiredTiger</td></tr><tr><td>Comments</td><td class="table-column-indexes"></td></tr></tbody></table>

##### 2.6.2.1.5 **magic\_token\_verif** JSON Schema

```
{
    "$schema": "http://json-schema.org/draft-04/schema#",
    "type": "object",
    "title": "Magic Token Verification",
    "properties": {
        "_id": {
            "type": "string",
            "title": "Id",
            "pattern": "^[a-fA-F0-9]{24}$"
        },
        "user_id": {
            "type": "string",
            "title": "User Id",
            "pattern": "^[a-fA-F0-9]{24}$"
        },
        "magic_token": {
            "type": "string",
            "title": "Magic Token"
        },
        "is_used": {
            "type": "boolean",
            "title": "Is Used?"
        },
        "expiration_date": {
            "type": "string",
            "title": "Expiration Date",
            "format": "date-time"
        }
    },
    "additionalProperties": false
}
```

##### 2.6.2.1.6 **magic\_token\_verif** JSON data

```
{
    "_id": ObjectId("5c631fc4cc9d2cefca4eecfc"),
    "user_id": ObjectId("da2d3ca9ee1bff15ade3b05f"),
    "magic_token": "Lorem",
    "is_used": true,
    "expiration_date": "2011-06-14T04:12:36.123Z"
}
```

##### 2.6.2.1.7 **magic\_token\_verif** Target Script

```
use user_management;

db.createCollection("magic_token_verif", {
    "capped": false,
    "validator": {
        "$jsonSchema": {
            "bsonType": "object",
            "title": "Magic Token Verification",
            "properties": {
                "_id": {
                    "bsonType": "objectId",
                    "title": "Id"
                },
                "user_id": {
                    "bsonType": "objectId",
                    "title": "User Id"
                },
                "magic_token": {
                    "bsonType": "string",
                    "title": "Magic Token"
                },
                "is_used": {
                    "bsonType": "bool",
                    "title": "Is Used?"
                }
            },
            "additionalProperties": false
        }
    },
    "validationLevel": "off"
});

db.magic_token_verif.createIndex({
    "user_id": 1
},
{
    "name": "User Id Unique",
    "unique": true
});
```

### <a id="3c6c484a-4a6d-4390-a7b3-117297837918"></a>2.6.2.2 Collection **otp\_verif**

##### 2.6.2.2.1 **otp\_verif** Tree Diagram

![Hackolade image](/assets/image173.png?raw=true)

##### 2.6.2.2.2 **otp\_verif** Properties

<table class="collection-properties-table"><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Collection name</td><td>OTP Verification</td></tr><tr><td>Technical name</td><td>otp_verif</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>$ref</td><td></td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Database</td><td><a href=#f671992c-2f83-4667-a12c-e7d809cfc82a><span class="name-container">user_management</span></a></td></tr><tr><td>Capped</td><td></td></tr><tr><td>Time series</td><td></td></tr><tr><td>Size</td><td></td></tr><tr><td>Max</td><td></td></tr><tr><td>Storage engine</td><td>WiredTiger</td></tr><tr><td>Config String</td><td></td></tr><tr><td>Validation level</td><td>Off</td></tr><tr><td>Validation action</td><td>Warn</td></tr><tr><td>Encryption metadata</td><td></td></tr><tr><td>Additional properties</td><td>false</td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.6.2.2.3 **otp\_verif** Fields

<table><thead><tr><td>Field</td><td>Type</td><td>Req</td><td>Key</td><td>Description</td><td>Comments</td></tr></thead><tbody><tr><td><a href=#3fa83438-beb7-4759-9b76-a08e18453083 class="margin-0">_id</a></td><td class="no-break-word">objectId</td><td>false</td><td>pk</td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#4428034a-1d93-4ad8-8713-6b96df5d791c class="margin-0">email</a></td><td class="no-break-word">string</td><td>false</td><td>fk</td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#459c6e50-a3bb-4103-8c34-cf36ee049a4e class="margin-0">otp</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#b0bfa47f-4108-4589-9603-c2af591bdb53 class="margin-0">is_used</a></td><td class="no-break-word">boolean</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#d2398192-1489-417c-91d4-a20daaf3c470 class="margin-0">expiration_date</a></td><td class="no-break-word">date-time</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="3fa83438-beb7-4759-9b76-a08e18453083"></a>2.6.2.2.3.1 Field **\_id**

##### 2.6.2.2.3.1.1 **\_id** Tree Diagram

![Hackolade image](/assets/image174.png?raw=true)

##### 2.6.2.2.3.1.2 **\_id** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Id</td></tr><tr><td>Technical name</td><td>_id</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>objectId</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td>true</td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="4428034a-1d93-4ad8-8713-6b96df5d791c"></a>2.6.2.2.3.2 Field **email**

##### 2.6.2.2.3.2.1 **email** Tree Diagram

![Hackolade image](/assets/image175.png?raw=true)

##### 2.6.2.2.3.2.2 **email** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Email</td></tr><tr><td>Technical name</td><td>email</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td><a href=#bf2c22f1-3ab1-499d-9310-1d6fe75f718a>user</a></td></tr><tr><td>Foreign field</td><td><a href=#6295501b-5234-4c40-a0cc-adb8202193a9>email</a></td></tr><tr><td>Relationship type</td><td>Foreign Key</td></tr><tr><td>Relationship name</td><td>fk OTP.Email to user.Email</td></tr><tr><td>Cardinality</td><td>1</td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="459c6e50-a3bb-4103-8c34-cf36ee049a4e"></a>2.6.2.2.3.3 Field **otp**

##### 2.6.2.2.3.3.1 **otp** Tree Diagram

![Hackolade image](/assets/image176.png?raw=true)

##### 2.6.2.2.3.3.2 **otp** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>OTP</td></tr><tr><td>Technical name</td><td>otp</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="b0bfa47f-4108-4589-9603-c2af591bdb53"></a>2.6.2.2.3.4 Field **is\_used**

##### 2.6.2.2.3.4.1 **is\_used** Tree Diagram

![Hackolade image](/assets/image177.png?raw=true)

##### 2.6.2.2.3.4.2 **is\_used** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Is Used?</td></tr><tr><td>Technical name</td><td>is_used</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>boolean</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="d2398192-1489-417c-91d4-a20daaf3c470"></a>2.6.2.2.3.5 Field **expiration\_date**

##### 2.6.2.2.3.5.1 **expiration\_date** Tree Diagram

![Hackolade image](/assets/image178.png?raw=true)

##### 2.6.2.2.3.5.2 **expiration\_date** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Expiration Date</td></tr><tr><td>Technical name</td><td>expiration_date</td></tr><tr><td>Activated</td><td>false</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td>date-time</td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.6.2.2.4 **otp\_verif** Indexes

<table class="index-table"><thead><tr><td class="table-property-column">Property</td><td class="table-column-property">Email Unique</td></tr></thead><tbody><tr><td>Name</td><td class="table-column-indexes">Email Unique</td></tr><tr><td>Activated</td><td class="table-column-indexes">true</td></tr><tr><td>Key</td><td class="table-column-indexes">email('ascending')</td></tr><tr><td>Hashed</td><td class="table-column-indexes"></td></tr><tr><td>Unique</td><td class="table-column-indexes">true</td></tr><tr><td>Drop duplicates</td><td class="table-column-indexes"></td></tr><tr><td>Sparse</td><td class="table-column-indexes"></td></tr><tr><td>Background indexing</td><td class="table-column-indexes"></td></tr><tr><td>Partial filter exp</td><td class="table-column-indexes"></td></tr><tr><td>Expire after (seconds)</td><td class="table-column-indexes"></td></tr><tr><td>Storage engine</td><td class="table-column-indexes">WiredTiger</td></tr><tr><td>Comments</td><td class="table-column-indexes"></td></tr></tbody></table>

##### 2.6.2.2.5 **otp\_verif** JSON Schema

```
{
    "$schema": "http://json-schema.org/draft-04/schema#",
    "type": "object",
    "title": "OTP Verification",
    "properties": {
        "_id": {
            "type": "string",
            "title": "Id",
            "pattern": "^[a-fA-F0-9]{24}$"
        },
        "email": {
            "type": "string",
            "title": "Email"
        },
        "otp": {
            "type": "string",
            "title": "OTP"
        },
        "is_used": {
            "type": "boolean",
            "title": "Is Used?"
        },
        "expiration_date": {
            "type": "string",
            "title": "Expiration Date",
            "format": "date-time"
        }
    },
    "additionalProperties": false
}
```

##### 2.6.2.2.6 **otp\_verif** JSON data

```
{
    "_id": ObjectId("c6d63f6bcbdaaadbdfda1f77"),
    "email": "Lorem",
    "otp": "Lorem",
    "is_used": true,
    "expiration_date": "2011-06-14T04:12:36.123Z"
}
```

##### 2.6.2.2.7 **otp\_verif** Target Script

```
use user_management;

db.createCollection("otp_verif", {
    "capped": false,
    "validator": {
        "$jsonSchema": {
            "bsonType": "object",
            "title": "OTP Verification",
            "properties": {
                "_id": {
                    "bsonType": "objectId",
                    "title": "Id"
                },
                "email": {
                    "bsonType": "string",
                    "title": "Email"
                },
                "otp": {
                    "bsonType": "string",
                    "title": "OTP"
                },
                "is_used": {
                    "bsonType": "bool",
                    "title": "Is Used?"
                }
            },
            "additionalProperties": false
        }
    },
    "validationLevel": "off",
    "validationAction": "warn"
});

db.otp_verif.createIndex({
    "email": 1
},
{
    "name": "Email Unique",
    "unique": true
});
```

### <a id="bf2c22f1-3ab1-499d-9310-1d6fe75f718a"></a>2.6.2.3 Collection **user**

##### 2.6.2.3.1 **user** Tree Diagram

![Hackolade image](/assets/image179.png?raw=true)

##### 2.6.2.3.2 **user** Properties

<table class="collection-properties-table"><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Collection name</td><td>user</td></tr><tr><td>Technical name</td><td></td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>$ref</td><td></td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Database</td><td><a href=#f671992c-2f83-4667-a12c-e7d809cfc82a><span class="name-container">user_management</span></a></td></tr><tr><td>Capped</td><td></td></tr><tr><td>Time series</td><td></td></tr><tr><td>Size</td><td></td></tr><tr><td>Max</td><td></td></tr><tr><td>Storage engine</td><td>WiredTiger</td></tr><tr><td>Config String</td><td></td></tr><tr><td>Validation level</td><td>Off</td></tr><tr><td>Validation action</td><td>Warn</td></tr><tr><td>Encryption metadata</td><td></td></tr><tr><td>Additional properties</td><td>false</td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.6.2.3.3 **user** Fields

<table><thead><tr><td>Field</td><td>Type</td><td>Req</td><td>Key</td><td>Description</td><td>Comments</td></tr></thead><tbody><tr><td><a href=#e876e38e-06d0-4e34-87be-2cb6ba19a5a4 class="margin-0">_id</a></td><td class="no-break-word">objectId</td><td>false</td><td>pk, dk</td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#6caf0682-c3ab-4e71-a0d2-f9ee2c68fc72 class="margin-0">full_name</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#e339375d-c233-4f5c-9973-4debe1bf1ee4 class="margin-0">gender</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#a7cd98bb-d3bc-4959-a6af-36b8f4f68988 class="margin-0">phone_number</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#4c16be16-cf48-4ac8-a89e-41f150fd848c class="margin-0">is_whatsapp</a></td><td class="no-break-word">boolean</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#6295501b-5234-4c40-a0cc-adb8202193a9 class="margin-0">email</a></td><td class="no-break-word">string</td><td>false</td><td>dk</td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#f0cad936-733b-48b0-8d35-917976dc2f08 class="margin-0">password</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#1d83f90d-34c4-42de-a09c-1a7939c00bf6 class="margin-0">date_of_birth</a></td><td class="no-break-word">date-time</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#f69484e3-4f1f-4bbf-bc18-41ab576a8307 class="margin-0">refresh_token</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#db693293-e22c-4737-a0c8-bc2cad58f74d class="margin-0">push_token</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#53a9d361-61b7-4cc1-8ebe-a9f70d32fd6a class="margin-0">account</a></td><td class="no-break-word">array</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#738c3ffd-67f7-4b92-afcf-1c0397b454c2 class="margin-5">[0]</a></td><td class="no-break-word">document</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#38e34ce1-cdcf-41c9-b8eb-a69558d7bef5 class="margin-10">oauth_id</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#0c6125bf-edd6-4ebb-a1a3-4fdf534a2f67 class="margin-10">oauth_code</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#a076e3ae-603d-439f-8505-84d21ec81768 class="margin-0">is_verified</a></td><td class="no-break-word">boolean</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#3ae6f3b5-d7a1-4c6b-9644-30cb1e49ffce class="margin-0">is_active</a></td><td class="no-break-word">boolean</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#b7eb1119-f21a-484a-bae2-a9fb5f7c7382 class="margin-0">created_date</a></td><td class="no-break-word">date-time</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#2ab9882f-9c50-4504-a852-fc3276ea204c class="margin-0">updated_date</a></td><td class="no-break-word">date-time</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="e876e38e-06d0-4e34-87be-2cb6ba19a5a4"></a>2.6.2.3.3.1 Field **\_id**

##### 2.6.2.3.3.1.1 **\_id** Tree Diagram

![Hackolade image](/assets/image180.png?raw=true)

##### 2.6.2.3.3.1.2 **\_id** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Id</td></tr><tr><td>Technical name</td><td>_id</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>objectId</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td>true</td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="6caf0682-c3ab-4e71-a0d2-f9ee2c68fc72"></a>2.6.2.3.3.2 Field **full\_name**

##### 2.6.2.3.3.2.1 **full\_name** Tree Diagram

![Hackolade image](/assets/image181.png?raw=true)

##### 2.6.2.3.3.2.2 **full\_name** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Full Name</td></tr><tr><td>Technical name</td><td>full_name</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="e339375d-c233-4f5c-9973-4debe1bf1ee4"></a>2.6.2.3.3.3 Field **gender**

##### 2.6.2.3.3.3.1 **gender** Tree Diagram

![Hackolade image](/assets/image182.png?raw=true)

##### 2.6.2.3.3.3.2 **gender** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Gender</td></tr><tr><td>Technical name</td><td>gender</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="a7cd98bb-d3bc-4959-a6af-36b8f4f68988"></a>2.6.2.3.3.4 Field **phone\_number**

##### 2.6.2.3.3.4.1 **phone\_number** Tree Diagram

![Hackolade image](/assets/image183.png?raw=true)

##### 2.6.2.3.3.4.2 **phone\_number** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Phone Number</td></tr><tr><td>Technical name</td><td>phone_number</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="4c16be16-cf48-4ac8-a89e-41f150fd848c"></a>2.6.2.3.3.5 Field **is\_whatsapp**

##### 2.6.2.3.3.5.1 **is\_whatsapp** Tree Diagram

![Hackolade image](/assets/image184.png?raw=true)

##### 2.6.2.3.3.5.2 **is\_whatsapp** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Is Whatsapp?</td></tr><tr><td>Technical name</td><td>is_whatsapp</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>boolean</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="6295501b-5234-4c40-a0cc-adb8202193a9"></a>2.6.2.3.3.6 Field **email**

##### 2.6.2.3.3.6.1 **email** Tree Diagram

![Hackolade image](/assets/image185.png?raw=true)

##### 2.6.2.3.3.6.2 **email** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Email</td></tr><tr><td>Technical name</td><td>email</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td>false</td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="f0cad936-733b-48b0-8d35-917976dc2f08"></a>2.6.2.3.3.7 Field **password**

##### 2.6.2.3.3.7.1 **password** Tree Diagram

![Hackolade image](/assets/image186.png?raw=true)

##### 2.6.2.3.3.7.2 **password** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Password</td></tr><tr><td>Technical name</td><td>password</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="1d83f90d-34c4-42de-a09c-1a7939c00bf6"></a>2.6.2.3.3.8 Field **date\_of\_birth**

##### 2.6.2.3.3.8.1 **date\_of\_birth** Tree Diagram

![Hackolade image](/assets/image187.png?raw=true)

##### 2.6.2.3.3.8.2 **date\_of\_birth** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Date of Birth</td></tr><tr><td>Technical name</td><td>date_of_birth</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td>date-time</td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td>false</td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="f69484e3-4f1f-4bbf-bc18-41ab576a8307"></a>2.6.2.3.3.9 Field **refresh\_token**

##### 2.6.2.3.3.9.1 **refresh\_token** Tree Diagram

![Hackolade image](/assets/image188.png?raw=true)

##### 2.6.2.3.3.9.2 **refresh\_token** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Refresh Token</td></tr><tr><td>Technical name</td><td>refresh_token</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="db693293-e22c-4737-a0c8-bc2cad58f74d"></a>2.6.2.3.3.10 Field **push\_token**

##### 2.6.2.3.3.10.1 **push\_token** Tree Diagram

![Hackolade image](/assets/image189.png?raw=true)

##### 2.6.2.3.3.10.2 **push\_token** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Push Token</td></tr><tr><td>Technical name</td><td>push_token</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="53a9d361-61b7-4cc1-8ebe-a9f70d32fd6a"></a>2.6.2.3.3.11 Field **account**

##### 2.6.2.3.3.11.1 **account** Tree Diagram

![Hackolade image](/assets/image190.png?raw=true)

##### 2.6.2.3.3.11.2 **account** Hierarchy

Parent field: **user**

Child field(s):

<table class="field-properties-table"><thead><tr><td>Field</td><td>Type</td><td>Req</td><td>Key</td><td>Description</td><td>Comments</td></tr></thead><tbody><tr><td><a href=#738c3ffd-67f7-4b92-afcf-1c0397b454c2 class="margin-NaN">[0]</a></td><td class="no-break-word">document</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.6.2.3.3.11.3 **account** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>OAuth Account</td></tr><tr><td>Technical name</td><td>account</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>array</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Min items</td><td></td></tr><tr><td>Max items</td><td></td></tr><tr><td>Unique items</td><td></td></tr><tr><td>Additional items</td><td>true</td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="738c3ffd-67f7-4b92-afcf-1c0397b454c2"></a>2.6.2.3.3.12 Field **\[0\]**

##### 2.6.2.3.3.12.1 **\[0\]** Tree Diagram

![Hackolade image](/assets/image191.png?raw=true)

##### 2.6.2.3.3.12.2 **\[0\]** Hierarchy

Parent field: **account**

Child field(s):

<table class="field-properties-table"><thead><tr><td>Field</td><td>Type</td><td>Req</td><td>Key</td><td>Description</td><td>Comments</td></tr></thead><tbody><tr><td><a href=#38e34ce1-cdcf-41c9-b8eb-a69558d7bef5 class="margin-NaN">OAuth&nbsp;Id</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr><tr><td><a href=#0c6125bf-edd6-4ebb-a1a3-4fdf534a2f67 class="margin-NaN">OAuth&nbsp;Code</a></td><td class="no-break-word">string</td><td>false</td><td></td><td><div class="docs-markdown"></div></td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.6.2.3.3.12.3 **\[0\]** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Display name</td><td></td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>document</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>DBRef</td><td></td></tr><tr><td>Min Properties</td><td></td></tr><tr><td>Max Properties</td><td></td></tr><tr><td>Additional properties</td><td>false</td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="38e34ce1-cdcf-41c9-b8eb-a69558d7bef5"></a>2.6.2.3.3.13 Field **oauth\_id**

##### 2.6.2.3.3.13.1 **oauth\_id** Tree Diagram

![Hackolade image](/assets/image192.png?raw=true)

##### 2.6.2.3.3.13.2 **oauth\_id** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>OAuth Id</td></tr><tr><td>Technical name</td><td>oauth_id</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="0c6125bf-edd6-4ebb-a1a3-4fdf534a2f67"></a>2.6.2.3.3.14 Field **oauth\_code**

##### 2.6.2.3.3.14.1 **oauth\_code** Tree Diagram

![Hackolade image](/assets/image193.png?raw=true)

##### 2.6.2.3.3.14.2 **oauth\_code** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>OAuth Code</td></tr><tr><td>Technical name</td><td>oauth_code</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td></td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="a076e3ae-603d-439f-8505-84d21ec81768"></a>2.6.2.3.3.15 Field **is\_verified**

##### 2.6.2.3.3.15.1 **is\_verified** Tree Diagram

![Hackolade image](/assets/image194.png?raw=true)

##### 2.6.2.3.3.15.2 **is\_verified** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Is Verified?</td></tr><tr><td>Technical name</td><td>is_verified</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>boolean</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="3ae6f3b5-d7a1-4c6b-9644-30cb1e49ffce"></a>2.6.2.3.3.16 Field **is\_active**

##### 2.6.2.3.3.16.1 **is\_active** Tree Diagram

![Hackolade image](/assets/image195.png?raw=true)

##### 2.6.2.3.3.16.2 **is\_active** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Is Active?</td></tr><tr><td>Technical name</td><td>is_active</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>boolean</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="b7eb1119-f21a-484a-bae2-a9fb5f7c7382"></a>2.6.2.3.3.17 Field **created\_date**

##### 2.6.2.3.3.17.1 **created\_date** Tree Diagram

![Hackolade image](/assets/image196.png?raw=true)

##### 2.6.2.3.3.17.2 **created\_date** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Created Date</td></tr><tr><td>Technical name</td><td>created_date</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td>date-time</td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

### <a id="2ab9882f-9c50-4504-a852-fc3276ea204c"></a>2.6.2.3.3.18 Field **updated\_date**

##### 2.6.2.3.3.18.1 **updated\_date** Tree Diagram

![Hackolade image](/assets/image197.png?raw=true)

##### 2.6.2.3.3.18.2 **updated\_date** properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>Updated Date</td></tr><tr><td>Technical name</td><td>updated_date</td></tr><tr><td>Activated</td><td>true</td></tr><tr><td>Id</td><td></td></tr><tr><td>Field-level encryption</td><td></td></tr><tr><td>Type</td><td>string</td></tr><tr><td>Description</td><td><div class="docs-markdown"></div></td></tr><tr><td>Format</td><td>date-time</td></tr><tr><td>Pattern</td><td></td></tr><tr><td>Min length</td><td></td></tr><tr><td>Max length</td><td></td></tr><tr><td>Default</td><td></td></tr><tr><td>Enum</td><td></td></tr><tr><td>Required</td><td></td></tr><tr><td>Primary key</td><td></td></tr><tr><td>Dependencies</td><td></td></tr><tr><td>Foreign collection</td><td></td></tr><tr><td>Foreign field</td><td></td></tr><tr><td>Relationship type</td><td></td></tr><tr><td>Relationship name</td><td></td></tr><tr><td>Cardinality</td><td></td></tr><tr><td>Faker function</td><td></td></tr><tr><td>Sample</td><td></td></tr><tr><td>Comments</td><td><div class="docs-markdown"></div></td></tr></tbody></table>

##### 2.6.2.3.4 **user** Indexes

<table class="index-table"><thead><tr><td class="table-property-column">Property</td><td class="table-column-property">Email Unique</td></tr></thead><tbody><tr><td>Name</td><td class="table-column-indexes">Email Unique</td></tr><tr><td>Activated</td><td class="table-column-indexes">true</td></tr><tr><td>Key</td><td class="table-column-indexes">email('ascending')</td></tr><tr><td>Hashed</td><td class="table-column-indexes"></td></tr><tr><td>Unique</td><td class="table-column-indexes">true</td></tr><tr><td>Drop duplicates</td><td class="table-column-indexes"></td></tr><tr><td>Sparse</td><td class="table-column-indexes"></td></tr><tr><td>Background indexing</td><td class="table-column-indexes"></td></tr><tr><td>Partial filter exp</td><td class="table-column-indexes"></td></tr><tr><td>Expire after (seconds)</td><td class="table-column-indexes"></td></tr><tr><td>Storage engine</td><td class="table-column-indexes">WiredTiger</td></tr><tr><td>Comments</td><td class="table-column-indexes"></td></tr></tbody></table>

##### 2.6.2.3.5 **user** JSON Schema

```
{
    "$schema": "http://json-schema.org/draft-04/schema#",
    "type": "object",
    "title": "user",
    "properties": {
        "_id": {
            "type": "string",
            "title": "Id",
            "pattern": "^[a-fA-F0-9]{24}$"
        },
        "full_name": {
            "type": "string",
            "title": "Full Name"
        },
        "gender": {
            "type": "string",
            "title": "Gender"
        },
        "phone_number": {
            "type": "string",
            "title": "Phone Number"
        },
        "is_whatsapp": {
            "type": "boolean",
            "title": "Is Whatsapp?"
        },
        "email": {
            "type": "string",
            "title": "Email"
        },
        "password": {
            "type": "string",
            "title": "Password"
        },
        "date_of_birth": {
            "type": "string",
            "title": "Date of Birth",
            "format": "date-time"
        },
        "refresh_token": {
            "type": "string",
            "title": "Refresh Token"
        },
        "push_token": {
            "type": "string",
            "title": "Push Token"
        },
        "account": {
            "type": "array",
            "title": "OAuth Account",
            "additionalItems": true,
            "items": {
                "type": "object",
                "properties": {
                    "oauth_id": {
                        "type": "string",
                        "title": "OAuth Id"
                    },
                    "oauth_code": {
                        "type": "string",
                        "title": "OAuth Code"
                    }
                },
                "additionalProperties": false
            }
        },
        "is_verified": {
            "type": "boolean",
            "title": "Is Verified?"
        },
        "is_active": {
            "type": "boolean",
            "title": "Is Active?"
        },
        "created_date": {
            "type": "string",
            "title": "Created Date",
            "format": "date-time"
        },
        "updated_date": {
            "type": "string",
            "title": "Updated Date",
            "format": "date-time"
        }
    },
    "additionalProperties": false
}
```

##### 2.6.2.3.6 **user** JSON data

```
{
    "_id": ObjectId("e98fd9dfaff6454baf2daac6"),
    "full_name": "Lorem",
    "gender": "Lorem",
    "phone_number": "Lorem",
    "is_whatsapp": true,
    "email": "Lorem",
    "password": "Lorem",
    "date_of_birth": "2011-06-14T04:12:36.123Z",
    "refresh_token": "Lorem",
    "push_token": "Lorem",
    "account": [
        {
            "oauth_id": "Lorem",
            "oauth_code": "Lorem"
        }
    ],
    "is_verified": true,
    "is_active": true,
    "created_date": "2011-06-14T04:12:36.123Z",
    "updated_date": "2011-06-14T04:12:36.123Z"
}
```

##### 2.6.2.3.7 **user** Target Script

```
use user_management;

db.createCollection("user", {
    "capped": false,
    "validator": {
        "$jsonSchema": {
            "bsonType": "object",
            "title": "user",
            "properties": {
                "_id": {
                    "bsonType": "objectId",
                    "title": "Id"
                },
                "full_name": {
                    "bsonType": "string",
                    "title": "Full Name"
                },
                "gender": {
                    "bsonType": "string",
                    "title": "Gender"
                },
                "phone_number": {
                    "bsonType": "string",
                    "title": "Phone Number"
                },
                "is_whatsapp": {
                    "bsonType": "bool",
                    "title": "Is Whatsapp?"
                },
                "email": {
                    "bsonType": "string",
                    "title": "Email"
                },
                "password": {
                    "bsonType": "string",
                    "title": "Password"
                },
                "date_of_birth": {
                    "bsonType": "string",
                    "title": "Date of Birth"
                },
                "refresh_token": {
                    "bsonType": "string",
                    "title": "Refresh Token"
                },
                "push_token": {
                    "bsonType": "string",
                    "title": "Push Token"
                },
                "account": {
                    "bsonType": "array",
                    "title": "OAuth Account",
                    "additionalItems": true,
                    "items": {
                        "bsonType": "object",
                        "properties": {
                            "oauth_id": {
                                "bsonType": "string",
                                "title": "OAuth Id"
                            },
                            "oauth_code": {
                                "bsonType": "string",
                                "title": "OAuth Code"
                            }
                        },
                        "additionalProperties": false
                    }
                },
                "is_verified": {
                    "bsonType": "bool",
                    "title": "Is Verified?"
                },
                "is_active": {
                    "bsonType": "bool",
                    "title": "Is Active?"
                },
                "created_date": {
                    "bsonType": "string",
                    "title": "Created Date"
                },
                "updated_date": {
                    "bsonType": "string",
                    "title": "Updated Date"
                }
            },
            "additionalProperties": false
        }
    },
    "validationLevel": "off",
    "validationAction": "warn"
});

db.user.createIndex({
    "email": 1
},
{
    "name": "Email Unique",
    "unique": true
});
```

### <a id="relationships"></a>

##### 3\. Relationships

### <a id="b29fcc15-a7c0-40ab-9ca7-f2854249bc09"></a>3.1 Relationship **fk Magic Token Verification(1).Doctor Id to Doctor.Id**

##### 3.1.1 **fk Magic Token Verification(1).Doctor Id to Doctor.Id** Diagram

<table><thead><tr><td>Parent Table</td><td>Parent field</td></tr></thead><tbody><tr><td><a href=#56d72f17-dfa0-4234-9b36-66a8d2bc45dd>doctor</a></td><td><a href=#3dc3ee6f-34b3-4997-82b2-3a6d9c03807a>_id</a></td></tr></tbody></table>

![Hackolade image](/assets/image198.png?raw=true)![Hackolade image](/assets/image199.png?raw=true)

<table><thead><tr><td>Child Table</td><td>Child field</td></tr></thead><tbody><tr><td><a href=#0b999a7b-fa7b-419d-9f6a-ef6df8bdbac7>magic_token_verif</a></td><td><a href=#66f606d5-bfd4-4e58-9732-9a328dd9c0b7>doctor_id</a></td></tr></tbody></table>

##### 3.1.2 **fk Magic Token Verification(1).Doctor Id to Doctor.Id** Properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>fk Magic Token Verification(1).Doctor Id to Doctor.Id</td></tr><tr><td>Description</td><td></td></tr><tr><td>Parent Collection</td><td><a href=#56d72f17-dfa0-4234-9b36-66a8d2bc45dd>doctor</a></td></tr><tr><td>Parent field</td><td><a href=#3dc3ee6f-34b3-4997-82b2-3a6d9c03807a>_id</a></td></tr><tr><td>Parent Cardinality</td><td>1</td></tr><tr><td>Child Collection</td><td><a href=#0b999a7b-fa7b-419d-9f6a-ef6df8bdbac7>magic_token_verif</a></td></tr><tr><td>Child field</td><td><a href=#66f606d5-bfd4-4e58-9732-9a328dd9c0b7>doctor_id</a></td></tr><tr><td>Child Cardinality</td><td>1</td></tr><tr><td>Comments</td><td></td></tr></tbody></table>

### <a id="3bdff99c-5203-4062-84ba-8ffba9af4b29"></a>3.2 Relationship **fk Magic Token Verification.Brand Id to Brand.Id**

##### 3.2.1 **fk Magic Token Verification.Brand Id to Brand.Id** Diagram

<table><thead><tr><td>Parent Table</td><td>Parent field</td></tr></thead><tbody><tr><td><a href=#687c8e38-bfb5-476a-b4d6-af6ef99ae244>brand</a></td><td><a href=#714db236-d303-4735-83a5-ce5b56c59fc6>_id</a></td></tr></tbody></table>

![Hackolade image](/assets/image200.png?raw=true)![Hackolade image](/assets/image201.png?raw=true)

<table><thead><tr><td>Child Table</td><td>Child field</td></tr></thead><tbody><tr><td><a href=#084dbd81-74a7-446b-bbfc-8c3e96add4a2>magic_token_verif</a></td><td><a href=#b9a18993-6e43-4df5-9402-6e8180241195>brand_id</a></td></tr></tbody></table>

##### 3.2.2 **fk Magic Token Verification.Brand Id to Brand.Id** Properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>fk Magic Token Verification.Brand Id to Brand.Id</td></tr><tr><td>Description</td><td></td></tr><tr><td>Parent Collection</td><td><a href=#687c8e38-bfb5-476a-b4d6-af6ef99ae244>brand</a></td></tr><tr><td>Parent field</td><td><a href=#714db236-d303-4735-83a5-ce5b56c59fc6>_id</a></td></tr><tr><td>Parent Cardinality</td><td>1</td></tr><tr><td>Child Collection</td><td><a href=#084dbd81-74a7-446b-bbfc-8c3e96add4a2>magic_token_verif</a></td></tr><tr><td>Child field</td><td><a href=#b9a18993-6e43-4df5-9402-6e8180241195>brand_id</a></td></tr><tr><td>Child Cardinality</td><td>1</td></tr><tr><td>Comments</td><td></td></tr></tbody></table>

### <a id="8aa0ac7f-3f40-411e-80f8-0070bd642cd9"></a>3.3 Relationship **fk Magic Token Verification.Merchant Id to Merchant.Id**

##### 3.3.1 **fk Magic Token Verification.Merchant Id to Merchant.Id** Diagram

<table><thead><tr><td>Parent Table</td><td>Parent field</td></tr></thead><tbody><tr><td><a href=#94f57272-e536-4bec-a992-620568b4a916>merchant</a></td><td><a href=#67c74360-af95-4586-b309-df297267051f>_id</a></td></tr></tbody></table>

![Hackolade image](/assets/image202.png?raw=true)![Hackolade image](/assets/image203.png?raw=true)

<table><thead><tr><td>Child Table</td><td>Child field</td></tr></thead><tbody><tr><td><a href=#93d85f88-2fd3-4050-b2dc-bb43ee06bf2f>magic_token_verif</a></td><td><a href=#69143d01-6049-454c-99b6-d5b05f33b0a9>merchant_id</a></td></tr></tbody></table>

##### 3.3.2 **fk Magic Token Verification.Merchant Id to Merchant.Id** Properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>fk Magic Token Verification.Merchant Id to Merchant.Id</td></tr><tr><td>Description</td><td></td></tr><tr><td>Parent Collection</td><td><a href=#94f57272-e536-4bec-a992-620568b4a916>merchant</a></td></tr><tr><td>Parent field</td><td><a href=#67c74360-af95-4586-b309-df297267051f>_id</a></td></tr><tr><td>Parent Cardinality</td><td>1</td></tr><tr><td>Child Collection</td><td><a href=#93d85f88-2fd3-4050-b2dc-bb43ee06bf2f>magic_token_verif</a></td></tr><tr><td>Child field</td><td><a href=#69143d01-6049-454c-99b6-d5b05f33b0a9>merchant_id</a></td></tr><tr><td>Child Cardinality</td><td>1</td></tr><tr><td>Comments</td><td></td></tr></tbody></table>

### <a id="c77cbdd1-8e88-4e3b-b1a8-6e63d5fe7e15"></a>3.4 Relationship **fk OTP Verification.Email to Brand.Merchant Email**

##### 3.4.1 **fk OTP Verification.Email to Brand.Merchant Email** Diagram

<table><thead><tr><td>Parent Table</td><td>Parent field</td></tr></thead><tbody><tr><td><a href=#687c8e38-bfb5-476a-b4d6-af6ef99ae244>brand</a></td><td><a href=#fdfdad1d-376a-44f0-9112-77543732a6e6>email</a></td></tr></tbody></table>

![Hackolade image](/assets/image204.png?raw=true)![Hackolade image](/assets/image205.png?raw=true)

<table><thead><tr><td>Child Table</td><td>Child field</td></tr></thead><tbody><tr><td><a href=#5f1e22f4-0879-40e9-aaf6-94dbcd99cd3f>otp_verif</a></td><td><a href=#5e3ffeba-6616-4874-81f1-055189857f33>email</a></td></tr></tbody></table>

##### 3.4.2 **fk OTP Verification.Email to Brand.Merchant Email** Properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>fk OTP Verification.Email to Brand.Merchant Email</td></tr><tr><td>Description</td><td></td></tr><tr><td>Parent Collection</td><td><a href=#687c8e38-bfb5-476a-b4d6-af6ef99ae244>brand</a></td></tr><tr><td>Parent field</td><td><a href=#fdfdad1d-376a-44f0-9112-77543732a6e6>email</a></td></tr><tr><td>Parent Cardinality</td><td>0..1</td></tr><tr><td>Child Collection</td><td><a href=#5f1e22f4-0879-40e9-aaf6-94dbcd99cd3f>otp_verif</a></td></tr><tr><td>Child field</td><td><a href=#5e3ffeba-6616-4874-81f1-055189857f33>email</a></td></tr><tr><td>Child Cardinality</td><td>1</td></tr><tr><td>Comments</td><td></td></tr></tbody></table>

### <a id="5676d0e8-8dd9-4d99-8663-4fa9903c3916"></a>3.5 Relationship **fk OTP.Email to user.Email**

##### 3.5.1 **fk OTP.Email to user.Email** Diagram

<table><thead><tr><td>Parent Table</td><td>Parent field</td></tr></thead><tbody><tr><td><a href=#bf2c22f1-3ab1-499d-9310-1d6fe75f718a>user</a></td><td><a href=#6295501b-5234-4c40-a0cc-adb8202193a9>email</a></td></tr></tbody></table>

![Hackolade image](/assets/image206.png?raw=true)![Hackolade image](/assets/image207.png?raw=true)

<table><thead><tr><td>Child Table</td><td>Child field</td></tr></thead><tbody><tr><td><a href=#3c6c484a-4a6d-4390-a7b3-117297837918>otp_verif</a></td><td><a href=#4428034a-1d93-4ad8-8713-6b96df5d791c>email</a></td></tr></tbody></table>

##### 3.5.2 **fk OTP.Email to user.Email** Properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>fk OTP.Email to user.Email</td></tr><tr><td>Description</td><td></td></tr><tr><td>Parent Collection</td><td><a href=#bf2c22f1-3ab1-499d-9310-1d6fe75f718a>user</a></td></tr><tr><td>Parent field</td><td><a href=#6295501b-5234-4c40-a0cc-adb8202193a9>email</a></td></tr><tr><td>Parent Cardinality</td><td>1</td></tr><tr><td>Child Collection</td><td><a href=#3c6c484a-4a6d-4390-a7b3-117297837918>otp_verif</a></td></tr><tr><td>Child field</td><td><a href=#4428034a-1d93-4ad8-8713-6b96df5d791c>email</a></td></tr><tr><td>Child Cardinality</td><td>1</td></tr><tr><td>Comments</td><td></td></tr></tbody></table>

### <a id="6406736b-1a68-44b5-a21a-73c36e03f2cb"></a>3.6 Relationship **fk Payment Method.Payment Gateway to payment\_gateway.Payment Gateway Code**

##### 3.6.1 **fk Payment Method.Payment Gateway to payment\_gateway.Payment Gateway Code** Diagram

<table><thead><tr><td>Parent Table</td><td>Parent field</td></tr></thead><tbody><tr><td><a href=#18f9a5af-8c70-44bc-a6b8-28a673f9eb13>payment_gateway</a></td><td><a href=#7a1dcbc7-ef4c-4386-81e8-f81d83ff681c>code</a></td></tr></tbody></table>

![Hackolade image](/assets/image208.png?raw=true)![Hackolade image](/assets/image209.png?raw=true)

<table><thead><tr><td>Child Table</td><td>Child field</td></tr></thead><tbody><tr><td><a href=#5bc92ff3-77a2-461f-b396-ecf1af9deb2b>payment_method</a></td><td><a href=#55240d42-cdba-4972-b39b-c5f0aa73847e>payment_gateway_code</a></td></tr></tbody></table>

##### 3.6.2 **fk Payment Method.Payment Gateway to payment\_gateway.Payment Gateway Code** Properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>fk Payment Method.Payment Gateway to payment_gateway.Payment Gateway Code</td></tr><tr><td>Description</td><td></td></tr><tr><td>Parent Collection</td><td><a href=#18f9a5af-8c70-44bc-a6b8-28a673f9eb13>payment_gateway</a></td></tr><tr><td>Parent field</td><td><a href=#7a1dcbc7-ef4c-4386-81e8-f81d83ff681c>code</a></td></tr><tr><td>Parent Cardinality</td><td>1</td></tr><tr><td>Child Collection</td><td><a href=#5bc92ff3-77a2-461f-b396-ecf1af9deb2b>payment_method</a></td></tr><tr><td>Child field</td><td><a href=#55240d42-cdba-4972-b39b-c5f0aa73847e>payment_gateway_code</a></td></tr><tr><td>Child Cardinality</td><td>0..n</td></tr><tr><td>Comments</td><td></td></tr></tbody></table>

### <a id="5f634399-6c3b-43f2-acc8-611994da352e"></a>3.7 Relationship **fk Payment.Payment Method to Payment Method.Payment Method Code**

##### 3.7.1 **fk Payment.Payment Method to Payment Method.Payment Method Code** Diagram

<table><thead><tr><td>Parent Table</td><td>Parent field</td></tr></thead><tbody><tr><td><a href=#5bc92ff3-77a2-461f-b396-ecf1af9deb2b>payment_method</a></td><td><a href=#74b8f0ba-fd04-408f-a536-90b65b1da3b4>code</a></td></tr></tbody></table>

![Hackolade image](/assets/image210.png?raw=true)![Hackolade image](/assets/image211.png?raw=true)

<table><thead><tr><td>Child Table</td><td>Child field</td></tr></thead><tbody><tr><td><a href=#0aacc000-c080-402f-ae1d-90997d4daf54>payment</a></td><td><a href=#12ff6896-4ee2-4f5a-812b-d6b88a3ce112>payment_method_code</a></td></tr></tbody></table>

##### 3.7.2 **fk Payment.Payment Method to Payment Method.Payment Method Code** Properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>fk Payment.Payment Method to Payment Method.Payment Method Code</td></tr><tr><td>Description</td><td></td></tr><tr><td>Parent Collection</td><td><a href=#5bc92ff3-77a2-461f-b396-ecf1af9deb2b>payment_method</a></td></tr><tr><td>Parent field</td><td><a href=#74b8f0ba-fd04-408f-a536-90b65b1da3b4>code</a></td></tr><tr><td>Parent Cardinality</td><td>1</td></tr><tr><td>Child Collection</td><td><a href=#0aacc000-c080-402f-ae1d-90997d4daf54>payment</a></td></tr><tr><td>Child field</td><td><a href=#12ff6896-4ee2-4f5a-812b-d6b88a3ce112>payment_method_code</a></td></tr><tr><td>Child Cardinality</td><td>0..n</td></tr><tr><td>Comments</td><td></td></tr></tbody></table>

### <a id="bf49e0b0-c051-469e-9b54-5290c853bf1d"></a>3.8 Relationship **fk Reset.User Id to user.Id**

##### 3.8.1 **fk Reset.User Id to user.Id** Diagram

<table><thead><tr><td>Parent Table</td><td>Parent field</td></tr></thead><tbody><tr><td><a href=#bf2c22f1-3ab1-499d-9310-1d6fe75f718a>user</a></td><td><a href=#e876e38e-06d0-4e34-87be-2cb6ba19a5a4>_id</a></td></tr></tbody></table>

![Hackolade image](/assets/image212.png?raw=true)![Hackolade image](/assets/image213.png?raw=true)

<table><thead><tr><td>Child Table</td><td>Child field</td></tr></thead><tbody><tr><td><a href=#71559dab-09ef-4f31-85fd-3585c5537361>magic_token_verif</a></td><td><a href=#c780c7e2-cc91-4a55-a79b-45d186686788>user_id</a></td></tr></tbody></table>

##### 3.8.2 **fk Reset.User Id to user.Id** Properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>fk Reset.User Id to user.Id</td></tr><tr><td>Description</td><td></td></tr><tr><td>Parent Collection</td><td><a href=#bf2c22f1-3ab1-499d-9310-1d6fe75f718a>user</a></td></tr><tr><td>Parent field</td><td><a href=#e876e38e-06d0-4e34-87be-2cb6ba19a5a4>_id</a></td></tr><tr><td>Parent Cardinality</td><td>1</td></tr><tr><td>Child Collection</td><td><a href=#71559dab-09ef-4f31-85fd-3585c5537361>magic_token_verif</a></td></tr><tr><td>Child field</td><td><a href=#c780c7e2-cc91-4a55-a79b-45d186686788>user_id</a></td></tr><tr><td>Child Cardinality</td><td>1</td></tr><tr><td>Comments</td><td></td></tr></tbody></table>

### <a id="955f6833-99c3-414c-887d-d47e8fd1ba1e"></a>3.9 Relationship **fk Temp Register Verification.OTP Id to OTP Verification.Id**

##### 3.9.1 **fk Temp Register Verification.OTP Id to OTP Verification.Id** Diagram

<table><thead><tr><td>Parent Table</td><td>Parent field</td></tr></thead><tbody><tr><td><a href=#5f1e22f4-0879-40e9-aaf6-94dbcd99cd3f>otp_verif</a></td><td><a href=#abc7118c-2d6c-470d-b95b-56c8b0487d49>_id</a></td></tr></tbody></table>

![Hackolade image](/assets/image214.png?raw=true)![Hackolade image](/assets/image215.png?raw=true)

<table><thead><tr><td>Child Table</td><td>Child field</td></tr></thead><tbody><tr><td><a href=#6dbd8e45-0d33-4936-a245-7717243bafe8>temp_register_verif</a></td><td><a href=#dcadbb78-86d4-447f-a31c-5921aa62b926>otp_id</a></td></tr></tbody></table>

##### 3.9.2 **fk Temp Register Verification.OTP Id to OTP Verification.Id** Properties

<table><thead><tr><td>Property</td><td>Value</td></tr></thead><tbody><tr><td>Name</td><td>fk Temp Register Verification.OTP Id to OTP Verification.Id</td></tr><tr><td>Description</td><td></td></tr><tr><td>Parent Collection</td><td><a href=#5f1e22f4-0879-40e9-aaf6-94dbcd99cd3f>otp_verif</a></td></tr><tr><td>Parent field</td><td><a href=#abc7118c-2d6c-470d-b95b-56c8b0487d49>_id</a></td></tr><tr><td>Parent Cardinality</td><td>1</td></tr><tr><td>Child Collection</td><td><a href=#6dbd8e45-0d33-4936-a245-7717243bafe8>temp_register_verif</a></td></tr><tr><td>Child field</td><td><a href=#dcadbb78-86d4-447f-a31c-5921aa62b926>otp_id</a></td></tr><tr><td>Child Cardinality</td><td>0..1</td></tr><tr><td>Comments</td><td></td></tr></tbody></table>

### <a id="edges"></a>